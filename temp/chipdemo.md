所有的hisilicon改为ingenic

hispark_taurus改为halley5

hi3516dv300改为x2000

hisi改为ingenic

hi35xx改为x2000

| 原名                    | 修改后                 |
| ----------------------- | ---------------------- |
| hisilicon               | ingenic                |
| hispark_taurus          | halley5                |
| hi3516dv300             | x2000                  |
| hisi                    | ingenic                |
| hi35xx                  | x2000                  |
| hi3881                  | x2000                  |
| ipcamera_hispark_taurus | halley5_spi_nand_flash |
|                         |                        |
|                         |                        |





| 修改路径                                      |      |
| --------------------------------------------- | ---- |
| vendor/ingenic/halley5/BUILD.gn               |      |
| vendor/ingenic/halley5/init_configs           |      |
| vendor/ingenic/halley5/hdf_config/device_info |      |
| vendor/ingenic/halley5/hdf_config             |      |
| vendor/ingenic/halley5/hdf_config/hdf.hcs     |      |
| vendor/ingenic/halley5/hdf_config/wifi        |      |
| vendor/ingenic/halley5/kernel_configs         |      |
| vendor/ingenic/halley5/config.json            |      |
|                                               |      |



![image-20211029114200720](/home/wenfei/.config/Typora/typora-user-images/image-20211029114200720.png)

![image-20211029114216738](/home/wenfei/.config/Typora/typora-user-images/image-20211029114216738.png)

可以对照着弄

| 修改路径                                              |          |
| ----------------------------------------------------- | -------- |
| device/ingenic/build                                  |          |
| device/ingenic/drivers                                | 内容很多 |
|                                                       |          |
|                                                       |          |
|                                                       |          |
| build/lite/components                                 |          |
| foundation/multimedia/camera_lite/frameworks/BUILD.gn |          |
| prebuilts/lite/sysroot/BUILD.gn                       |          |
| foundation/multimedia/camera_lite/services/BUILD.gn   |          |
| drivers/adapter/uhdf/test/BUILD.gn                    |          |
|                                                       |          |
| device/ingenic/drivers/ingenic_sdk/Makefile           |          |
|                                                       |          |
|                                                       |          |
|                                                       |          |
|                                                       |          |
|                                                       |          |
|                                                       |          |
|                                                       |          |

删除wifi一切内容





一键修改文件名的app

一键修改代码中的单词

![image-20211029172853770](/home/wenfei/.config/Typora/typora-user-images/image-20211029172853770.png)





| hdf_ingenic_sdk |                                       |
| --------------- | ------------------------------------- |
| mmc             |                                       |
| mtd_common      | device/ingenic/drivers/libs/ohos/llvm |
| spinor_flash    |                                       |

![image-20211030092532138](/home/wenfei/.config/Typora/typora-user-images/image-20211030092532138.png)

lib_dirs = [ "libs/ohos/llvm/$LOSCFG_PLATFORM" ]

![image-20211030100918190](/home/wenfei/.config/Typora/typora-user-images/image-20211030100918190.png)

kernel/liteos_a/platform/Kconfig

![image-20211030101119273](/home/wenfei/.config/Typora/typora-user-images/image-20211030101119273.png)

endor/ingenic/halle





一、编译框架搭建

当前已经基本能成功，通过hb能够拉起来

后面特别紧急的任务进度计划：

1、工具链选择（arm64下的和mips的）：mips的可以借用https://gitee.com/wenfei6316/device_ingenic里通过

![image-20211030114720743](/home/wenfei/.config/Typora/typora-user-images/image-20211030114720743.png)

拉取的代码里有编译工具（临时用）demo_companydemo_company

2、框架的搭建

device/vendor下的目录和文件的建立

3、镜像的打包工作和镜像的烧写方法

以上的任务需要提到进度中了

二、汇编的代码部分

1、arm64下的汇编华为这边李浩在全程协助中，我们这边也需要人员配合中

2、mips的汇编需要我们这边来完成

三、driver的开发

任务需要调整

1、我们要摸清楚模块驱动的框架，简单的可以让大家尝试

2、君正的驱动功能适配可能会有厂家协助完成

3、树莓派后面也会有驱动开发，这块极可能我们全程开发了，厂家极可能不会与帮助

hb build -t notest -f

demo_company/demo_board/sdk_liteos/drivers/Kconfig

