# Makefile学习指导

# 一、资料介绍

## 1.1 文档资料

[GNU_makefile中文手册](./Makefile/GNU_makefile中文手册.pdf)

[跟我一起写Makefile-陈皓](./Makefile/跟我一起写Makefile-陈皓.pdf)

## 1.2 视频资料

### 1.2.1 华清创客学院视频

02 - Linux C语言_高级--->D5 - Makefile

### 1.2.2 详细介绍视频

从零开始一步一步写项目的Makefile视频课程

# 二、Makefile需要掌握知识汇总

## 2.1 规则介绍

### 2.1.1 C代码到可执行程序之间经历的过程

![image-20211002231515660](../img/operation_manual/Makefile/代码编译过程.png)

#### 2.1.1.1 预处理阶段

```makefile
gcc -E -o main.i main.c / gcc -E main.c -o main.i
```

#### 2.1.1.2 编译阶段

```makefile
gcc -S main.i -o main.S
```

#### 2.1.1.3 汇编阶段

```makefile
gcc -c main.s -o main.o / as main.s -o main.o
```

#### 2.1.1.4 链接阶段

```makefile
gcc main.o -o main / ld -o main main.o
```

### 2.1.2 Makefile组成规则

make命令执行时，需要一个Makefile文件，告诉make命令需要怎么样去编译和链接程序。

一个简单的Makefile组成由左边的TARGET目标文件、右边的PREREQUISITES依赖文件或目标和下面的命令组成。为了组成前面三部分，Makefile会使用到各种变量、函数调用、复杂的规则等。

Note：每条COMMAND前必须是Tab，不能是空格

```makefile
TARGET... : PREREQUISITES...
	COMMAND
```

例：

```makefile
main: main.c
	gcc -o $@ $<
```

### 2.1.3 重新编译规则（详细参见2.1章节）

![image-20211001073758832](../img/operation_manual/Makefile/重新编译规则.png)

Note:

​	该规则适用于工程之前已经编译过，后面修改文件后再次重新编译

### 2.1.4 Tab规则（详细参见2.2章节）

![image-20211001073954147](../img/operation_manual/Makefile/Tab规则.png)

### 2.1.5 make如何工作（详细参见2.4章节）

​	默认情况下，直接使用make时，执行的是Makefile中第一个规则，那么该规则的第一个目标就是最后的最终目标，但是我们也可以指定执行我们想要的规则。如下例子有个说明

例：

有以下Makefile文件

```makefile
all:
    @echo "Hello World!"

clean:
    @echo "rm files"
```

当我们直接执行make时，打印的是Hello World!；而执行make clean时，打印的是rm files

```makefile
$ make all
Hello World!
$ make clean
rm files
```

### 2.1.6 自动推导和隐含规则

建议先把[华清创客学院课程的视频](#1.2.1 华清创客学院视频)先看完，这些规则通过视频学习后再看就好很多

隐含规则1：编译C程序的隐含规则

"<n>.o"的目标的依赖目标会自动推导为"<n>.c"，并且其生成命令是：

```makefile
$(CC) $(CFLAGS) $(CPPFLAGS) -c $@ $<
```

隐含规则2：链接Object文件的隐含规则

"<n>"目标依赖于"<n>.o"，通过运行C的编译器来运行链接程序生成（一般是ld），其生成命令是：

```makefile
$(CC) $(LDFLAGS) <n>.o
```

### 2.1.7 模式规则%

参见10.5章节

功能：简单来说使用%来匹配目标文件，这个%可以匹配任何非空字符串。

例：

```makefile
%.o: %.c
	gcc -c $< -o $@
```

### 2.1.8 特殊目标

|       目标名称        | 通用性 | 功能           |
| :-------------------: | :----: | -------------- |
|        .PHONY         | ★★★★★  | 伪目标         |
|       .SUFFIXES       |   ★    | 遇到时自行查找 |
|       .DEFAULT        |   ★    | 遇到时自行查找 |
|       .PRECIOUS       |   ★    | 遇到时自行查找 |
|     .INTERMEDIATE     |   ★    | 遇到时自行查找 |
|      .SECONDARY       |   ★    | 遇到时自行查找 |
|   .DELETE_ON_ERROR    |   ★    | 遇到时自行查找 |
|        .IGNORE        |   ★    | 遇到时自行查找 |
| .LOW_RESOLUTION_TIME  |   ★    | 遇到时自行查找 |
|        .SILENT        |   ★    | 遇到时自行查找 |
| .EXPORT_ALL_VARIABLES |   ★    | 遇到时自行查找 |
|     .NOTPARALLEL      |   ★    | 遇到时自行查找 |

#### .PHONY伪目标介绍

详细参见4.6章节

功能：最主要的功能是，如果执行的规则all在当前目录下有该同名文件且未更新，则make时不会去执行该规则命令，如果申明了.PHONY则表示该规则和文件名无关，每次都会去执行

例

当未申明.PHONY时

```makefile
all:
	echo "Hello World!"
    @echo "Hello Linux!"
```

执行make all命令后：

```makefile
$ ls; make all
all  all.c  main  main.o  Makefile  t.sh
make: 'all' is up to date.
```

当申明.PHONY时

```makefile
.PHONY: all 
all:
	echo "Hello World!" 
    @echo "Hello Linux!" 
```

执行make all命令后：

```makefile
$ ls; make all
all  all.c  main  main.o  Makefile  t.sh
echo "Hello World!"
Hello World!
Hello Linux!
```

### 2.1.100 make命令的参数介绍

可以直接使用man make查看make命令的参数说明，可参考：https://www.cnblogs.com/reddusty/p/4744819.html

| 参数选项 | 使用频次 | 参数介绍                                                    |
| :------: | :------: | ----------------------------------------------------------- |
|    -C    |  ★★★★★   | 读入指定目录dir下的Makefile                                 |
|    -f    |   ★★★★   | 读入当前目录下的指定file文件作为Makefile                    |
|    -i    |    ★     | 忽略所有的命令执行错误                                      |
|    -I    |   ★★★    | 指定被包含的Makefile所在的目录，类似于gcc命令的中-I参数选项 |
|    -n    |    ★     | 只打印要执行的命令，但是不执行这些命令                      |
|    -p    |    ★★    | 显示make变量数据库和隐含规则                                |
|    -s    |    ★★    | 执行命令时不显示命令                                        |
|    -w    |    ★     | make在执行过程中改变目录时打印当前目录名                    |



## 2.2 Makefile关键字和指示符

### 2.2.1 关键字include ★★★★★

当成C语言中的include使用即可

### 2.2.2 关键字export/unexport ★★★★★

export希望将一个变量传递给子make

unexport正好相反，不希望将一个变量传递给子make

### 2.2.3 关键字vpath ★★

详细参见4.5.2章节

重要程度：

为不同类型的文件（由文件名区分）指定不同的搜索目录  

例：

```makefile
vpath %.c foo ../src
vpath %.h ../inc
```

表示执行make时，对所有的.c文件可以从foo ../src目录下查找，对所有的.h文件可以从foo ../inc目录下查找

### 2.2.4 指示符override ★

详细参见6.7章节

备注：一般不要使用override，一旦使用后在后面对该变量重新赋值操作容易失败

作用：

a) 变量使用override修饰后，在后面对该变量赋值操作都必须添加override修视，否则赋值操作无效

例：

有如下代码

```makefile
target = main

override CFLAGS += -g -Wall
CFLAGS = -O2

all:
    gcc $(CFLAGS) -o $(target)  $(target).c
```

执行make all时，CFLAGS显是的是-g -Wall

```shell
$ make all
gcc -g -Wall -o main  main.c
```

b) 变量使用override修饰后，执行 make 时命令行指定的变量值不会和Makefile中的变量冲突

例：

有如下代码

```makefile
target = main

override CFLAGS += -g
override WALL = -Wall

all:
    gcc $(CFLAGS) $(WALL) -o $(target)  $(target).c
```

执行make CFLAGS=-O2 WALL=-Werror all时，CFLAGS显是的是-O2 -g，WALL显是的是-Wall

```shell
$ make CFLAGS=-O2 WALL=-Werror all
gcc -O2 -g -Wall -o main  main.c
```

## 2.3 语法介绍

### 2.3.1 条件语法

#### 2.3.1.1 关键字ifneq/ifeq

详细参见7.2章节

语法格式

```makefile
ifneq (condition)
else ifneq (condition)
else
endif
```

例：

```makefile
ifeq ($(TARGET_ARCH), arm)
	LOCAL_SRC_FILES := ...
else ifeq ($(TARGET_ARCH), x86)
	LOCAL_SRC_FILES := ...
else 
	LOCAL_SRC_FILES := ...
endif
```

#### 2.3.1.2 关键字ifndef/ifdef

语法格式

```makefile
ifdef (condition)
else
endif
```

例：

```makefile
ifndef CONFIG_FUNCTION_TRACER
KBUILD_CFLAGS   += -fomit-frame-pointer
endif
```

### 2.3.2 循环语法

## 2.4 变量介绍

### 2.4.1 变量的定义和使用

#### 2.4.1.1 变量的定义方式

直接定义，例如src = ./main.c表示src的值为./main.c，如果使用$(src)时就直接使用的是./main.c

#### 2.4.1.2 变量的赋值

变量的赋值分为三种

##### =赋值（递归展开赋值）

如果有引用其他变量时，在赋值的时候不会展开，而是会在该变量被引用的时候才会展开

##### ：=赋值（直接展开赋值）

如果有引用其他变量时，在赋值的时候直接展开

##### ?=赋值

如果变量在之前没有被赋值那么才赋值如果已经赋值了就不赋值了

例：

```makefile
y = l
x = $(y)
z := $(y)
y = m
y ?= n
t ?= n

all: 
	@echo x = $x
	@echo y = $y
	@echo z = $z
	@echo t = $t
```

执行make all后的结果

```makefile
$ make all
x = m
y = m
z = l
t = n
```

#### 2.4.1.3 变量高级用法

##### 替换引用

详细参见6.3.1章节

用法：$(VAR:A=B) 

功能：替换变量“VAR”中所有“A”字符结尾的字改为“B”结尾的字  

例：

```makefile
foo := a.o b.o c.o
bar := $(foo:.o=.c)

all:
    @echo $(foo)
    @echo $(bar)
```

执行make all结果：bar为a.c b.c c.c

```makefile
$ make all
a.o b.o c.o
a.c b.c c.c
```

##### 嵌套引用

详细参见6.3.2章节

一个变量名（文本串）之中可以包含对其它变量的引用  

例：

```makefile
x = y
y = z
z = u
a := $($($(x)))

all:
    @echo $(a)
```

执行make all结果：a的值为u

```makefile
$ make all
u
```

### 2.4.2 自动变量（通配符）

​	详细参见10.5.3章节

#### 2.4.2.1 标准自动变量

| 自动变量 | 通用性 | 变量说明                                                     |
| :------: | :----: | :----------------------------------------------------------- |
|    $@    | ★★★★★  | 目标文件名                                                   |
|    $<    | ★★★★★  | 第一个依赖文件的名称                                         |
|    $^    |  ★★★   | 所有依赖文件列表（去除重复项）                               |
|    $*    |   ★★   | 慎用：简单理解为目标文件名去除后缀的部分                     |
|    $+    |   ★    | 所有依赖文件列表（保留重复项），一般使用$^                   |
|    $?    |   ★    | 所有时间戳比目标文件晚的依赖文件，基本不使用                 |
|    $%    |   ★    | 当目标文件是一个静态库文件时，代表静态库的一个成员名  ，基本不使用 |

例：

main.o: main.c func1.c func2.c main.c

$@对应的是main.o

$<对应的是main.c

$^对应的是main.c func1.c func2.c

$*对应的是main

$+对应的是main.c func1.c func2.c main.c

#### 2.4.2.2 复合自动变量（自己定义的）

| 自动变量 | 通用性 | 变量说明                                            |
| :------: | :----: | :-------------------------------------------------- |
|  $(@D)   |   ★★   | 目标文件的目录部分（不包含/）                       |
|  $(@F)   |   ★★   | 目标文件中去除目录以外的部分（实际文件名）          |
|  $(<D)   |   ★★   | 第一个依赖文件的目录部分（不包含/）                 |
|  $(<F)   |   ★★   | 第一个依赖文件的去除目录以外的部分（实际文件名）    |
|  $(^D)   |   ★★   | 所有依赖文件列表的目录部分（不包含/）（去除重复项） |
|  $(^F)   |   ★★   | 所有依赖文件列表的目录部分（不包含/）（去除重复项） |
|  $(*D)   |   ★    | 目标“茎”中的目录部分                                |
|  $(*F)   |   ★    | 目标“茎”中的文件名部分                              |
|  $(+D)   |   ★    | 所有依赖文件列表的目录部分（不包含/）（保留重复项） |
|  $(+F)   |   ★    | 所有依赖文件列表的目录部分（不包含/）（保留重复项） |
|  $(?D)   |   ★    | 所有时间戳比目标文件晚的依赖文件的目录部分          |
|  $(?F)   |   ★    | 所有时间戳比目标文件晚的依赖文件的文件名部分        |
|  $(%D)   |   ★    | 库文件成员名中的目录部分                            |
|  $(%F)   |   ★    | 库文件成员名中的文件名部分                          |

### 2.4.3 系统环境变量

#### 2.4.3.1 VPATH

功能：指定依赖文件的搜索路径，当规则的依赖文件在当前目录不存在时，make 会在此变量所指定的目录下去寻找这些依赖文件

例：

```makefile
VPATH = src:../headers
foo.o: foo.c foo.h
	$(GCC) -c $^ -o $@
```

对于foo.c或foo.h文件如果在编译的时候在指定的路径下找不到，就会到src和../headers路径下去搜索，它没有vpath灵活，vpath能针对不同类型的文件设置不同的搜索路径，后面再说。

2.4.4 预定义变量

| 预定义变量 | 通用性 | 变量说明                          |
| :--------: | :----: | --------------------------------- |
|     AR     |   ★★   | 库文件操作的命令名称，默认值为ar  |
|     AS     |  ★★★   | 汇编操作的命令名称，默认值为as    |
|     CC     | ★★★★★  | C编译器的名称，默认值为cc         |
|    CPP     |   ★★   | C预编译器的名称，默认值为$(CC) -E |
|    CXX     | ★★★★★  | C++编译器的名称，默认值为g++      |
|     FC     |   ★    | FORTRAN编译器的名称，默认值为777  |
|     RM     |   ★★   | 文件删除的命令，默认值为rm -f     |
|  ARFLAGS   |   ★★   | 库文件操作的选项，无默认值        |
|  ASFLAGS   |   ★★   | 汇编操作的选项，无默认值          |
|   CFLAGS   |  ★★★★  | C编译器的选项，无默认值           |
|  CPPFLAGS  |   ★★   | C预编译器的选项，无默认值         |
|  CXXFLAGS  |  ★★★★  | C++编译器的选项，无默认值         |
|   FFLAGS   |   ★    | FORTRAN编译器的选项，无默认值     |

## 2.5 函数介绍

函数介绍参见第八章

### 2.5.1 subst函数

函数用法：$(subst FROM, TO, TEXT)

函数名称：字符串替换函数

函数功能：把字符串"TEXT"中的"FROM"字符替换为"TO"

函数返回值：替换后的新字符串  

例：

```makefile
all:
    @echo $(subst world, Linux, Hello world!)
```

执行make all的结果为：Hello Linux!

```makefile
$ make all
Hello Linux!
```

### 2.5.2 patsubst函数

函数用法：$(patsubst PATTERN, REPLACEMENT, TEXT)

函数名称：模式替换函数 

函数功能：搜索"TEXT"中以空格分开的单词，将符合模式"TATTERN"替换为"REPLACEMENT"

函数返回值：替换后的新字符串  

例：

```makefile
foo := a.o b.o c.o

all:
    @echo $(patsubst %.o, %.c, $(foo))
```

执行make all的结果为：a.c b.c c.c

```makefile
$ make all
a.c b.c c.c
```

### 2.5.3 strip函数

函数用法：$(strip STRINT)

函数名称：去空格函数 

函数功能：去掉字符串"STRINT"开头和结尾的空字符，并将其中多个连续空字符合并为一个空字符

函数返回值：无前导和结尾空字符、使用单一空格分割的多单词字符串  

例：

```makefile
STR =        a       b c

all:
    @echo $(strip $(STR))
```

执行make all的结果为：a b c

```makefile
$ make all
a b c
```

### 2.5.4 findstring函数

函数用法：$(findstring FIND, IN)

函数名称：查找字符串函数

函数功能：在字符串"IN"中查找"FIND"字符串

函数返回值：查找到返回"FIND"，否则返回空

例：

```makefile
all:
    @echo $(findstring s, a b c)
    @echo $(findstring a, a b c)
```

执行make all的结果为：

```makefile
$ make all

a
```

### 2.5.5 filter函数

函数用法：$(filter PATTERN…, TEXT)

函数名称：过滤函数

函数功能：过滤掉字符串"TEXT"中所有不符合模式"PATTERN"的单词，保留所有符合此模式的单词

函数返回值：空格分割的"TEXT"字符串中所有符合模式"PATTERN"的字符串

例：

```makefile
src := foo.c bar.c baz.s ugh.h

all:
    @echo $(filter %.c %.s, $(src))
```

执行make all的结果为：foo.c bar.c baz.s

```makefile
$ make all
foo.c bar.c baz.s
```

### 2.5.6 filter-out函数

函数用法：$(filter-out PATTERN…, TEXT)

函数名称：反过滤函数

函数功能：和filter函数实现的功能相反  过滤掉字符串"TEXT"中所有符合模式"PATTERN"的单词，保留所有不符合此模式的单词

函数返回值：空格分割的"TEXT"字符串中所有不符合模式"PATTERN"的字符串

例：

```makefile
src := foo.c bar.c baz.s ugh.h

all:
    @echo $(filter-out %.c %.s, $(src))
```

执行make all的结果为：ugh.h

```makefile
$ make all
ugh.h
```

### 2.5.7 sort函数

函数用法：$(sort LIST)

函数名称：排序函数

函数功能：将字符串"LIST"中的单词以首字母为准进行排序（升序），并去掉重复的单词

函数返回值：空格分割的没有重复单词的字符串

例：

```makefile
all:
    @echo $(sort foo bar lose foo)
```

执行make all的结果为：bar foo lose

```makefile
$ make all
bar foo lose
```

### 2.5.8 word函数

函数用法：$(word N, TEXT)

函数名称：取单词函数

函数功能：取字符串"TEXT"中第N个单词（N的值从1开始）

函数返回值：返回字符串中第N个单词

例：

```makefile
all:
    @echo $(word 2, foo bar baz)
```

执行make all的结果为：bar

```makefile
$ make all
bar
```

### 2.5.9 wordlist函数

函数用法：$(wordlist S, E, TEXT)

函数名称：取字符串函数

函数功能：从字符串"TEXT"中取出从S开始到E的单词串。S和E表示单词在字符串中位置的数字

函数返回值：字符串"TEXT"中从第S到E（包括E）的单词字符串

例：

```makefile
all:
    @echo $(wordlist 2, 3, foo bar baz)
```

执行make all的结果为：bar baz

```makefile
$ make all
bar baz
```

### 2.5.10 words函数

函数用法：$(words, TEXT)

函数名称：统计单词数目函数

函数功能：计算字符串"TEXT"中单词的数目

函数返回值："TEXT"字符串中单词数

例：

```makefile
all:
    @echo $(words, foo bar baz)
```

执行make all的结果为：3

```makefile
$ make all
3
```

### 2.5.11 firstword函数

函数用法：$(firstword NAMES…)

函数名称：取首单词函数

函数功能：取字符串"NAMES…"中的第一个单词

函数返回值：字符串"NAMES…"的第一个单词

例：

```makefile
all:
    @echo $(firstword foo bar)
```

执行make all的结果为：foo

```makefile
$ make all
foo
```

### 2.5.12 dir函数

函数用法：$(dir NAMES…)

函数名称：取目录函数

函数功能：从文件名序列"NAMES…"中取出各个文件名的目录部分

函数返回值：空格分割的文件名序列"NAMES…"中每一个文件的目录部分

例：

```makefile
all:
    @echo $(dir src/foo.c hacks)
```

执行make all的结果为：src/ ./  

```makefile
$ make all
src/ ./
```

### 2.5.12 notdir函数

函数用法：$(notdir NAMES…)

函数名称：取文件名函数

函数功能：从文件名序列"NAMES…"中取出非目录部分

函数返回值：文件名序列"NAMES…"中每一个文件的非目录部分

例：

```makefile
all:
    @echo $(notdir foo bar)
```

执行make all的结果为：foo.c hacks  

```makefile
$ make all
foo.c hacks
```

### 2.5.13 suffix函数

函数用法：$(suffix NAMES…)

函数名称：取后缀函数

函数功能：从文件名序列"NAMES…"中取出各个文件名的后缀，如果文件名没有后缀部分，则返回空  

函数返回值：空格分割的文件名序列"NAMES…"中每一个文件的后缀序列

例：

```makefile
all:
    @echo $(suffix src/foo.c inc/bar.h hacks)
```

执行make all的结果为：.c .h

```makefile
$ make all
.c .h
```

### 2.5.14 basename函数

函数用法：$(basename NAMES…)

函数名称：取前缀函数

函数功能：从文件名序列"NAMES…"中取出各个文件名的前缀部分

函数返回值：空格分割的文件名序列"NAMES…"中每一个文件的前缀序列

例：

```makefile
all:
    @echo $(basename src/foo.c inc/bar.h hacks)
```

执行make all的结果为：src/foo inc/bar hacks

```makefile
$ make all
src/foo inc/bar hacks
```

### 2.5.15 addsuffix函数

函数用法：$(addsuffix SUFFIX, NAMES…)

函数名称：加后缀函数

函数功能：为"NAMES…"中的第一个文件名添加后缀"SUFFIX"

函数返回值：以单空格分割的添加了后缀"SUFFIX"的文件名序列

例：

```makefile
all:
    @echo $(addsuffix .c, foo bar)
```

执行make all的结果为：foo.c bar.c

```makefile
$ make all
foo.c bar.c
```

### 2.5.16 addprefix函数

函数用法：$(addprefix PREFIX, NAMES…)

函数名称：加前缀函数

函数功能：为"NAMES…"中的第一个文件名添加前缀"PREFIX"

函数返回值：以单空格分割的添加了前缀"PREFIX"的文件名序列

例：

```makefile
all:
    @echo $(addprefix src/, foo bar)
```

执行make all的结果为：src/foo src/bar

```makefile
$ make all
src/foo src/bar
```

### 2.5.17 join函数

函数用法：$(join LIST1, LIST2)

函数名称：单词连接函数

函数功能：将字串"LIST1"和字串"LIST2"各单词进行对应连接

函数返回值：单空格分割的合并后的字符串（文件名）序列  

例：

```makefile
all:
    @echo $(join a b c, .c .o)
```

执行make all的结果为：foo

```makefile
$ make all
a.c b.o c
```

### 2.5.18 wildcard函数

函数用法：$(wildcard PATTERN)

函数名称：获取匹配模式文件名函数

函数功能：列出当前目录下所有符合模式"PATTERN"格式的文件名

函数返回值：空格分割的、存在当前目录下的所有符合模式"PATTERN"的文件名

例：

```makefile
all:
    @echo $(wildcard *.c)
```

执行make all的结果为：main.c

```makefile
$ ls; make all
main  main.c  Makefile
main.c
```

### 2.5.18 wildcard函数

函数用法：$(wildcard PATTERN)

函数名称：获取匹配模式文件名函数

函数功能：列出当前目录下所有符合模式"PATTERN"格式的文件名

函数返回值：空格分割的、存在当前目录下的所有符合模式"PATTERN"的文件名

例：

```makefile
all:
    @echo $(wildcard *.c)
```

执行make all的结果为：main.c

```makefile
$ ls; make all
main  main.c  Makefile
main.c
```

### 2.5.19 foreach函数

函数用法：$(foreach VAR, LIST, TEXT)

函数功能：将LIST中的值一次赋给VAR后执行TEXT

函数返回值：空格分割的多次表达式"TEXT"的计算的结果

例：

```makefile
LIST = main main.c test.h

all:
    @echo $(foreach val, $(LIST), $(val))
```

执行make all的结果为：main main.c test.h

```makefile
$ make all
main main.c test.h
```

### 2.5.20 if函数

函数用法：$(if CONDITION, THEN-PART[, ELSE-PART])

函数功能：如果CONDITION不为空，则返回THEN-PART，否则返回ELSE-PART

函数返回值：如果CONDITION不为空，则返回THEN-PART，否则返回ELSE-PART

例：

```makefile
target = main

app = $(if $(target), $(target), a.out)

all:
    @echo $(app)
```

执行make all的结果为：main

```makefile
$ make all
main
```

### 2.5.21 value函数

函数用法：$(value VARIABLE)

函数名称：

函数功能：直接返回变量"VARIBALE"的值，如果VARIABLE中有需要展开的变量会报错

函数返回值：空格分割的、存在当前目录下的所有符合模式"PATTERN"的文件名

例：

```makefile
target = main
LPATH = $PATH ./
LIST = main.c test.h $(target)

all:
    @echo $(LPATH)
    @echo $(value LPATH)
    @echo $(value LIST)
```

执行make all的结果为：

```makefile
$ make all
ATH ./
/home/wenfei/.local/bin:/home/wenfei/tools/ninja:/home/wenfei/tools/llvm/bin:/home/wenfei/tools/hc-gen:/home/wenfei/tools/gn:/home/wenfei/.local/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/sbin:/usr/sbin:/home/wenfei/tools/gcc-linaro-4.9.4-2017.01-x86_64_arm-linux-gnueabi/bin ./
/bin/sh: target: command not found
main.c test.h
```

### 2.5.22 eval函数

该函数理解比较复杂

可参考：https://blog.csdn.net/brooknew/article/details/8452358

### 2.5.23 origin函数

函数用法：$(origin VARIABLE)

函数功能：查询参数"VARIABLE"的出处

函数返回值：返回"VARIABLE"的定义方式，用字符串表示

例：

```makefile
target = main
override DST = main.o

all:
    @echo target is $(origin target)
    @echo DST is $(origin DST)
    @echo  CC is $(origin CC)
    @echo WALL is $(origin WALL)
    @echo CFLAGS is $(origin CFLAGS)
```

执行make all的结果为：

```makefile
$ make WALL=-Wall all
target is file
DST is override
CC is default
SHELL is file
WALL is command line
CFLAGS is undefined
```

### 2.5.24 shell函数

函数用法：$shell cmd)

函数功能：使用一个shell命令作为此函数的参数，函数的返回结果是此命令在shell中的执行结果

例：

```makefile
all:
    @echo ls is: $(shell ls)
    @echo pwd: $(shell pwd)
```

执行make all的结果为：

```makefile
$ make all
ls is: main main.c Makefile
pwd: /home/wenfei/test
```

### 2.5.25 call函数

函数用法：$(call VARIABLE, PARAM, PARAM, ...)

函数功能：创建定制化参数函数的引用函数  

函数返回值：变量"VARIABLE"定义的表达式的计算值

例：

```makefile
reverse = $(1) $(2)

define func
$(info echo $1)
endef

all:
    $(call func, abc)
    @echo $(call reverse, a, b)
    @echo $(call subst, world, Linux, Hello world!)
```

执行make all的结果为：main.c

```makefile
$ make all
echo  abc
a b
Hello Linux!
```

## 2.6 其他介绍

### 2.6.1 命令前加@的作用

表示执行命令的时候不要把要执行的命令打印出来

例：

```makefile
all:
	echo "Hello World!"
	@echo "Hello Linux!"
```

执行make all时，第一条命令会把命令打印出来，而第二条不会

```makefile
$ make all
echo "Hello World!"
Hello World!
Hello Linux!
```









