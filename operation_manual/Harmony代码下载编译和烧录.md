# 代码下载和编译，基于Ubuntu20.04环境搭建

## 一、Ubuntu依赖库安装

按照该文档[Ubuntu环境搭建](./Ubuntu环境搭建.md)中的要求安装相应的工具，当前10.20.14.56服务器的root权限安装的工具已经完成

## 二、代码下载和编译

​		我们目前项目需求中涉及到功能模块的开发和移植，使用的是master分支的代码，涉及到指定单板的移植和开发项目使用的是v3.0 LTS分支的代码。

### 2.1 主线master分支代码下载和编译

#### 2.1.1 代码下载

```shell
mkdir -p ~/OpenHarmony/master
cd ~/OpenHHarmony/master
repo init -u https://gitee.com/openharmony/manifest.git -b master --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

#### 2.1.2 代码编译

**设置编译路径并选择编译平台**

a> 路径选择 . 表示编译后的out目录在当前目录下

b> product可通过键盘上下左右键选择想要编译的平台，可以通过鼠标选择

c> 选择成功后按Enter键保存

d> 输入hb build -f执行编译

```
hb set
[OHOS INFO] Input code path: .
OHOS Which product do you need? ipcamera_hispark_taurus
hb build -f
```

![image-20211018120434815](../img/operation_manual/code_download_compiler/docker编译方法.png)

e> 出现以下表示编译成功

```
[OHOS INFO] ipcamera_hispark_taurus build success
```

![image-20211018145650950](../img/operation_manual/code_download_compiler/编译成功.png)

f> 编译结果

在out/hispark_taurus/ipcamera_hispark_taurus目录下有如下镜像

![image-20211018150440919](../img/operation_manual/code_download_compiler/镜像结果.png)

在device/hisilicon/hispark_taurus/sdk_liteos/uboot/out/boot目录下有u-boot-hi3516dv300.bin文件

![image-20211018150649263](../img/operation_manual/code_download_compiler/uboot.png)

在out/obj目录下是编译生成的中间文件，但是必须在root下才有权限，或者在docker在进入

![image-20211018151034356](../img/operation_manual/code_download_compiler/编译中间文件.png)

Note:

a> 可通过hb --help查看hb命令包含哪些

b> 可通过hb build --help查看hb build命令的帮助文档

##### 2.1.2.2 本地直接编译，容易不成功，不推荐

当前只尝试Hi3516DV300能够编译成功

```shell
cd ~/OpenHarmony/master/build
bash prebuilts_download.sh
sudo ./build.sh --product-name Hi3516DV300 --ccache
```

以下表示bash prebuilts_download.sh执行成功

![image-20211018153322260](../img/operation_manual/code_download_compiler/prebuild执行成功.png)

### 2.2 v3.0 LTS分支代码下载和编译

#### 2.2.1 代码下载

```shell
mkdir -p ~/OpenHarmony/v3
cd ~/OpenHHarmony/v3
repo init -u https://gitee.com/openharmony/manifest.git -b refs/tags/OpenHarmony-v3.0-LTS --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

#### 2.2.2 代码编译

和2.1.2.1 编译方法一致

**设置编译路径并选择编译平台**

a> 路径选择 . 表示编译后的out目录在当前目录下

b> product可通过键盘上下左右键选择想要编译的平台，可以通过鼠标选择

c> 选择成功后按Enter键保存

d> 输入hb build -f执行编译

```
hb set
[OHOS INFO] Input code path: .
OHOS Which product do you need? ipcamera_hispark_taurus
hb build -f
```

### 2.3 Canary分支代码下载和编译

#### 2.3.1 代码下载

```shell
mkdir -p ~/OpenHarmony/canary
cd ~/OpenHarmony/canary
repo init -u https://gitee.com/openharmony/manifest.git -b refs/tags/OpenHarmony-2.0-Canary --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

#### 2.3.2 代码编译

和2.1.2.1 编译方法一致

**设置编译路径并选择编译平台**

a> 路径选择 . 表示编译后的out目录在当前目录下

b> product可通过键盘上下左右键选择想要编译的平台，可以通过鼠标选择

c> 选择成功后按Enter键保存

d> 输入hb build -f执行编译

```
hb set
[OHOS INFO] Input code path: .
OHOS Which product do you need? ipcamera_hispark_taurus
hb build -f
```

### 2.4 v1.1.3 LTS分支代码下载和编译

#### 2.4.1 代码下载

```shell
repo init -u https://gitee.com/openharmony/manifest.git -b refs/tags/OpenHarmony-v1.1.3-LTS --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

#### 2.4.2 代码编译

略

### 2.5 hb代码编译方式

#### 2.5.1 全量编译命令

```shell
hb build -f
```

#### 2.5.2 不编译测试文件命令

```shell
hb build -t notest -f
```

#### 2.5.3 编译单模块

如下命令中的ai, libcrypto_static是对应路径下BUILD.gn里面生成目标的名称

```shell
hb build -T //path:{target}
hb build -T //applications/sample/camera/ai:ai
hb build -T //third_party/openssl:libcrypto_static
```



## 三、Hispark_Taurus开发板烧录

### 3.1 代码版本选择

版本可以选择v3LTS版本，实测烧写正常，master分支尚未验证

### 3.2 编译方法选择

参考[2.2.2 代码编译](#2.2.2 代码编译)

### 3.3 镜像选择

#### 3.3.1 uboot选择

路径：device/hisilicon/hispark_taurus/sdk_liteos/uboot/out/boot/u-boot-hi3516dv300.bin

第一次烧写时候需要选择uboot，其他时候除非擦除全器件，否则不用烧写uboot

#### 3.3.2 其他镜像选择

路径：out/hispark_taurus/ipcamera_hispark_taurus

镜像名：OHOS_Image.bin、rootfs_vfat.img、userfs_vfat.img

### 3.4 烧录工具安装

工具路径：svn://10.20.14.56/tools/烧录工具

#### 3.4.1 USB-to-Serial Comm Port和串口驱动安装

直接双击安装

#### 3.4.2 HiTool工具使用

HiTool是绿色烧写工具，直接运行就行

### 3.5 烧录

3.5.1 将编译好的镜像放到指定文件夹中，例如F:\hispark_tauras目录下

3.5.2 打开HiTool.exe软件，进入窗口--->首选项--->HiBurn--->TFTP设置--->开启TFTP加速取消勾选，然后确定

3.5.3 默认单板已经烧写了uboot后配置启动参数

a> 打开串口，启动单板后迅速按回车进入uboot界面

b> 在串口中输入print，查看配置参数

```shell
arch=arm
baudrate=115200
board=hi3516dv300
board_name=hi3516dv300
bootargs=console=ttyAMA0,115200n8 root=emmc fstype=vfat rw rootaddr=10M rootsize=20M
bootcmd=mmc read 0x0 0x80000000 0x800 0x4800;go 0x80000000;
bootdelay=3
cpu=armv7
ethact=eth0
ethaddr=00:75:8c:7d:19:d2
gateway=10.20.14.254
ipaddr=10.20.14.52
serverip=10.20.14.53
soc=hi3516dv300
stderr=serial
stdin=serial
stdout=serial
vendor=hisilicon
verify=n
```

重要参数介绍：

```shell
arch=arm      #芯片架构
baudrate=115200    #设置波特率
board=hi3516dv300  #单板型号
board_name=hi3516dv300 #设置单板名
bootargs=console=ttyAMA0,115200n8 root=emmc fstype=vfat rw rootaddr=10M rootsize=20M
#root/fstype/rootaddr/rootsize必须和HiTool中设置的一致
bootcmd=mmc read 0x0 0x80000000 0x800 0x4800;go 0x80000000;
bootdelay=3
cpu=armv7
ethact=eth0
ethaddr=00:75:8c:7d:19:d2 #Mac地址设置
gateway=10.20.14.254 #网关地址
ipaddr=10.20.14.52 #单板的IP地址设置
serverip=10.20.14.53 #下载镜像的服务器ip地址（网口下载时必须配置成功）
soc=hi3516dv300
stderr=serial
stdin=serial
stdout=serial
vendor=hisilicon
verify=n
```

c> 新增/修改变量，结束后使用save保存

```shell
setenv ipaddr 10.20.14.52
save
re  #重启
```

3.5.4 烧录镜像配置

![image-20211027155230330](../img/operation_manual/code_download_compiler/Hitool参数配置.png)

3.5.5 镜像烧录

a> 关闭已经打开的串口

b> 点击HiTool中的烧写

c> 开发板断电后重启

![image-20211027155840505](../img/operation_manual/code_download_compiler/正常烧写.png)

![image-20211027155929110](../img/operation_manual/code_download_compiler/烧录成功.png)
