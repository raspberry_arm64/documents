#### 单板nfs挂载Windows下目录

##### 1，下载安装nfs服务器软件

官网下载，一路回车即可。



##### 2，启动nfs服务器

![](https://gitee.com/halley5/documents/raw/master/img/nfs/1.png)

打开nfs服务器后打开Edit下的preferences，显示如下

![](https://gitee.com/halley5/documents/raw/master/img/nfs/2.png)

大部分按默认设置，不需要改动，在"Exports"菜单里面把要共享的目录发布出来。



##### 3，在"Exports"菜单里面 发布要共享的目录

![](https://gitee.com/halley5/documents/raw/master/img/nfs/3.png)

点击 “Edit  exports file”,程序会打开一个记事本，让你编辑共享脚本。

![](https://gitee.com/halley5/documents/raw/master/img/nfs/4.png)

把要共享的目录添加到这里，比如我要共享的目录是e盘下的driver目录，

在输出表文件下我加入 e:\driver   -name:nfs   -public

1,填写服务器的共享目录，就是在Windows下要共享出去的目录

2，共享目录名，方便之后按这个名字挂载

注意点：

1，盘符一定要用小写字母 c, d 等表示

2，文件夹名不要出现空格

添加完编辑表文件后点击“Save”保存下，在按确实，这时共享目录就发布出去了。



##### 4 ，防火墙设置

Nfs服务器架设好了之后，然后设置防火墙

进入到"控制面板---->Windows Defender 防火墙“

![](https://gitee.com/halley5/documents/raw/master/img/nfs/5.png)

进入到“高级设置”里面，进入到下面的界面

![](https://gitee.com/halley5/documents/raw/master/img/nfs/6.png)

第一步 点击”入站规则“，进入了以后第二步点击”新建规则“，进入到以下界面

![](https://gitee.com/halley5/documents/raw/master/img/nfs/7.png)

选择“端口”然后点击进入“下一步”

![](https://gitee.com/halley5/documents/raw/master/img/nfs/8.png)

选“特定本地端口”，输入nfs服务器的端口号“ 111，1058，2049”，然后点击进入“下一步”

![](https://gitee.com/halley5/documents/raw/master/img/nfs/9.png)

选择“允许连接”，点击进入“下一步”

![](https://gitee.com/halley5/documents/raw/master/img/nfs/10.png)

点击进入下一步

![](https://gitee.com/halley5/documents/raw/master/img/nfs/11.png)

输入规则名称，我这里的名称是nfs，点击完成即可。

这是TCP通信规则的建立，UDP通信规则的建立也是上述流程。



##### 5, 单板nfs挂载共享目录

首先启动单板，单板启动后进入#ohos 命令行

在#ohos命令行下，新建一个目录作为挂载点目录

例如： mkdir test    这个test目录就是挂载点目录

然后输入命令：mount  192.168.1.2:/nfs   ./test   nfs

mount是挂载命令

192.168.1.2:/nfs  :  192.168.1.2是nfs服务器的IP地址

./test ：就是新建的挂载点目录

nfs:共享目录名

这样就完成了单板挂载Windows的共享目录。

进入到test目录，在#ohos命令行下输入ls ,可以共享到Windows下共享目录里面的文件。

