### MIPS中断和异常

架构版本2新增与异常和中断处理相关的特性：

增加了协处理器0的EBase寄存器，当StatusBEV等于0时，可以修改异常向量的基地址。需要进行EBase注册。

版本1中断控制机制的扩展，包括两种可选的中断模式:

- 向量中断(VI)模式，在这种模式下，不同的中断源由处理器和每个中断都直接指向一个专用的处理程序。当与GPR阴影寄存器结合时，这种模式显著减少了处理内部中断所需的周期数。

- 外部中断控制器(EIC)模式，在此模式下，协处理器0寄存器字段的定义与中断关联改变以支持外部中断控制器。这可以支持更多的基于的优先中断，同时还提供了将中断直接指向专用处理程序和获取的能力GPR阴影寄存器的优点。

停止计数寄存器的能力，高功率敏感的应用程序，在计数寄存器不是使用，或用于降低功率模式。此更改是必需的。增加了DI和EI指令，提供了自动禁用或启用中断的能力。这两种指令都是必需的。

在Cause寄存器中添加TI和PCI位来表示未执行的定时器和性能计数器rupts。此更改是必需的。

增加了一个执行危险序列，可以用来清除软件引入的危险写入协处理器0寄存器，影响中断系统状态。



#### 6.1 中断

第1版包括两个软件中断、六个硬件中断和两个专用中断:定时器和性能计数器。中断是通过一般方式处理的异常向量(偏移量0x180)或特殊中断向量(0x200)，基于CauseIV的值。软件需要对中断进行优先排序。版本2的架构为版本 1的中断架构增加了一个向上兼容的扩展，它支持端口矢量中断。另外，版本2增加了一个新的中断模式，通过改变中断体系结构来支持使用外部中断控制器。虽然不可屏蔽中断(NMI)在其名称中包含“中断”，但更准确地描述为NMI异常，因为它不影响，也不受处理器中断系统的控制。

只有当以下所有条件都为真时，才会触发中断:

一个特定的中断服务请求，作为中断模式的一个功能，如下所述。

状态寄存器中的IE位是1。

DM位在调试寄存器中是一个零(处理器实现EJTAG)

状态寄存器中的EXL和ERL位都为零。

逻辑上，中断服务请求与状态寄存器的IE位进行与操作。最后的中断请求只有当状态寄存器中的EXL和ERL位都为零，并且调试寄存器中的DM位为零时才会生效，分别对应于非异常、非错误、非调试处理模式。

##### 6.1.1 中断模式

三种中断模式：

中断兼容模式：向量偏移= 0x180。这个模式是必须的

矢量中断(VI)模式：增加了优先级和矢量中断的能力。该模式是可选的，由Config3寄存器中的VInt位表示。

外部中断控制器(EIC)模式：它重新定义了处理中断的方式，以提供完整的中断支持外部中断控制器处理中断的优先级和矢量。这种模式是可选的，它的存在是由Config3寄存器中的VEIC位表示的。

##### 6.1.1.1 中断兼容模式：

默认中断模式，当出现复位异常时，进入该模式。在这种模式下，中断是非矢量的，并且是分派的异常矢量偏移量0x180(如果CauseIV = 0)或矢量偏移量0x200(如果CauseIV = 1)以下条件为真:

- CauseIV = 0
- StatusBEV = 1
- IntCtlVS = 0，如果矢量中断没有实现，或者已经被禁用，就会出现这种情况。



中断兼容模式的典型软件处理程序可能如下所示:

```
IVexception:

mfc0 k0, C0_Cause /* 读取IP位的Cause寄存器 */   mfc0:从协处理器0的寄存器取值

mfc0 k1, C0_Status /* 与上IM位的状态寄存器 */

andi k0, k0, M_CauseIM /* 只保留Cause寄存器的IP位 */   
 
and k0, k0, k1 /*与上IM位的掩码 */  

beq k0, zero, Dismiss /* 没有位设置-虚假中断*/ 如果$k0的值等于$zero的值，跳转

clz k0, k0 /* 查找首位设置，IP7..IP0;k0 = 16 . .23 */

xori k0, k0, 0x17 /* 16..23 => 7..0 */

sll k0, k0, VS /* 切换到仿真软件IntCtlVS */

la k1, VectorBase /* 获取8个中断向量 */

addu k0, k0, k1 /* 计算基数和偏移量*/

jr k0 /* 跳转到特定的异常例程*/

nop 
```



每个中断处理程序处理一个特定的中断，如果有多个中断请求，则确定中断的实际来源是或在一个IP线上。一旦任务完成，中断有两种处理方式:

—完全处于中断级别(例如，一个简单的UART中断)。

—通过保存足够的状态和重新启用其他中断。



SimpleInterrupt:

在这里处理设备中断并清除中断请求查看设备。，一些寄存器可能需要保存和恢复。协处理器0状态像一个ERET，回到中断的代码。

NestedException:

嵌套异常通常需要保存EPC和Status寄存器

/* 在这里保存GPRs，并设置软件上下文 */

```
mfc0 k0, C0_EPC     /* 得到重启地址 */
```

```
sw k0, EPCSave     /* 保存在内存中 */

mfc0 k0, C0_Status    /* 获取状态值 */

sw k0, StatusSave    /* 保存在内存中 */

li k1, ~IMbitsToClear    /* 获得lm位来清除这个中断 */

and k0, k0, k1    /*清除状态副本中的位 */

ins k0, zero, S_StatusEXL, (W_StatusKSU+W_StatusERL+W_StatusEXL)

/*清除 k0里面的KSU, ERL, EXL 位  */

mtc0 k0, C0_Status /* 修改掩码，切换到内核模式，*/

/* 重新启用中断 */
```



为了完成中断处理，必须恢复保存的值并且重新启动原来的中断代码。

```
di                                    /* 禁用中断——可能不需要 */

lw k0, StatusSave            /* 获取保存状态(包括EXL设置)) */

lw k1, EPCSave          /*and   EPC */

mtc0 k0, C0_Status   /* 还原原来的值 */

mtc0 k1, C0_EPC       /* and EPC */

/* 恢复GPRs和软件状态 */

eret         /* 取消中断t */
```



##### 6.1.1.2矢量中断模式

​		矢量中断模式建立在中断兼容模式的基础上，它添加了一个优先级编码器来为等待中断排序，并生成一个矢量，每个中断都可以被定向到专用的处理程序。这模式还允许将每个中断映射到一个GPR阴影集，供中断处理程序使用。矢量中断模式在以下条件都为真时生效:

- Confifig3VInt = 1
- Confifig3VEIC = 0
- IntCtlVS ≠ 0

-  CauseIV = 1
-  StatusBEV = 0

​		在VI中断模式中，六个硬件中断被解释为单独的硬件中断请求。计时器性能计数器中断以一种依赖于实现的方式与硬件中断相结合(与中断结合，它们分别由IntCtlIPTI和IntCtlIPPCI指示)来提供这些中断与硬件中断的适当的相对优先级。处理器中断逻辑将每个CauseIP位与对应的StatusIM位进行匹配。如果这些值中的任何一个是1，并且如果中断是enabled (StatusIE = 1, StatusEXL = 0, StatusERL = 0)，中断被通知，

​		优先级顺序对每个硬件中断和软件中断赋予一个低于所有硬件中断的相对优先级。当优先级编码器找到最高优先级等待中断时，它输出一个编码的矢量号，用于计算该中断的处理程序。

​		矢量中断模式的典型软件处理程序绕过了上面兼容性模式处理程序显示的IVexception标签后面的整个代码序列。相反，硬件执行优先级，直接调度到中断处理程序。与兼容性模式的例子不同，矢量中断处理器可以利用一个专用的GPR阴影设置来避免保存任何寄存器。因此，上面显示的SimpleInterrupt代码不需要保存GPRs。

​		嵌套中断类似于兼容性模式下显示的中断，但也可以利用运行在GPR阴影集中的嵌套的异常程序，专用于中断或在另一个阴影集中。这样的程序可能如下所示:

NestedException:

／嵌套异常通常需要保存EPC, Status和SRSCtl寄存器，设置适当的GPR阴影设置为例行程序，禁用适当的IM位在状态，以防止一个中断循环，放置处理器为内核模式，并重新启用中断。示例代码：

／/使用当前GPR阴影设置，并设置软件上下文/

`mfc0 k0, C0_EPC             /获取重启地址/`

`sw k0, EPCSave              /保存到内存/`

`mfc0 k0, C0_Status      /获取状态值/`

`sw   k0,   StatusSave     /保存到内存/`

`mfc0    k0, C0_SRSCtl      /如果更改阴影设置，则保存SRSCtl`  

`sw  k0,  SRSCtlSave`  

`li     k1  ,  ~IMbitsToClear    //获取Im位来清除中断，` 

`and k0, k0, k1      /清除状态副本中的位` 

`ins       k0,  zero ,  S_StatusEXL (W_StatusKSU+W_StatusERL+W_StatusEXL)       //  清除k0中的KSU, ERL, EXL位`

`mtc0 k0, C0_Status      //修改掩码，切换到内核模式`



这里进程中断，包括清除设备中断

为了完成中断处理，必须恢复保存的值并且重新启动原来的中断代码。

`di                                /* 禁用中断——可能不需要 */`

`lw k0, StatusSave    /* 保存状态(包括EXL集) */`

`lw k1, EPCSave        /* and EPC */`

`mtc0 k0, C0_Status    /* 还原原值 */`

`lw k0, SRSCtlSave     /* Get saved SRSCtl */`

`mtc0 k1, C0_EPC     /* and EPC */`

`mtc0 k0, C0_SRSCtl       /   * 恢复阴影集 */`

`ehb                                 /* 明确风险   */`

`eret                                /* 解除中断 */`



##### 6.1.1.3外部中断控制器模式

​		外部中断控制器模式重新定义了配置处理器中断逻辑的方式，以提供对外部中断控制器的支持。中断控制器负责确定所有中断的优先级，包括硬件、软件、计时器和性能计数器中断，并直接向处理器提供矢量最高优先级中断的编号(可选的优先级级别)。EIC中断模式是在所有的以下条件为真时实现:

-  Confifig3VEIC = 1
-  IntCtlVS ≠ 0
-  CauseIV = 1
-  StatusBEV = 0

​		在EIC中断模式下，处理器将软件中断请求(CauseIP1..IP0)、定时器中断请求(CauseTI)和性能计数器中断请求(CausePCI)的状态发送给外部中断控制器，它以一种依赖于系统的方式将这些中断与其他硬件中断进行优先级排序。中断控制器可以是一个硬连接的逻辑块，或者它可以基于控制和状态寄存器进行配置。这允许中断控制器要更具体或更一般地作为系统环境的一个功能而需要。

​		外部中断控制器对其中断请求进行优先级排序，并产生优先级级别和向量号要服务的最高优先级中断。优先级，称为请求中断优先级(RIPL)， 6位编码，取值范围为0....63。0表示没有等待的中断请求。值1 . .63表示要服务的中断的最低(1)到最高(63)RIPL。中断控制器将此值传递到6个硬件中断线路上，在EIC中断中将其作为一个编码值处理模式。矢量偏移量有几个实现选项:

1. 第一种选择是将RIPL值作为处理器的向量数。
2. 第二个选项是将一个单独的矢量号和RIPL一起发送给处理器。
3. 第三种选择是将整个矢量偏移量随RIPL一起发送给处理器。

​		StatusIPL(覆盖StatusIM7..IM2)被解释为当前正在运行(值为零表示当前没有正在服务的中断)处理器所在的中断优先级级别(IPL)。当中断控制器请求一个中断服务，处理器会比较RIPL和StatusIPL，以确定请求中断的优先级是否高于当前的IPL。如果RIPL确实大于StatusIPL，并且中断被启用(StatusIE = 1, StatusEXL = 0, StatusERL = 0)一个中断请求被发送到管道。当处理器开始中断异常，它加载RIPL到CauseRIPL(覆盖CauseIP7..IP2)，并向外部发出信号中断控制器来通知它请求正在被执行。因为CauseRIPL只有当发出中断异常被信号时才被处理器加载，它在中断处理期间对软件可用。这个向量进入核心的EIC与IntCtlVS相结合，以确定中断服务例程在哪里。向量号不存储在任何软件可见的寄存器中。一些实现可能会选择使用将RIPL作为矢量号，但这不是必需的。

​		典型的EIC中断模式软件处理程序绕过IVexception之后的整个代码序列上面显示的兼容性模式处理程序的标签。相反，硬件执行优先级排序和调度直接到中断处理程序。与兼容性模式的例子不同，EIC中断处理程序可以利用一个专用的GPR阴影设置来避免保存到任何寄存器。因此，显示了SimpleInterrupt代码以上不需要保存GPRs。



NestedException:

/*嵌套异常通常需要保存EPC、Status和SRSCtl寄存器，为例程设置适当的GPR阴影集，禁用状态中相应的IM位，以防止一个中断循环，把处理器放置在内核模式，并重新启用中断。示例代码：

/* 使用当前的GPR阴影集，并设置软件上下文 */

`mfc0  k1,  C0_Cause    /*  读取Cause寄存器获取RIPL值*/`

`mfc0   k0 ,  C0_EPC        / * 获得重启地址  */`

`srl     k1,  k1 ,  S_CauseRIPL     /* 右对齐RIPL字段*/`

`sw    k0  ,  EPCSave      /* 保存在内存中 */`

`mfc0    k0,  C0_Status    /* 获取状态值 */`

`sw   k0,  StatusSave   /* 保存在内存中 */`

`ins   k0,  k1,  S_StatusIPL,  6   /* 在状态副本中设置IPL为RIPL */`

`mfc0   k1,  C0_SRSCtl    /*如果更改阴影集，则保存SRSCtl */`

`sw    k1,  SRSCtlSave`  

`/*如果切换阴影集，在这里将新的值写入SRSCtlPSS */`

`ins   k0,  zero,  S_StatusEXL, (W_StatusKSU+W_StatusERL+W_StatusEXL)`

`/* 清除k0中的KSU, ERL, EXL位*/`

`mtc0  k0,  C0_Status  /* 修改IPL，切换到内核模式, */`

`/* 重新启用中断 */`



如果切换阴影集，只清除上面的KSU，写入目标地址到EPC，并执行一个eret清除EXL，切换阴影集，并跳转到例行程序。

进程中断在这里，包括清除设备中断。

中断完成代码与上面VI模式中显示的代码相同。



#### 6.1.2为矢量中断生成异常矢量偏移量

​		对于矢量中断(在VI或EIC中断模式-选项1和2中)，一个矢量号由中断控制逻辑产生。这个数字与IntCtlVS结合来创建中断偏移，它被添加到0x200来创建异常向量偏移量。对于VI中断模式，矢量数的范围是0到7范围内。对EIC中断模式，矢量数在范围1到63，包含(0是“没有中断”的编码)。IntCtlVS字段指定向量位置之间的间距。如果该值为零(默认重置状态)，则向量间距为零，处理器恢复到中断兼容模式。非零值启用向量间的向量数和值的一个代表性子集的异常向量偏移量。

​		矢量中断的异常矢量偏移量的一般方程为:vectorOffset←0x200 + (vectorNumber × (IntCtlVS || 0b00000))



##### 6.1.2.1软件危害和中断系统

​		软件写入协处理器0寄存器字段可能会改变中断发生的条件。这将创建一个协处理器0 (CP0)风险。第1版在体系结构中，没有体系结构定义的方法在导致中断状态改变的指令之后来限定指令的数量，在改变到中断状态之前执行的情况是有的。在该架构的第2版中，添加了EHB指令，该指令可由软件使用以清除风险。

一个EHB，在这些字段之一被列出的指令修改后执行，使中断的改变状态不迟于EHB之后的指令可见。

在以下示例中，对CauseIM字段的更改会被EHB显示:

`mfc0    k0,   C0_Status`

`ins   k0,  zero,   S_StatusIM4, 1   /* 清除IM字段第4位 */`

`mtc0    k0 ,  C0_Status     /* 重写寄存器 */`

`ehb          /* 明确风险 */`

/* 中断状态的改变不会晚于此指令*/

类似地，DI指令的效果在EHB中是可见的:

`di             /* 禁用中断*/`

`ehb           /清除风险*/`

/改变到中断状态是不晚于这个指令的*/



#### 6.2异常

​		当异常发生时，指令的正常执行可能会中断。这样的事件可以生成为指令执行的副产物(例如，由add指令引起的整数溢出或由a加载指令)，或者由与指令执行不直接相关的事件(例如外部中断)触发。当一个异常发生时，处理器停止处理指令，保存足够的状态以恢复中断的指令流，进入内核模式，并启动软件异常处理程序。保存的状态和软件异常处理程序的地址是异常类型和处理器当前状态的函数。

##### 6.2.1异常优先级

表6.6列出了所有可能的异常，以及每个异常的相对优先级，从高到低。

​										表6.6异常优先级

| 异常                 | 描述                                                         | 类型       |
| -------------------- | ------------------------------------------------------------ | ---------- |
| 复位                 | cold复位信号被断言给处理器                                   | 异步重置   |
| 软复位               | 复位信号被断言给处理器                                       |            |
| 单步调试             | 出现EJTAG单步。优先于其他异常(包括异步异常)，<br>以便可以单步进入中断(或其他异步)处理程序。 | 同步调试   |
| 调试中断             | 一个EJTAG中断(EjtagBrk或DINT)被断言                          | 异步调试   |
| 不精确的调试数据中断 | 一个不精确的EJTAG数据中断条件被断言                          |            |
| 不可屏蔽中断(NMI)    | NMI信号被断言给处理器                                        | 异步       |
| 机器检查             | 处理器检测到内部不一致                                       |            |
| 中断                 | 一个已启用的中断发生                                         |            |
| 延迟监视             | 一个监视异常，延迟是因为EXL是一个异常，在EXL归零后断言。     |            |
| 调试指令中断         | 一个EJTAG指令中断条件被断言。优先上述指令获取异常以允许中断非法的指令地址。 | 同步调试   |
| Watch -指令获取      | 在取指令时检测到监视地址匹配。优先级高于指令获取异常，以允许监视非法指令地址。 | 同步       |
| 地址错误-获取指令    | PC中加载了一个非字对齐的地址                                 |            |
| TLB重新读取指令      | 在取指令时发生TLB错误。                                      |            |
| TLB无效-取指令       | 在TLB表项中，有效位为0，该表项映射的地址是指令获取所引用的地址。 |            |
| TLB Execute-Inhibit  | 一个指令获取匹配了一个有效的TLB条目，该条目具有XI位设置。    |            |
| 缓存错误-取指令      | 取指令时发生缓存错误。                                       |            |
| 总线错误-取指令      | 在取指令时发生总线错误。                                     |            |
| SDBBP                | 执行EJTAG SDBBP指令                                          | 同步调试   |
| 有效异常的指令       | 指令无法完成，因为它是不允许的访问所需的资源，或者是非法的:协处理器不可用，保留指令。<br>如果两个异常同时发生指令，协处理器不可用异常优先在保留指令异常上。 | 同步       |
| 执行异常             | 发生了一个基于指令的异常:整数溢出、trap、系统调用，断点，浮点数，协处理器2异常。 |            |
| 精确调试数据中断     | 加载/存储时精确的EJTAG数据中断(仅地址匹配)或断言存储(地址+数据匹配)条件上的数据中断。<br>优先于数据获取异常，以允许中断非法数据地址。 | 同步调试   |
| Watch-数据存取       | 在引用的地址上检测到监视地址匹配装载或储存优先级高于数据获取异常允许监视非法数据地址。 | 同步       |
| 地址错误-数据访问    | 未对齐的地址，或在当前处理器模式被加载或存储指令引用         |            |
| TLB填充—数据访问     | 在数据访问时发生TLB缺失                                      |            |
| TLB无效-数据访问     | 在映射由加载或存储指令保护的地址引用的TLB条目中，有效位为零  |            |
| TLB Read-Inhibit     | 数据读访问匹配了一个有效的TLB表项，该表项设置了RI位。        |            |
| TLB修改-数据访问     | 在映射由store指令引用的地址的TLB表项中，dirty位为零          |            |
| 缓存错误-数据访问    | 加载或存储数据引用时发生缓存错误                             | 同步或异步 |
| 总线错误-数据访问    | 在加载或存储数据引用上发生总线错误                           |            |



表6.7的“类型”一列描述了异常的类型。表6.8解释了每种异常类型的特征。

| 异常类型 | 特征                                                         |
| -------- | ------------------------------------------------------------ |
| 异步复位 | 指示指令执行时异步发生的复位类型异常。这些异常总是具有最高的优先级，<br>以保证处理器可以始终处于可运行状态 |
| 异步调试 | 指示指令执行时异步发生的EJTAG调试异常。这些异常相对于其他异常具有很高的优先级，<br>因为进入调试模式的愿望，即使在存在其他异常的情况下，异步同步和同步。 |
| 异步     | 指示指令执行时异步发生的任何其他类型的异常。这些异常比同步异常具有更高的优先级主要是为了符号方便。<br>如果有人认为异步异常发生在指令之间，那么它们要么是相对于前一个的最低优先级指令，或相对于下一条指令的最高优先级。 |
| 同步调试 | 表示由于指令执行而发生的EJTAG调试异常，并且精确地报告导致异常的指令。这些异常优先级高于其他同步异常，<br>以允许进入Debug模式，即使存在其他例外。 |
| 同步     | 表示由于指令执行而发生的任何其他异常精确地报告了导致异常的指令。这些异常的优先级往往低于其他类型的异常，<br>但有一个相对同步异常之间的优先级。 |



##### 6.2.2异常矢量位置

​		复位、软复位和NMI异常总是指向位置0xBFC0.0000。EJTAG Debug异常被导向位置0xBFC0.0480，或者位置0xFF20.0200如果ProbTrap位是0或1，分别在EJTAG_Control_register中。

​		所有其他异常的地址都是一个向量偏移量和一个向量基地址的组合。版本1结构上，矢量基址固定。在体系结构的版本2(以及随后的版本)中，软件是允许通过EBase寄存器指定向量的基地址，用于StatusBEV等于0时发生的异常。表6.8给出了vector base address作为异常的函数，以及是否在Status寄存器中设置了BEV位。表6.9给出了异常与向量基地址的偏移量。注意静脉点滴在Cause寄存器导致interrupt使用专用的异常向量偏移量，而不是通用的异常向量。对于体系结构的发布版2(以及随后的发布版)的实现，表6.4给出了从在StatusBEV = 0和CauseIV = 1的情况下。用于实现体系结构的第1版其中CauseIV = 1，向量偏移量相当于IntCtlVS为0。

​		表6.10将这两个表合并成一个包含所有可能的向量地址的状态函数会影响矢量选择。为了避免表中的复杂性，vector地址值假设EBase寄存器(就像在Release 2设备中实现的那样)没有从其重置状态更改，并且IntCtlVS为0。

​		在体系结构的版本2(以及后续版本)中，软件必须保证EBase15..12中包含0小于或等于矢量偏移中最有效位的所有位的位置。这种情况只会发生在启用VI或EIC中断模式的中断发生时，产生大于0xFFF的面偏移量。的如果不满足这个条件，处理器的操作是未定义的。

​                                                  表6.8例外向量基地址

 ![image-20211119155406758](https://gitee.com/halley5/documents/raw/master/img/interrupt/1.png)

​                表6.9异常向量偏移量

![image-20211119155452571](https://gitee.com/halley5/documents/raw/master/img/interrupt/2.png)

表6.10异常向量

![image-20211119155616529](https://gitee.com/halley5/documents/raw/master/img/interrupt/3.png)



##### 6.2.3一般异常处理

除Reset、Soft Reset、NMI、cache error、EJTAG Debug异常(如下所示)有各自的特殊处理外，异常的基本处理流程是相同的:

- 如果Status寄存器中的EXL位为零，则EPC寄存器被装入PC中，执行将在PC中执行重启，BD位在Cause寄存器中被适当设置。值加载进入EPC寄存器取决于处理器是否实现MIPS16 ASE，以及是否指令位于具有延迟槽的分支或跳转的延迟槽中。表6.11显示了存储在每个CP0 PC寄存器，包括EPC。对于架构的发布版2的实现，如果StatusBEV= 0时，将SRSCtl寄存器中的CSS字段复制到PSS字段，并从适当的源加载CSS值。
- 如果设置了状态寄存器中的EXL位，则没有加载EPC寄存器，并且在中没有更改BD位导致注册。对于架构的第2版的实现，SRSCtl寄存器没有改变。

##### 表6.11  在异常中存储在EPC、errorrepc或DEPC中的值

| MIPS16是否实施？ | 在分支/跳延迟槽? | 存储在EPC/ errorrepc /DEPC中的值                             |
| ---------------- | ---------------- | ------------------------------------------------------------ |
| NO               | NO               | 指令地址                                                     |
| NO               | YES              | 分支或跳转指令的地址(PC-4)                                   |
| YES              | NO               | 指令地址的上31位组合与ISA模式位                              |
| YES              | YES              | 分支或跳转指令的上31位(PC-2)MIPS16 ISA模式和32位ISA中的PC-4模式)，<BR>结合ISA模式位 |

- Cause寄存器的CE和ExcCode字段装载了适合异常的值。的对于除协处理器不可用异常之外的任何异常类型，CE字段被加载，但没有定义。
- EXL位在状态寄存器中设置。
- 处理器在异常向量处启动。

加载到EPC中的值表示异常的重新启动地址，不需要由异常修改正常情况下的处理程序软件。软件不需要查看原因寄存器中的BD位，除非它愿意识别导致异常的指令的地址。

注意，个别异常类型可能会将附加信息加载到其他寄存器中。下面对每种异常类型的描述中对此进行了说明。

Operation:

/* 如果StatusEXL为1，则所有异常都通过一般异常向量 ，并且EPC、CauseBD和SRSCtl都没有被修改*/

`if StatusEXL = 1 then`

​	`vectorOffset ← 0x180`

`else`

​	`if InstructionInBranchDelaySlot then`

​		`EPC ← restartPC/* PC of branch/jump */`

​		`CauseBD ← 1`

​	`else`

​		`EPC ← restartPC /* PC of instruction */`

​		`CauseBD ← 0`

`endif`

`/* 作为异常类型的函数计算向量偏移量 */`

`NewShadowSet ← SRSCtlESS` 

`if ExceptionType = TLBRefill then`

​	`vectorOffset ← 0x000`

`else if  (ExceptionType = Interrupt)  then`

​	`if (CauseIV = 0) then`

​		`vectorOffset ← 0x180`

​	`else`

​		`if (StatusBEV = 1) or (IntCtlVS = 0) then`

​			`vectorOffset ← 0x200`

​		`else`

​		`if Config3VEIC = 1 then`

​			`if (EIC_option1)`

​				`VecNum ← CauseRIPL`

​			`elseif (EIC_option2)`

​				`VecNum ← EIC_VecNum_Signal`

​			`endif`

​			`NewShadowSet ← SRSCtlEICSS`

​	`else`

​			`VecNum ← VIntPriorityEncoder()`

​			`NewShadowSet ← SRSMapIPL×4+3..IPL×4`

​	`endif`

​	`if (EIC_option3)`

​			`vectorOffset ← EIC_VectorOffset_Signal`

​	`else`

​			`vectorOffset ← 0x200 + (VecNum × (IntCtlVS || 0b00000))`

​	`endif`

`endif   /* if (StatusBEV = 1) or (IntCtlVS = 0) then */*`

`endif   /* if (CauseIV = 0) then */`

`endif   /* elseif (ExceptionType = Interrupt) then */`

`/*更新一个实现的阴影集信息  */`

`if (ArchitectureRevision ≥ 2) and (SRSCtlHSS > 0) and (StatusBEV = 0) then`

​		`SRSCtlPSS ← SRSCtlCSS`

​		`SRSCtlCSS ← NewShadowSet`

​	`endif`

`endif /* if StatusEXL = 1 then */`

`CauseCE ← FaultingCoprocessorNumber`

`CauseExcCode ← ExceptionType`

`StatusEXL ← 1`

`/ * 计算向量的基址*/`

`if StatusBEV = 1 then`

​	`vectorBase ← 0xBFC0.0200`

`else`

​	`if ArchitectureRevision ≥ 2 then`

​	`/* EBase31的固定值..30强制基地位于kseg0或kseg1 */`

​		`vectorBase ← EBase31..12 || 0x000`

​	`else`

​		`vectorBase ← 0x8000.0000`

​	`endif`

`end`

/* 异常PC是vectorBase和vectorOffset的和。向量 offset > 0xFFF (只中断矢量或EIC）， 需要 EBase15 . .12在每个位的位置上小于或等于矢量偏移的最有效位位置 */



##### 6.2.4 EJTAG调试异常

当满足多个EJTAG相关条件中的一个时，就会发生EJTAG调试异常。请参考EJTAG此异常的详细说明。

##### 输入向量使用

如果EJTAG_Control_register中的ProbTrap位为零，则为0xBFC0 0480;如果是，则0xFF20 0200一个。



##### 6.2.5复位异常

​		一个复位异常发生时，cold复位信号被断言给处理器。这个异常是不可屏蔽的。当一个重置异常发生时，处理器执行一个完全重置初始化的操作，包括中止状态机，建立临界状态，通常将处理器置于可以在未缓存，未映射的地址空间执行指令的状态。在复位异常中，只有以下寄存器定义了状态:

- Random寄存器被初始化为TLB表项的数目- 1。
- Wired寄存器被初始化为0。
- Config、Config1、Config2和Config3寄存器用它们的启动状态初始化。
- 状态寄存器的RP、BEV、TS、SR、NMI和ERL字段被初始化为指定的状态。
- 监视寄存器启用和性能计数器寄存器中断启用被清除。
- errrepc寄存器与重启PC一起加载，如表6.11所示。注意这个值可能是不可预测的，如果复位异常被认为是应用到处理器的功率的结果，因为在这种情况下，PC可能没有有效值。在某些实现中，加载到errorrepc寄存器中的值在复位或软复位异常上可能无法预测。
- PC加载0xBFC0 0000。

##### Cause寄存器ExcCode的值：

None

##### 额外的状态保存

None

##### 输入向量的使用

Reset (0xBFC0 0000)

##### 操作

`Random ← TLBEntries - 1`

`PageMaskMaskX ← 0 # 1KB page support implemented`

`PageGrainESP ← 0 # 1KB page support implemented`

`Wired ← 0`

`HWREna ← 0`

`EntryHiVPN2X ← 0 # 1KB page support implemented`

`StatusRP ← 0`

`StatusBEV ← 1`

`StatusTS ← 0`

`StatusSR ← 0`

`StatusNMI ← 0`

`StatusERL ← 1`

`IntCtlVS ← 0`

`SRSCtlHSS ← HighestImplementedShadowSet`

`SRSCtlESS ← 0`

`SRSCtlPSS ← 0`

`SRSCtlCSS ← 0`

`SRSMap ← 0`

`CauseDC ← 0`

`EBaseExceptionBase ← 0`

`Config ← ConfigurationState`

`ConfigK0 ← 2 # Suggested - see Config register description`

`Config1 ← ConfigurationState`

`Config2 ← ConfigurationState`

`Config3 ← ConfigurationState`

`WatchLo[n]I ← 0 # For all implemented Watch registers`

`WatchLo[n]R ← 0 # For all implemented Watch registers`

`WatchLo[n]W ← 0 # For all implemented Watch registers`

`PerfCnt.Control[n]IE ← 0 # For all implemented PerfCnt registers`

`if InstructionInBranchDelaySlot then`

​	`ErrorEPC ← restartPC # PC of branch/jump`

`else`

​	`ErrorEPC ← restartPC # PC of instruction`

`endif`

`PC ← 0xBFC0 0000`



##### 6.2.6软复位异常

​		软复位异常发生时，复位信号断言给处理器。这个异常是不可屏蔽的。当软复位异常发生时，处理器执行完整复位初始化的一个子集。虽然软复位中断不会不必要地改变处理器的状态，它可能会被迫这样做以便放置一种可以从未缓存的、未映射的地址空间执行指令的处理器状态。因为总线,缓存，或其他操作可能中断，部分缓存，内存，或其他处理器状态可能不一致。

​		复位和软复位异常的主要区别是在实际使用中。复位异常是典型的用于在上电时初始化处理器，而软复位异常通常用于从没有响应(挂)处理器中恢复。提供语义上的差异是为了允许引导软件保存关键协处理器0或其他寄存器状态，以帮助调试潜在的问题。因此，处理器可以重置相同的状态，但是软件保存的任何状态的解释可能有很大的不同。

除了任何硬件初始化需要，以下状态建立在软复位异常：

- 状态寄存器的RP、BEV、TS、SR、NMI和ERL字段被初始化为指定的状态。
- 监视寄存器启用和性能计数器寄存器中断启用被清除.
- 在重启PC中加载errorpc寄存器，如表6.11所示。
- PC加载0xBFC0 0000.

##### Cause Register ExcCode Value

None

##### Additional State Saved

None

##### Entry Vector Used

Reset (0xBFC0 0000)



##### Operation

`PageMaskMaskX ← 0            # 实现了1KB页面支持`

`PageGrainESP ← 0               # 实现了1KB页面支持`

`EntryHiVPN2X ← 0               # 实现了1KB页面支持`

`ConfigK0 ← 2                       # Suggested - see Config register description`

`StatusRP ← 0`

`StatusBEV ← 1`

`StatusTS ← 0`

`StatusSR ← 1`

`StatusNMI ← 0`

`StatusERL ← 1`

`WatchLo[n]I ← 0                      # 所有已实现的Watch寄存器`

`WatchLo[n]R ← 0            #所有已实现的Watch寄存器`

`WatchLo[n]W ← 0                  # 所有已实现的Watch寄存器`

`PerfCnt.Control[n]IE ← 0               # 所有已实现的Watch寄存器`

`if InstructionInBranchDelaySlot then`

`ErrorEPC ← restartPC                    # PC的分支/跳转`

`else`

`ErrorEPC ← restartPC                      # PC指令`

`endif`

`PC ← 0xBFC0 0000`



#### 6.2.7 NMI异常

​		当NMI信号被断言到处理器时，一个不可屏蔽的中断异常发生。虽然它被描述为一个中断，但更准确地描述为一个异常，因为它是不可屏蔽的。一个NMI异常只在指令边界发生，因此不进行任何重置或其他硬件初始化。缓存的状态，内存和其他处理器状态是一致的，并且所有寄存器都被保留，但有以下异常:

- Status寄存器的BEV、TS、SR、NMI和ERL字段被初始化为指定的状态。
- 在重启PC时加载errorpc寄存器，如表6.11所示。
- PC加载0xBFC0 0000。

##### Cause Register ExcCode Value

None

##### Additional State Saved

None

##### Entry Vector Used

Reset (0xBFC0 0000)

##### Operation

`StatusBEV ← 1`

`StatusTS ← 0`

`StatusSR ← 0`

`StatusNMI ← 1`

`StatusERL ← 1`

`if InstructionInBranchDelaySlot then`

`ErrorEPC ← restartPC            # PC的分支/跳转`

`else`

`ErrorEPC ← restartPC           # PC指令`

`endif`

`PC ← 0xBFC0 0000`



#### 6.2.8机器检查异常

当处理器检测到内部不一致时，机器检查异常发生。以下情况导致机器检查异常:

- 在基于TLB的MMU中检测TLB中的多个匹配项。

##### Cause 寄存器 ExcCode的值

MCheck 

##### 额外的状态保存

取决于引起异常的条件。

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### 6.2.9地址错误异常

地址错误异常发生在以下情况:

- 从没有在字边界对齐的地址获取指令。加载或存储字指令执行时，地址不在字边界上对齐。
- 加载或存储半字指令执行时，地址不在半字边界上对齐。
- 从用户模式或管理器模式引用内核地址空间。
- 从用户模式中引用管理器地址空间。

注意，在指令取取没有在字边界对齐的情况下，PC在检测到条件之前被更新。因此，EPC和BadVAddr都指向未对齐的指令地址。

##### Cause 寄存器 ExcCode的值

AdEL:Reference是一个加载或一个指令获取

AdES: Reference 是一个加载

##### 额外的状态保存

| 寄存器状态    | 值            |
| ------------- | ------------- |
| BadVAddr      | 失败地址      |
| Context(VPN2) | UNPREDICTABLE |
| EntryHi(VPN2) | UNPREDICTABLE |
| EntryLo0      | UNPREDICTABLE |
| EntryLo1      | UNPREDICTABLE |

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### 6.2.10 TLB重新填充异常

​		在基于TLB的MMU中，当没有TLB表项与映射地址的引用匹配时，在状态寄存器中发生TLB重新填充异常，EXL位为零。注意，这与条目匹配的情况不同但是无效位关闭，在这种情况下发生TLB 错误异常。

##### Cause寄存器 ExcCode的值

TLBL:引用是一个加载或一个指令获取

TLBS: 引用是一个加载

##### 额外的状态保存

| 寄存器状态 | 值                                                           |
| ---------- | ------------------------------------------------------------ |
| BadVAddr   | 失败地址                                                     |
| Context    | 如果设置了Config3CTXTC位，则上下文注册表的位对应于VirtualIndex字段的设置位ContextConfig<br>寄存器是用高阶加载的丢失的虚拟地址的位。如果Config3CTXTC位是清楚的，那么BadVPN2字段包含失败地址中的VA31..13 |
| EntryHi    | VPN2字段包含失败地址的VA31..13;的ASID字段包含错过的引用的ASID。 |
| EntryLo0   | UNPREDICTABLE                                                |
| EntryLo1   | UNPREDICTABLE                                                |

##### Entry Vector Used

- 如果在异常时StatusEXL = 0，则TLB重新填充向量(偏移0x000)。

- 一般异常向量(偏移0x180)，如果StatusEXL = 1在异常时间。

  

#### 6.2.11 Execute-Inhibit异常

​		当取指令的虚拟地址与其XI . TLB表项匹配时，执行抑制异常发生一些设置。只有当XI位在TLB中实现并启用时，才会发生这种异常类型由PageGrainXIE位表示。

##### Cause寄存器 ExcCode的值

if *PageGrain*IEC == 0 TLBL

if *PageGrain*IEC == 1 TLBXI

##### Entry Vector Used

一般异常向量(偏移量0x180)



##### 6.2.12 Read-Inhibit异常

​		当内存负载引用的虚拟地址与某个TLB表项匹配时，会发生Read-Inhibit异常设置RI位。只有在TLB中实现了RI位并使能时，才会出现这种由PageGrainRIE位表示异常类型。MIPS16 pc相对负载是一种特殊情况，不受RI位的影响。

##### Cause寄存器 ExcCode的值

if *PageGrain*IEC == 0 TLBL

if *PageGrain*IEC == 1 TLBRI

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### 6.2.13 TLB无效异常

当TLB表项与映射地址空间的引用匹配时，TLB无效异常入口的有效位被关闭。

注意，在没有TLB项匹配映射地址空间的引用且EXL位为1的情况下在状态寄存器中与TLB无效异常没有区别，在某种意义上，两者都使用常规异常向量并提供TLBL或TLBS的ExcCode值。区分这两种情况的唯一方法是在TLB中搜索以找到匹配的条目(使用TLBP)。

如果RI和XI位是在TLB中实现的，并且PageGrainIEC位是清晰的，那么这个异常也是如果在内存负载引用上找到一个设置了RI位或设置了XI位的有效的、匹配的TLB项，则发生在一个指令上取内存标号。MIPS16 pc相对负载是一个特例，不受RI的影响一些。

##### Cause寄存器 ExcCode的值

TLBL:引用一个加载或一个指令获取

TLBS: 引用是一个加载

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### 6.2.14 TLB修改异常

当匹配的TLB项有效时，对映射地址的存储引用发生TLB修改异常，但条目的D位为零，表示该页不可写。

##### Cause寄存器 ExcCode的值

Mod

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### 6.2.15缓存错误异常

​		当指令或数据引用检测到缓存标签或数据错误，或奇偶校验或时，当有缓存错误发生时发生缓存错误异常，系统总线检测到ECC错误。这个异常是不可屏蔽的。因为这个错误在缓存中，异常向量指向未映射、未缓存的地址。

##### Cause寄存器 ExcCode的值

N/A

##### 额外的状态保存

CacheErr                Error state

ErrorEPC                Restart PC

##### Entry Vector Used

缓存错误向量(偏移0x100)

##### Operation

`CacheErr ← ErrorState`

`StatusERL ← 1`

`if InstructionInBranchDelaySlot then`

`ErrorEPC ← restartPC      # PC的分支/跳转`

`else`

`ErrorEPC ← restartPC       # PC指令 `

`endif`

`if StatusBEV = 1 then`

`PC ← 0xBFC0 0200 + 0x100`

`else`

`if ArchitectureRevision ≥ 2 then`

`/* EBase31的固定值。30位和29位强制为1，将向量放入kseg1 */`

`PC ← EBase31..30 || 1 || EBase28..12 || 0x100`

`else`

`PC ← 0xA000 0000 + 0x100`

`endif`

`end`



#### 6.2.16总线错误异常

​		当指令、数据或预取访问发出总线请求(由于缓存丢失或预取请求)时，总线错误发生不可缓存引用)，该请求在错误中终止。注意，在总线事务期间检测到的奇偶校验错误被报告为缓存错误异常，而不是总线错误异常。

##### Cause寄存器 ExcCode的值

IBE: 指令引用错误

DBE: 数据引用错误

##### Additional State Saved

None

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### 6.2.17整数溢出异常

当所选整数指令导致2的补码溢出时，将发生整数溢出异常。

##### Cause寄存器 ExcCode的值

Ov

##### Additional State Saved

None

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### **6.2.18 Trap 异常**

当trap指令的结果为TRUE时，将发生trap异常



##### Cause寄存器 ExcCode的值

Tr

##### Additional State Saved

None

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### 6.2.19系统调用异常

系统调用异常发生在执行SYSCALL指令时。



##### Cause寄存器 ExcCode的值

Sys

##### Additional State Saved

None

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### 6.2.20断点异常

当执行BREAK指令时，会发生断点异常。



##### Cause寄存器 ExcCode的值

Bp

##### Additional State Saved

None

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### 6.2.21保留指令异常

如果下列任何一个条件为真，则会发生保留指令异常:

- 执行了一条指令，该指令指定了用“∗”(保留)标记的操作码字段的编码，“β”(高阶ISA)，或未实现的“ε”(ASE)。
- 执行了一条指令，该指令指定了标记的函数字段的SPECIAL操作码编码“∗”(保留)，或“β”(高阶ISA)。
- 执行了一条指令，该指令指定了标记为“∗”的rt字段的一个regime操作码编码。(保留)。
- 执行了一条指定函数字段的未实现的SPECIAL2操作码编码的指令标记为未实现的“θ”(可用的合作伙伴)或未实现的“σ”(EJTAG)。
- 执行了一条指令，指定了用“∗”标记的rs字段的COPz操作码编码。(保留)，“β”(高阶ISA)，或未实现的“ε”(ASE)，假设对协处理器的访问是允许的。如果不允许访问协处理器，则会发生协处理器不可用异常。为COP1操作码，以前的ISAs的一些实现将这种情况报告为浮点异常，设置FCSR寄存器的原因字段中的未实现操作位。
- 执行了一条指定函数字段的未实现的COP0操作码编码的指令rs是标记为“∗”(保留)或未实现的“σ”(EJTAG)的CO，假设允许访问coprocessor 0。如果不允许访问协处理器，就会发生协处理器不可用异常代替。
- 执行了一条指令，该指令指定了用“∗”标记的函数字段的COP1操作码编码(保留)、“β”(高阶ISA)或未实现的“ε”(ASE)，假设对协处理器1的访问是允许的。如果不允许访问协处理器，则会发生协处理器不可用异常。一些以前ISAs的实现将此情况报告为浮点异常，并设置未实现FCSR寄存器Cause字段中的操作位。



##### Cause寄存器 ExcCode的值

RI

##### Additional State Saved

None

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### 6.2.22协处理器不可用异常

如果下列任何一个条件为真，就会发生协处理器不可用异常:

- 当处理器在除调试模式以外的模式下运行时，执行了COP0或Cache指令或内核模式，状态寄存器中的CU0位为零
- 一条COP1, COP1X,LWC1, SWC1, LDC1, SDC1或MOVCI(特殊操作码函数域编码)指令，状态寄存器中的CU1位为零。

- 执行COP2、LWC2、SWC2、LDC2、SDC2指令，状态寄存器中的CU2位为A零。COP2指令包括MFC2、DMFC2、CFC2、MFHC2、MTC2、DMTC2、CTC2、MTHC2。

注意:在MIPS32架构的第2版中，COP3作为用户定义的协处理器的使用已经被移除。COP3的使用是为该体系结构的未来扩展保留的。



##### Cause寄存器 ExcCode的值

CPU

##### Additional State Saved

CauseCE               被引用的协处理器的单元号

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### 6.2.23浮点异常

浮点异常由浮点协处理器发起，以发出浮点异常的信号。

##### 寄存器 ExcCode的值

FPE

##### Additional State Saved

FCSR          显示浮点异常的原因

##### Entry Vector Used

一般异常向量(偏移量0x180)



#### 6.2.24 协处理器 2异常

协处理器2异常由协处理器2发起，发出精确的协处理器2异常信号

##### 寄存器 ExcCode的值

C2E

##### Additional State Saved

由协处理器定义

##### Entry Vector Used

一般异常向量(偏移量0x180



#### **6.2.25 监视异常**

​		当指令或数据出现时，通过启动监视异常，监视设施提供了一种软件调试工具引用于存储在WatchHi和WatchLo寄存器中的地址信息相匹配。如果状态寄存器的EXL和ERL位都为零出现监视异常，则立即执行。如果任何一个位是一个时间，一个监视异常通常被获取，在Cause寄存器中的WP位被设置，并且异常被延迟到两者状态寄存器中的EXL和ERL位为零。软件可以使用原因寄存器中的WP位来确定如果EPC寄存器指向导致监视异常的指令，或者在内核模式异常确实发生了。

​		如果EXL或ERL位是状态寄存器中的一个位，并且一条指令同时生成一个监视异常(它被EXL和ERL位的状态延迟)和低优先级异常，低优先级异常被获取。

​		如果处理器在调试模式下执行，则不会出现监视异常。监视注册要匹配的时候当处理器处于调试模式时，异常被抑制，WP位不变。

​		数据监视异常是否由预取或缓存指令触发取决于实现地址匹配监视寄存器地址匹配条件。由SC指令触发的监视会这样做，即使存储不会完成，因为LL位是零。



##### 寄存器 ExcCode的值

WATCH

##### Additional State Saved

CauseWP             指示监视异常被推迟到之后StatusEXL和StatusERL都是零。这一点直接导致监视异常，所以软件必须把此位作为异常处理程序的一部分清除掉，以防止监视异常在当前处理程序执行结束时执行。

##### Entry Vector Used

一般异常向量(偏移量0x180



#### 6.2.26中断异常

当启动中断服务请求时，会发生中断异常。

##### 寄存器 ExcCode的值

Int

##### Additional State Saved

CauseIP      表示正在挂起的中断

##### Entry Vector Used

一般异常向量(偏移量0x180

##### Entry Vector Used

如果*Cause*寄存器中的*IV*位为零，则一般异常向量(偏移量0x180)。如果*Cause*寄存器中的*IV*位为1，则中断向量(偏移0x200).





