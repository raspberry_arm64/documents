# Ubuntu环境搭建

## 如果是刚刚安装完的Ubuntu20.04或者wsl，更新一下下载源

```shell
sudo su
cd /etc/apt/
mv sources.list source.list_bak
vim sources.list
添加以下内容到sources.list中

deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
```

```
apt-get update
apt-get upgrade
```



## 一、root权限安装的工具包

### 1.1 直接安装的程序和依赖包

```shell
apt-get install apt-utils
apt-get install vim
apt-get install software-properties-common
apt-get install openssh-server
apt-get install iputils-ping
apt-get install curl
apt-get install net-tools
apt-get install bsdmainutils
apt-get install kmod
apt-get install bc
apt-get install rsync
apt-get install gawk
apt-get install ssh
apt-get install ccache
apt-get install zip
apt-get install python-dev
apt-get install make
apt-get install m4
apt-get install gcc-multilib
apt-get install ca-certificates-java
apt-get install unzip
apt-get install python3-yaml
apt-get install perl
apt-get install openssl
apt-get install libssl1.1
apt-get install gnupg
apt-get install xsltproc
apt-get install x11proto-core-dev
apt-get install tcl
apt-get install python3-crypto
apt-get install python-crypto
apt-get install libxml2-utils
apt-get install libxml2-dev
apt-get install libx11-dev
apt-get install libssl-dev
apt-get install libgl1-mesa-dev
apt-get install lib32z1-dev
apt-get install lib32ncurses5-dev
apt-get install g++-multilib
apt-get install flex
apt-get install bison
apt-get install doxygen
apt-get install git
apt-get install subversion
apt-get install tofrodos
apt-get install pigz
apt-get install expect
apt-get install python3-xlrd 
apt-get install git-core
apt-get install gperf 
apt-get install build-essential
apt-get install zlib1g-dev
apt-get install libc6-dev-i386
apt-get install lib32z-dev
apt-get install openjdk-8-jdk
apt-get install ruby
apt-get install mtools
apt install python3-pip
apt-get install gcc-arm-linux-gnueabi
```

### 1.2 配置sh为bash

```shell
ls -l /bin/sh           		#如果显示为“/bin/sh -> bash”则为正常，否则请按以下方式修改： 
sudo dpkg-reconfigure dash   	#然后选择no
```

### 1.3 git lfs安装

```shell
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
apt-get install git-lfs
apt install git-lfs
git lfs install
```

### 1.4 repo工具安装

```shell
curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > repo
chmod a+x repo
mv repo /usr/local/bin/
```

### 1.3 git安装和配置

## 二、Harmony工具下载

```shell
mkdir /home/prebuilts
cd /home/prebuilts
mkdir aa-1.0 hc-gen-0.65-linux node-v12.18.4-linux-x64 clang-mingw
mkdir idl-1.0 proguard cmake-linux-x86-3.16.5 llvm python-linux-x86-3.8.5
mkdir d8-1.5.13 ndk-1.0 restool ninja-linux-x86-1.10.1 gn-linux-x86-1717

cd /home/prebuilts/node-v12.18.4-linux-x64
wget https://mirrors.huaweicloud.com/nodejs/v12.18.4/node-v12.18.4-linux-x64.tar.gz
tar -xvf node-v12.18.4-linux-x64.tar.gz

cd /home/prebuilts/aa-1.0
wget https://repo.huaweicloud.com/harmonyos/compiler/aafwk/1.0/aa-1.0.tar.gz
tar -xvf aa-1.0.tar.gz
 
cd /home/prebuilts/cmake-linux-x86-3.16.5
wget https://repo.huaweicloud.com/harmonyos/compiler/cmake/3.16.5/linux/cmake-linux-x86-3.16.5.tar.gz
tar -xvf cmake-linux-x86-3.16.5.tar.gz

cd /home/prebuilts/d8-1.5.13
wget https://repo.huaweicloud.com/harmonyos/compiler/d8/1.5.13/d8-1.5.13.tar.gz
tar -xvf d8-1.5.13.tar.gz

cd /home/prebuilts/gn-linux-x86-1717
wget https://repo.huaweicloud.com/harmonyos/compiler/gn/1717/linux/gn-linux-x86-1717.tar.gz
tar -xvf gn-linux-x86-1717.tar.gz

cd /home/prebuilts/idl-1.0
wget https://repo.huaweicloud.com/harmonyos/compiler/idl/1.0/idl-1.0.tar.gz
tar -xvf idl-1.0.tar.gz

cd /home/prebuilts/llvm
wget https://repo.huaweicloud.com/harmonyos/compiler/clang/10.0.1-53907-b/linux/llvm.tar.gz
tar -xvf llvm.tar.gz

cd /home/prebuilts/ndk-1.0
wget  https://repo.huaweicloud.com/harmonyos/compiler/ndk/1.0/ndk-1.0.tar.gz
tar -xvf ndk-1.0.tar.gz

cd /home/prebuilts/ninja-linux-x86-1.10.1
wget https://repo.huaweicloud.com/harmonyos/compiler/ninja/1.10.1/linux/ninja-linux-x86-1.10.1.tar.gz
tar -xvf ninja-linux-x86-1.10.1.tar.gz

cd /home/prebuilts/python-linux-x86-3.8.5
wget https://repo.huaweicloud.com/harmonyos/compiler/python/3.8.5/linux/python-linux-x86-3.8.5.tar.gz
tar -xvf python-linux-x86-3.8.5.tar.gz

cd /home/prebuilts/restool
wget https://repo.huaweicloud.com/harmonyos/compiler/restool/1.023-b/restool.tar.gz
tar -xvf restool.tar.gz

cd /home/prebuilts/hc-gen-0.65-linux
wget https://repo.huaweicloud.com/harmonyos/compiler/hc-gen/0.65/linux/hc-gen-0.65-linux.tar
tar -xvf hc-gen-0.65-linux.tar

cd /home/prebuilts/clang-mingw
wget https://repo.huaweicloud.com/harmonyos/compiler/mingw-w64/7.0.0/clang-mingw.tar.gz
tar -xvf clang-mingw.tar.gz

cd /home/prebuilts/proguard
wget https://repo.huaweicloud.com/harmonyos/compiler/proguard/7.0.1/proguard.tar.gz
tar -xvf proguard.tar.gz

vim /etc/profile
export PATH=$PATH:/home/prebuilts/llvm/llvm/bin
```



## 三、个人账号必须配置的工具

### 3.1 git安装和配置

```shell
git config --global user.name "yourname"
git config --global user.email "your-email-address"
git config --global core.editor vim
git config --global credential.helper store
```

### 3.3 安装hb命令

```shell
python3 -m pip install --user ohos-build
vim ~/.bashrc
export PATH=$PATH:~/.local/bin
source ~/.bashrc
```


## 四、选择性安装工具

### 4.1 docker环境安装部署

#### 安装docker

```shell
sudo apt-get install docker
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
```

#### 获取Dockerfile脚本文件，用来构建本地Docker镜像

```shell
cd ~/Harmony
git clone https://gitee.com/openharmony/docs.git
```

#### 进入Dockerfile代码目录路径执行Docker镜像构建命令

```shell
cd ~/Harmony/docs/docker
sudo ./build.sh
```

#### 进入docker构建环境

```shell
cd ~/OpenHarmony/v3.0
sudo docker run -it -v $(pwd):/home/openharmony swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker:0.0.5
```



## 五、root账号使用说明

### 5.1 root密码请找郭彬要

### 5.2 root使用方法

登录自己的账号后输入 su root，然后输入root密码即可登录

