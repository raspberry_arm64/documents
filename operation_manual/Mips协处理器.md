#### **9.33 Cause Register (CP0 Register 13, Select 0)**

**Compliance Level:** *Required*.

Cause寄存器主要描述最近一次异常的原因。此外，字段还控制软件中断请求和中断被分派的矢量。除了IP1..0,IV和WP字段，Cause寄存器中的所有字段都是只读的。架构的第2版增加了可选的支持对于EIC (外部中断控制器)中断模式，IP7..2被解释为请求的中断优先级(RIPL)。

 **Cause Register Format**

![image-20211123094540859](https://gitee.com/halley5/documents/raw/master/img/CP0/1.png)

Cause寄存器注册字段描述

| Name     | Bits          | Description                                                  | Read/Write | Reset State | Compliance                      |
| -------- | ------------- | ------------------------------------------------------------ | ---------- | ----------- | ------------------------------- |
| BD       | 31            | 中断是否发生在延时槽中                                       | R          | Undefifined | Required                        |
| TI       | 30            | 时钟中断标志                                                 | R          | Undefifined | Required(Release 2)             |
| CE       | 29..28        | 发生协处理器不可用异常时引用的协处理器单元号                 | R          | Undefifined | Required                        |
| DC       | 27            | 禁用计数寄存器,这个位必须写为0，读时返回0                    | R/W        | 0           | Required(Release 2)             |
| PCI      | 26            | 性能计数器中断,这个位表示是否一个性能计数器中断是挂起的      | R          | R           | Required(版本2和性能计数器实现) |
| ASE      | 25:24,17:16   | 这些位是留给MCU ASE的。如果MCU ASE没有实现，这些位返回零必须用零写入。 |            |             | 要求为MCU ASE，否则保留         |
| IV       | 23            | 是否使用特殊向量地址                                         | R/W        | Undefifined | Required                        |
| WP       | 22            | 指示监视异常被延迟,如果没有实现监视寄存器，则必须实现此位写入和读取为零时忽略 | R/W        | Undefifined | 如果需要观察寄存器实现          |
| FDCI     | 21            | 快速调试通道中断。这个位表示是否一个FDC中断正在等待          | R          | Undefifined | Required                        |
| IP7..IP2 | 15..10        | 当前中断等待标志，对应兼容和向量模式的8个中断号              | R          | Undefifined | Required                        |
| RIPL     | ..10          | 当前正在执行的中断号，仅外部中断控制模式                     | R          | Undefifined | Optional(仅外部中断控制模式)    |
| IP1..IP0 | 9..8          | 控制软件中断的请求，它也实现了EIC中断模式                    | R/W        | Undefifined | Required                        |
| ExcCdoe  | 6..2          | 异常代码                                                     | R          | Undefifined | Required                        |
| 0        | 20..16,7,1..0 | 必须写成零;读取时返回0。                                     | 0          | 0           | Required                        |

**Cause Register ExcCode Field**

| **Decimal** | **十六进制** | ****助记符**** | **描述**                              |
| ----------- | ------------ | -------------- | ------------------------------------- |
| 0           | 0x00         | Int            | 中断                                  |
| 1           | 0x01         | Mod            | TLB修改异常                           |
| 2           | 0x02         | TLBL           | TLB异常(加载或取指令)                 |
| 3           | 0x03         | TLBS           | TLB异常(存储)                         |
| 4           | 0x04         | AdEL           | 地址错误异常(加载或取指令)            |
| 5           | 0x05         | AdES           | 地址错误异常(存储)                    |
| 6           | 0x06         | IBE            | 总线错误异常(取指令)                  |
| 7           | 0x07         | DBE            | 总线错误异常(数据引用:加载或存储)     |
| 8           | 0x08         | Sys            | 系统调用异常                          |
| 9           | 0x09         | Bp             | 断点异常                              |
| 10          | 0x0a         | RI             | 保留指令异常                          |
| 11          | 0x0b         | CpU            | 协处理器无法使用异常                  |
| 12          | 0x0c         | Ov             | 算术溢出异常                          |
| 13          | 0x0d         | Tr             | 捕获异常                              |
| 14          | 0x0e         | \-             | 保留                                  |
| 15          | 0x0f         | FPE            | 浮点异常                              |
| 16-17       | 0x10-0x11    | \-             | 可用于依赖于实现的使用                |
| 18          | 0x12         | C2E            | 保留用于精确的协处理器2异常           |
| 19          | 0x13         | TLBRI          | TLB Read-Inhibit异常                  |
| 20          | 0x14         | TLBXI          | TLB Execution-Inhibit异常             |
| 21          | 0x15         | \-             | 保留                                  |
| 22          | 0x16         | MDMX           | MDMX不可使用的异常 (MDMX ASE)         |
| 23          | 0x17         | WATCH          | 引用WatchHi/WatchLo地址               |
| 24          | 0x18         | MCheck         | 机器检查                              |
| 25          | 0x19         | Thread         | 线程分配、回收或调度异常(MIPS®MT ASE) |
| 26          | 0x1a         | DSPDis         | DSP ASE State关闭异常(MIPS®DSP ASE)   |
| 27-29       | 0x1b - 0x1d  | \-             | 保留                                  |
| 30          | 0x1e         | CacheErr       | 缓存错误                              |
| 31          | 0x1f         | \-             | 保留                                  |



#### **9.34 NestedExc (CP0 Register 13, Select 5)**

**Compliance Level:** *Optional*.

嵌套异常(NestedExc)寄存器是一个只读寄存器，包含StatusEXL和的值StatusERL，在接受当前异常之前。该寄存器是嵌套故障特征的一部分，该寄存器的存在可以通过读取Config5NFExists。NestedExc寄存器的格式如图9-33所示;表9.45描述了NestedExc寄存器字段。

**Figure 9-33 NestedExc Register Format**

![image-20211123104138380](https://gitee.com/halley5/documents/raw/master/img/CP0/2.png)

**Table 9.45 NestedExc Register 字段含义**

| Name | Bits  | 描述                                                         | Read/Write | Reset State | **Compliance** |
| ---- | ----- | ------------------------------------------------------------ | ---------- | ----------- | -------------- |
| 0    | 31..3 | 保留，读为0                                                  | R0         | 0           | Required       |
| ERL  | 2     | 在接受当前异常之前，StatusERL prior的值由任何一个异常来更新设置 | R          | Undefifined | Required       |
| EXL  | 1     | 在接受当前异常之前，StatusERL prior的值将由指定的异常类型更新 | R          | Undefifined | Required       |
| 0    | 0     | 保留，读为0                                                  | R0         | 0           | Required       |



#### **9.35 异常程序计数器 (CP0 Register 14, Select 0)**

**Compliance Level:** *Required*.

​		异常程序计数器(EPC)是一个包含处理地址的读/写寄存器处理异常后恢复。EPC寄存器的所有位都是重要的，并且必须是可写的。除非状态寄存器中的EXL位已经是1，否则处理器在出现异常时写入EPC寄存器。

- 对于同步(精确)异常，EPC包含:

​				导致异常的指令的虚拟地址，或者发生异常时，前一个分支或跳转指令的虚拟地址指令位于分支延迟槽中，并且Cause寄存器中的分支延迟位已设置。

- 对于异步(不精确)异常，EPC包含恢复执行的指令地址。

  

​		处理器读取EPC寄存器作为执行ERET指令的结果。软件可以写EPC寄存器来改变处理器的恢复地址，并读取EPC寄存器来确定在什么地址处理器将恢复。EPC寄存器格式如图9-34所示;表9.46描述了EPC寄存器字段。

**Figure 9-34 EPC Register Format**

31                                                                                                                                                                                                                                                                           0

| EPC  |
| ---- |



**Table 9.46 EPC Register字段含义**

| Name | Bits  | 描述           | Read/Write | Reset State | Compliance |
| ---- | ----- | -------------- | ---------- | ----------- | ---------- |
| EPC  | 31..0 | 异常程序计数器 | R/W        | Undefifined | Required   |

##### 9.35.1 实现的处理器中EPC寄存器的特殊处理MIPS16e ASE或microMIPS32基础架构

在实现MIPS16e ASE或microMIPS32基本架构的处理器中，EPC寄存器需要特殊的处理。

当处理器写入EPC寄存器时，它将继续处理的地址与值组合在一起ISA模式寄存器:

`EPC ← resumePC31..1 || ISAMode0`

当处理器读取EPC寄存器时，它将这些位分配给PC和ISAMode寄存器:

`PC ← EPC31..1 || 0`

`ISAMode ← EPC0`

软件读取EPC寄存器简单地返回到GPR的最后一个值写没有解释。软件写入EPC寄存器存储一个新值，处理器如上所述解释该值。



#### 9.36嵌套异常程序计数器 (CP0 Register 14, Select 2)

**Compliance Level:** *Optional*

嵌套异常程序计数器(NestedEPC)是一个读写寄存器，其行为与EPC寄存器除了:

- NestedEPC寄存器忽略StatusEXL的值，因此一旦出现任何StatusEXL，就会进行更新异常，包括嵌套异常。
- NestedEPC寄存器不被ERET/DERET/IRET指令使用。软件需要复制如果希望返回到所存储的地址，则将NestedEPC寄存器的值赋给EPC寄存器NestedEPC。

该寄存器是嵌套故障特征的一部分，该寄存器的存在可以通过读取Config5NFExists。

NestedEPC寄存器格式如图9-35所示;表9.47描述了NestedEPC寄存器字段

**Figure 9-35 NestedEPC Register Format**

31                                                                                                                                                                                                                                                                          0

| NestedEPC |
| --------- |

**Table 9.47 NestedEPC Register字段含义**

| Name      | Bits  | 描述                                                         | Read/Write | Reset State | Compliance |
| --------- | ----- | ------------------------------------------------------------ | ---------- | ----------- | ---------- |
| NestedEPC | 31..0 | 嵌套异常程序计数器由异常更新，它将更新EPC。StatusEXL未设置(MCheck, Interrupt, Address)错误，所有TLB异常，总线错误，CopUnusable，预留指令、溢出、Trap、系统调用、FPU等)． 对于这些异常类型，将更新该寄存器字段不管StatusEXL的值是多少。不被更新的异常类型更新(复位，软复位，NMI，缓存错误)。没有被调试异常更新。 | R/W        | Undefifined | Required   |



#### **9.37 Processor Identifification (CP0 Register 15, Select 0)**

**Compliance Level:** *Required*

​		处理器标识(PRId)寄存器是一个32位只读寄存器，它包含标识处理器的信息制造商、制造商选项、处理器标识和处理器的修订级别。图9-36显示PRId寄存器的格式。

**Figure 9-36 PRId Register Format**

31                                                                      24      23                                                     16   15                                                     8      7                                                     0                                          

| Company Options | Company ID | Processor ID | Revision |
| --------------- | ---------- | ------------ | -------- |

**Table 9.48 PRId Register 字段含义**

| Name            | Bits   | 描述                                                         | Read/Write | Reset State | Compliance |
| --------------- | ------ | ------------------------------------------------------------ | ---------- | ----------- | ---------- |
| Company Options | 31..24 | 处理器的设计者或制造商可以使用与公司相关的选项。这里字段的值不是由体系结构指定的。如果此字段为没有实现，它必须读为零。 | R          | Preset      | Optional   |
| Company ID      | 23..16 | 标识设计或制造的公司处理器。软件可以区分MIPS32/microMIPS32或一个实现早期MIPS ISA的MIPS64/microMIPS64处理器通过检查该字段为零。如果非零，处理器实现MIPS32 / microMIPS32或MIPS64 / microMIPS64体系结构 | R          | Preset      | Required   |
| Processor ID    | 15..8  | 标识处理器的类型。该领域允许软件在单个公司内区分不同的处理器实现，并通过CompanyID字段，如上所述。结合的CompanyID和ProcessorID字段创建一个分配给每个处理器实现的唯一编号。 | R          | Preset      | Required   |
| Revision        | 7..0   | 指定处理器的修订号。这一领域允许软件区分一个版本和另一个相同类型的处理器。如果该字段不是实现时，它必须读为零。 | R          | Preset      | Optional   |

软件不应该使用这个寄存器的字段来推断处理器的配置信息。相反,应该使用配置寄存器来确定处理器的功能。



#### **9.38 EBase Register (CP0 Register 15, Select 1)**

**Compliance Level:** *Required* (Release 2).

​		EBase寄存器是一个读写寄存器，包含StatusBEV时使用的异常向量的基址等于0，和一个只读的CPU编号值，该值可能被软件用来区分不同的处理器多处理器系统。

​		EBase寄存器允许软件识别多处理器系统中的特定处理器，并允许每个处理器的异常向量是不同的，特别是在由异构neous处理器组成的系统中。位31 . .12个EBase寄存器用0连接，形成异常的基寄存器当StatusBEV为0时，向量。异常向量的基地址来自固定的默认值，当StatusBEV为1时，或任何EJTAG调试异常。位31..12的复位状态初始化异常基寄存器为0x8000.0000，以提供向后兼容性第1版实现。

​		如果写门位没有实现，EBase寄存器31..30位的值固定为0b10，而基址和异常偏移量的增加抑制了终端的第29位和第30位之间的进位异常处理。这两种限制的组合强制最终的异常地址位于kseg0或中Kseg1未映射虚拟地址段。对于缓存错误异常，在最终的异常中，第29位被强制为1基地址，以便该异常总是在kseg1未映射、未缓存的虚拟地址段中运行。

​		可以有选择地扩展EBase寄存器的操作，以允许Base异常字段的上位编写。这允许异常向量被放置在地址空间的任何地方。为了确保与MIPS32向后兼容，必须先设置写门位，然后才能更改上面的位。对于写门情况，完整的位集31..12是用来计算矢量位置的。软件可以检测写门的存在通过写入 1到该位位置，并检查是否设置了位。

​		基址和异常偏移的添加被执行，抑制在29位和30位之间的进位最后例外地址。

​		如果要改变异常基寄存器的值，必须在StatusBEV等于1的情况下进行。的操作当StatusBEV为0时，如果Exception Base字段用不同的值写入，则处理器为UNDEFINED。

**Figure 9-37 EBase Register Format**

![image-20211123114502433](https://gitee.com/halley5/documents/raw/master/img/CP0/3.png)

**Table 9.49 EBase Register字段含义**

| Name           | Bits   | 描述                                                         | Read/Write | Reset State    | Compliance |
| -------------- | ------ | ------------------------------------------------------------ | ---------- | -------------- | ---------- |
| 1              | 31     | 这个位在写时被忽略，在读时返回1。                            | R          | 1              | Required   |
| 0              | 30     | 这个位在写时被忽略，读时返回0。                              | R          | 0              | Required   |
| Exception Base | 29..12 | 31..30位组合，此字段指定当StatusBEV为时，异常向量的基址零。  | R/W        | 0              | Required   |
| 0              | 11..10 | 必须写成零;读取时返回0。                                     | 0          | 0              | Reserved   |
| CPUNum         | 9..0   | 在多处理器系统中该字段指定CPU的编号，可以通过软件来将特定的处理器与其他处理器区分开来。当处理器在系统中实现时，该字段的值由对处理器硬件的输入来设置环境。在单处理器系统中，此值应该设置为零。该字段也可以通过RDHWR寄存器0读取 | R          | 预设或外部设置 | Required   |

 **Figure 9-38 EBase Register Format**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        

![image-20211123140242345](https://gitee.com/halley5/documents/raw/master/img/CP0/4.png)

| Name           | Bits   | 描述                                                         | Read/Write | Reset State    | Compliance |
| -------------- | ------ | ------------------------------------------------------------ | ---------- | -------------- | ---------- |
| Exception Base | 31..12 | 该字段指定StatusBEV为零时异常向量的基址。位31 . .只有当WG被设置时，才可以写30。当WG是零，这些位在写入时没有变化。 | R/W        | 0x80000        | Required   |
| WG             | 11     | 写阀。当WG=0的值被写入时，位31..30在写EBase时保持不变。WG位必须在写值中设置为true来更改位31. .30的值. | R/W        | 0              | Required   |
| 0              | 10     | 必须写成零;读取时返回0。                                     | R0         | 0              | Reserved   |
| CPUNum         | 9..0   | 该字段指定CPU的编号，当处理器在系统中实现时，该字段的值由对处理器硬件的输入来设置环境。在单处理器系统中，此值应该设置为零。 | R          | 预设或外部设置 | Required   |



#### **9.39 CDMMBase Register (CP0 Register 15, Select 2)**

**Compliance Level:** *Optional*

​		公共设备内存映射功能的36位物理基址由这个寄存器定义。只有当Config3CDMM设置为1时，该寄存器才存在。

​		对于实现多个VPEs的设备，对该寄存器的访问由VPEConf0MVP寄存器字段控制。如果MVP位被清除，对该寄存器的读操作返回所有零，对该寄存器的写操作被忽略。

**Figure 9.39 CDMMBase Register**

![image-20211123141536517](https://gitee.com/halley5/documents/raw/master/img/CP0/5.png)

**Table 9.52 CDMMBase Register 字段含义**

| Name             | Bits  | 描述                                                         | Read/Write | Reset State | Compliance |
| ---------------- | ----- | ------------------------------------------------------------ | ---------- | ----------- | ---------- |
| CDMM_UP PER_ADDR | 31:11 | 内存的基本物理地址的35:15位的内存映射寄存器。实现的物理地址位数为具体的实现 | R/W        | Undefifined | Required   |
| EN               | 10    | 启用CDMM区域。如果这个位被清除，内存请求到这个地址区域转到常规系统内存中。如果设置了这个位，对该区域的内存请求转到CDMM逻辑 | R/W        | 0           | Required   |
| CI               | 9     | 如果设置为1，这表示CDMM的第一个64字节设备寄存器组被保留给管理CDMM区域行为的其他寄存器，这些寄存器不是IO设备寄存器。 | R          | Preset      | Optional   |
| CDMMSize         | 8：0  | 这个字段表示在内核中实例化的64字节设备注册块的数量。         | R          | Preset      | Required   |



#### **9.40 CMGCRBase Register (CP0 Register 15, Select 3)**

**Compliance Level:** *Optional*

​		内存映射的Coherency Manager全局配置寄存器空间的36位物理基地址是反映在这个寄存器上。只有当Config3CMGCR设置为1时，该寄存器才存在。

​		在实现MIPS MT ASE的设备上，这个寄存器在每个处理器上实例化一次。

**Figure 9.40 CMGCRBase Register**

 31                                                                                                                                              11      10                                                                                                            0

| CMGCR_BASE_ADDR | 0    |
| --------------- | ---- |

**Table 9.53 CMGCRBase Register 字段含义**

| Name             | Bits   | 描述                             | Read/Write | Reset State    | Compliance |
| ---------------- | ------ | -------------------------------- | ---------- | -------------- | ---------- |
| CMGCR_B ASE_ADDR | 31：11 | 这个寄存器字段反映了GCR_BASE的值 | R          | 预设(IP配置值) | Required   |
| 0                | 10：0  | 必须写成零;读取时返回零          | 0          | 0              | Reserved   |



#### **9.41 Confifiguration Register (CP0 Register 16, Select 0)**

**Compliance Level:** *Required*.

​		Config寄存器指定各种配置和功能信息。配置寄存器中的大多数字段是在Reset Exception过程中由硬件初始化的，或者是常量。三个字段，K23, KU, K0，必须在重置异常处理程序中由软件初始化。

**Figure 9-41 Confifig Register Format**

![image-20211123143523353](https://gitee.com/halley5/documents/raw/master/img/CP0/6.png)

**Table 9.54 Confifig Register 字段含义**

| Name | Bits  | 描述                                                         | Read/Write | Reset State                         | Compliance |
| ---- | ----- | ------------------------------------------------------------ | ---------- | ----------------------------------- | ---------- |
| M    | 31    | 表示Config1寄存器在选择字段值1。                             | R          | 1                                   | Required   |
| K23  | 30:28 | 对于实现固定映射MMU的处理器，该字段指定kseg2和kse3的可缓存性和相干属性。对于没有实现固定映射MMU的处理器，该字段读取为零和在写入时被忽略。 | R/W        | 未定义的处理器和固定映射MMU;否则为0 | Optional   |
| KU   | 27:25 | 对于实现固定映射MMU的处理器，此字段指定kuseg的可缓存性和一致性属性。对于没有实现固定映射MMU时，该字段读为零并被忽略写。 | R/W        | 未定义的处理器和固定映射MMU;否则为0 | Optional   |
| Impl | 24:16 | 这个字段是为实现保留的。指的是处理器规范的格式和定义这一字段 |            | Undefifined                         | Optional   |
| BE   | 15    | 处理器运行的端序模式:0:小端   1：大端                        | R          | 预置或外部设置                      | Required   |
| AT   | 14:13 | 处理器实现的架构类型。编码值0-2表示地址和寄存器宽度(32位或64位)。<br>0:MIPS32或 microMIPS32；1：仅对32位兼容性段访问MIPS64或microMIPS64；2：所有地址段访问mips64或microMIPS64；3：保留。 | R          | Preset                              | Required   |
| AR   | 12:10 | MIPS32架构修订级别                                           | R          | Preset                              | Required   |
| MT   | 9:7   | MMU类型                                                      | R          | Preset                              | Required   |
| 0    | 6:4   | 必须写成零;读取时返回0。                                     | 0          | 0                                   | Reserved   |
| VI   | 3     | 虚拟指令缓存(同时使用虚拟索引)和虚拟标签):0：指令缓存不是虚拟的；1：指令缓存是虚拟的 | R          | Preset                              | Required   |
| K0   | 2：0  | 可缓存性和一致性属性。                                       | R/W        | Undefifined                         | Required   |



#### **9.42 Confifiguration Register 1 (CP0 Register 16, Select 1)**

**Compliance Level:** *Required*

​		Config1寄存器是Config寄存器的一个附件，它编码额外的功能信息。中所有字段Config1寄存器是只读的

​		cache和Dcache的配置参数包括每路设置数的编码、行大小和结合性。因此，一个缓存的总缓存大小是:

​				`Cache Size = Associativity * Line Size * Sets Per Way`

​		如果行大小为零，则没有实现缓存。

**Figure 9-42 Confifig1 Register Format**

![image-20211123145520833](https://gitee.com/halley5/documents/raw/master/img/CP0/7.png)

**Table 9-42 Confifig1 Register Field Descriptions**

| Name         | Bits   | 描述                                                         | Read/Write | Reset State | Compliance |
| ------------ | ------ | ------------------------------------------------------------ | ---------- | ----------- | ---------- |
| M            | 31     | 这个位是保留的                                               | R          | Preset      | Required   |
| MMU Size - 1 | 30..25 | TLB的表项个数减1。值0隐含着ConfigMT拥有值为' none '。        | R          | Preset      | Required   |
| IS           | 24:22  | 编码对应的含义 0：64；1：128；2：256；3：512；4：1024；5：2048；6：4096；7：32 | R          | Preset      | Required   |
| IL           | 21:19  | 编码对应的含义 0：没有指令缓存；1：4字节；2：8字节；3：16字节；4：32字节；5：64字节；6：128字节；7：保留 | R          | Preset      | Required   |
| IA           | 18:16  | 0：直接映射；1： 2-way；2： 3-way；3： 4-way；4： 5-way；5： 6-way；6： <br>7-way；7: 8-way | R          | Preset      | Required   |
| DS           | 15:13  | 编码对应的含义 0：64；1：128；2：256；3：512；4：1024；5：2048；6：4096；7：32 | R          | Preset      | Required   |
| DL           | 12:10  | 编码对应的含义 0：没有指令缓存；1：4字节；2：8字节；3：16字节；4：32字节；5：64字节；6：128字节；7：保留 | R          | Preset      | Required   |
| DA           | 9:7    | 0：直接映射；1： 2-way；2： 3-way；3： 4-way；4： 5-way；5： 6-way；6： <br/>7-way；7: 8-way | R          | Preset      | Required   |
| C2           | 6      | 协处理器2实现:0:没有协处理器2实现;1:协处理器2实现            | R          | Preset      | Required   |
| MD           | 5      | 用于表示MDMX ASE在MIPS64 / microMIPS64处理器。不用于MIPS32 / microMIPS32处理器。 | R          | 0           | Required   |
| PC           | 4      | 执行性能计数器寄存器:0:无实现 ； 1：实现                     | R          | Preset      | Required   |
| WR           | 3      | 监视寄存器实现:0：无实现 ； 1：实现                          | R          | Preset      | Required   |
| CA           | 2      | 代码压缩(MIPS16e)实现:0：MIPS16e没有实现；1：实现            | R          | Preset      | Required   |
| EP           | 1      | EJTAG实现：0：无实现； 1：实现                               | R          | Preset      | Required   |
| FP           | 0      | FPU实现：0：无实现； 1：实现                                 | R          | Preset      | Required   |



#### **9.43 Confifiguration Register 2 (CP0 Register 16, Select 2)**

**Figure 9-43 Confifig2 Register Format**

![image-20211123152429997](https://gitee.com/halley5/documents/raw/master/img/CP0/8.png)

**Table 9.55 Confifig2 Register Field Descriptions**

| Name | Bits  | 描述                                                         | Read/Write | Reset State | Compliance |
| ---- | ----- | ------------------------------------------------------------ | ---------- | ----------- | ---------- |
| M    | 31    | 这个位是保留的，表示现在是Config3寄存器。                    | R          | Preset      | Required   |
| TU   | 30:28 | 特定于实现的三级缓存控制或状态位。如果这个字段没有被实现，它应该读为0，写时忽略。 | R/W        | Preset      | Optional   |
| TS   | 27:24 | 编码对应的含义 0：64；1：128；2：256；3：512；4：1024；5：2048；6：4096；7：8192；8-15：保留 | R          | Preset      | Required   |
| TL   | 23:20 | 编码对应的含义 0：现在没有指令缓存；1：4；2：8；3：16；4：32；5：64；6：128；7：256；8-15：保留 | R          | Preset      | Required   |
| TA   | 19:16 | 三级缓存结合性                                               | R          | Preset      | Required   |
| SU   | 15:12 | 实现特定的二级缓存控制或status位。如果这个字段没有被实现，它应该读为0，写时忽略。 | R/W        | Preset      | OptionaL   |
| SS   | 11:8  | 编码对应的含义 0：64；1：128；2：256；3：512；4：1024；5：2048；6：4096；7：8192；8-15：保留 | R          | Preset      | Required   |
| SL   | 7:4   | 编码对应的含义 0：现在没有指令缓存；1：4；2：8；3：16；4：32；5：64；6：128；7：256；8-15：保留 | R          | Preset      | Required   |
| SA   | 3:0   | 二级缓存结合性                                               | R          | Preset      | Required   |



#### **9.44 Confifiguration Register 3 (CP0 Register 16, Select 3)**

Config3寄存器编码额外的功能。Config3寄存器中的所有字段都是只读的。

**Figure 9-44 Confifig3 Register Format**

![image-20211123154619111](https://gitee.com/halley5/documents/raw/master/img/CP0/9.png)

**Table 9.56 Confifig3 Register 字段含义**

| Name     | Bits  | 描述                                                         | Read/Write                                               | Reset State | Compliance                           |
| -------- | ----- | ------------------------------------------------------------ | -------------------------------------------------------- | ----------- | ------------------------------------ |
| M        | 31    | 这个位是保留的，表示现在是Config4寄存器。                    | R                                                        | Preset      | Required                             |
| BPG      | 30    | 实现了大页面特性。这个位表示支持大于256mb的TLB页面C0_PageMask寄存器是64位宽。0：大页面没有被实现PageMask寄存器是32位宽。1：大页面实现PageMask寄存器是64位宽 | R                                                        | Preset      | Required                             |
| CMGCR    | 29    | 实现了Coherency Manager内存映射全局配置寄存器空间。          | R                                                        | Preset      | 连贯的多个核心实现使用一致性管理器。 |
| 0        | 28,23 | 必须写成零;读取时返回零                                      | 0                                                        | 0           | Reserved                             |
| BP       | 27    | BadInstrP寄存器实现。这一点表明故障先验分支指令字寄存器是否存在。0：无实现；1：实现 | R                                                        | Preset      | Required                             |
| BI       | 26    | BadInstr寄存器实现。0：无实现；1：实现                       | R                                                        | Preset      | Required                             |
| SC       | 25    | 段控制实现。这一点表明段控制是否注册SegCtl0, SegCtl1和SegCtl2。0：段控制未实现；1：段控制实现 | R                                                        | Preset      | Required                             |
| PW       | 24    | 硬件页表遍历实现。该位指示页表遍历是否注册PWBase，PWField和PWSize。0：未实现；1：实现 | R                                                        | Preset      | Required                             |
| IPLW     | 22:21 | 如果IPL字段的宽度为8位，则Status的第18位和第16位分别用作最高有效位和第二有效位该字段的最大有效位。 | R                                                        | Preset      | 如果MCU ASE是实现的：Required        |
| MMAR     | 20:18 | microMIPS32架构修订级别。MIPS32架构修订级别由AR字段配置      | R                                                        | Preset      | 如果microMIPS是实现的：Required      |
| MCU      | 17    | 实现了MIPS®MCU ASE：0：未实现 ； 1：实现                     | R                                                        | Preset      | 如果MCU ASE是实现的：Required        |
| ISAOnExc | 16    | 反映向异常进行矢量处理后使用的指令集架构。影响其关闭设置相对于EBase的所有异常。0：mips32用于进入异常向量。1：microMIPS用于进入一个异常向量。 | RW如果这两个指令集是实现;预设如果只有microMIPS是实现的。 | Undefifined | 如果microMIPS是实现的：Required      |
| ISA      | 15:14 | 指示指令集可用性：0：仅实现MIPS32指令集。1：只实现了microMIPS32 ；2：mips32和microMIPS32 ISAs实现。3：mips32和microMIPS32 ISAs实现。 | R                                                        | Preset      | 如果microMIPS是实现的：Required      |
| ULRI     | 13    | UserLocal寄存器实现：0：未实现 ； 1：实现                    | R                                                        | Preset      | Required                             |
| RXI      | 12    | 指示PageGrain寄存器是否存在RIE和XIE位。0：不存在 ； 1：存在  | R                                                        | Preset      | Required                             |
| DSP2P    | 11    | 实现MIPS®DSP ASE Revision 2，0：未实现 ；1:实现              | R                                                        | Preset      | Required                             |
| DSPP     | 10    | MIPS®DSP ASE实现：0：未实现 ；1:实现。                       | R                                                        | Preset      | Required                             |
| CTXTC    | 9     | ContextConfig寄存器的实现和宽度，0：未实现   1：实现         | R                                                        | Preset      | Required                             |
| ITL      | 8     | MIPS®IFlowTraceTM机制实现： 0：未实现   1：实现              | R                                                        | Preset      | Required(仅版本2.1)                  |
| LPA      | 7     | 表示支持在MIPS64处理器上大型物理视图地址，MIPS32不使用并在读取时返回零。 | R                                                        | Preset      | Required(仅版本2)                    |
| VEIC     | 6     | 支持一个外部中断控制器实现。 0：未实现   1：实现             | R                                                        | Preset      | Required(仅版本2)                    |
| VInt     | 5     | 矢量中断执行， 0：未实现   1：实现                           | R                                                        | Preset      | Required(仅版本2)                    |
| SP       | 4     | 实现了小(1KByte)页的支持， 0：未实现   1：实现               | R                                                        | Preset      | Required(仅版本2)                    |
| CDMM     | 3     | 通用设备内存映射实现，0：未实现   1：实现                    | R                                                        | Preset      | Required                             |
| MT       | 2     | MIPS®MT ASE实现，0：未实现   1：实现                         | R                                                        | Preset      | Required                             |
| SM       | 1     | martMIPS™ASE实现。0：未实现   1：实现                        | R                                                        | Preset      | Required                             |
| TL       | 0     | Trace Logic 实现，0：未实现   1：实现                        | R                                                        | Preset      | Required                             |



#### **9.45 Confifiguration Register 4 (CP0 Register 16, Select 4)**

Config4寄存器编码额外的功能。

FTLB中页面对条目的数量= decode(FTLBSets) * decode(FTLBWays)。

在VTLB中可访问的页面对条目的数量是通过连接Config4VTLBSizeExt和定义的Config1MMUSize。通过修改VTLB的大小，可以允许软件在VTLB中保留高索引槽位。

**Figure 9-45 Confifig4 Register Format**

![image-20211123164655844](https://gitee.com/halley5/documents/raw/master/img/CP0/10.png)

​	**Table 9.57 Confifig4 Register Field Descriptions**

| Name           | Bits  | 描述                                                         | Read/Write                                          | Reset State             | Compliance                                             |
| -------------- | ----- | ------------------------------------------------------------ | --------------------------------------------------- | ----------------------- | ------------------------------------------------------ |
| M              | 31    | 这个位是保留的，表示现在是Config5寄存器。                    | R                                                   | Preset                  | Required                                               |
| IE             | 30:29 | TLB使指令支持/配置无效。                                     | R                                                   | Preset                  | Required:TLBINV,TLBINVF,EntryHiEHINV这些特性必须是实现 |
| AE             | 28    | 如果设置了该位，则EntryHIASID扩展为10位                      | R                                                   | Preset                  | Required                                               |
| VTLBSizeExt    | 27:24 | 如果Config4MMUExt=3，则此字段连接到Config1MMUSize的最有效位的左边字段表示VTLB的大小。 | R                                                   | Preset                  | Required :如果 MMUExtDef=3                             |
| KScrExist      | 23:16 | 指示有多少临时寄存器可用                                     | R                                                   | Preset                  | Required ：如果内核抓取寄存器是可用                    |
| MMU Ext Def    | 15:14 | MMU扩展定义。定义如何解释Config4[13:0]。                     | R                                                   | Preset                  | Required                                               |
| FTLB Page Size | 10:8  | 显示FTLB阵列表项的页面大小。0：kb;1:4kb ;2:16kb; 3:64kb  ; 4:256 kb; 5: 1GB ; 6 : 4GB ; 7:保留 | R/W:如果实现多个FTLB页大小，R：只实现一个FTLB页大小 | 预设,选择值是实现特定的 | Required ：如果 MMUExtDef=2                            |
| FTLB Page Size | 12:8  | 显示FTLB阵列表项的页面大小。0：kb;1:4kb ;2:16kb; 3:64kb  ; 4:256 kb;5：1MB；6：4MB；7：16MB...19:256TB | R/W:如果实现多个FTLB页大小，R：只实现一个FTLB页大小 | 预设,选择值是实现特定的 | Required ：如果MMUExtDef=3                             |
| FTLB Ways      | 7:4   | 设置FTLB阵列的关联性,0:2  ;  1:3  ;  2:4...7-15:保留         | R                                                   | R                       | Required ：如果 MMUExtDef=2                            |
| FTLB Sets      | 3:0   | 表明FTLB数组设置每条路线的数量                               | R                                                   | R                       | Required ：如果 MMUExtDef=2                            |
| MMU Size Size  | 7:0   | 如果Config4MMUExt=1，则此字段是的扩展Config1MMUSize-1字段。0：1 ， 1：2，  2：4,....,15:32768 | R                                                   | R                       | Required ：如果 MMUExtDef=1                            |



#### **9.46 Confifiguration Register 5(CP0 Register 16, Select 5)**

Config5寄存器编码了额外的功能:

- 缓存错误异常矢量控制。
- 分割控件的遗留兼容性。
- EVA指令(LBK, LBUK, LHK, LHUK, LWK, SBK, SHK, SWK)的存在。
- 存在嵌套故障特征(NestedExc, NestedEPC)

**Figure 9-46 Confifig5 Register Format**

![image-20211123191309962](https://gitee.com/halley5/documents/raw/master/img/CP0/11.png)

**Table 9.58 Confifig5 Register 字段含义**

| Name     | Bits | 描述                                                         | Read/Write | Reset State | Compliance                        |
| -------- | ---- | ------------------------------------------------------------ | ---------- | ----------- | --------------------------------- |
| M        | 31   | 此位保留是为了表示尚未定义的反有图形寄存器。根据当前的体系结构定义，<br>这个位应该总是读取为0。 | R          | Preset      | Required                          |
| K        | 30   | 开启/关闭ConfigK0, ConfigKu, ConfigK23 Cache如果分割控制是相干属性控制实现的。 | R/W        | 0           | 细分控制所需要                    |
| CV       | 29   | 缓存错误异常矢量控制,0:在缓存错误异常中，矢量地址位31 . .29被迫放置在kseg1向量。1:在缓存错误异常中，矢量address使用完整的EBase值来表示位31 . . 29。 | R/W        | 0           | 细分控制所需要                    |
| EVA      | 28   | 增强的虚拟寻址指令实现                                       | R          | Preset      | Optional                          |
| 0        | 27:1 | 读取时返回零。                                               | R0         | 0           | Reserved                          |
| NF Exist | 0    | 表示存在嵌套故障特征。嵌套故障特征允许识别故障异常处理程序中的行为。 | R          | Preset      | Required:如果嵌套的故障特征存在。 |

​		关于Config5K的注释，段CCA的确定。下表9.59显示了哪个字段确定当Config5K=0或Config5K=1时段的CCA，在有/没有a的实现上TLB，当该区域被访问时未映射。



#### **9.47 Reserved for Implementations (CP0 Register 16, Selects 6 and 7)**

​		Compliance Level: 依赖于实现。

​		CP0寄存器16、 Selects 6和7保留用于依赖于实现的使用，并不是由体系结构定义的。为了使用CP0寄存器16， Selects 6和7，不需要实现CP0寄存器16， Selects 2到5只能在每个寄存器中设置M位。也就是说，如果Config2和Config3寄存器不需要实现时，不需要仅仅为了提供M位而实现它们。



#### **9.48 Load Linked Address (CP0 Register 17, Select 0)**

**Compliance Level:** *Optional*

​		LLAddr寄存器通过包含物理地址的相关比特位来读取到最近的跟负载有关的指令。该寄存器依赖于实现，仅用于诊断目的，在正常情况下不起作用操作。

**Figure 9-46 LLAddr Register Format**

31                                                                                                                                                                                                                                                                           0

| PAddr |
| :---: |

**Table 9.60 LLAddr Register Field Descriptions**

| Name  | Bits  | 描述                                                         | Read/Write | Reset State | Compliance |
| ----- | ----- | ------------------------------------------------------------ | ---------- | ----------- | ---------- |
| PAddr | 31..0 | 这个字段编码物理地址来读到最多的加载链接指令。该寄存器的格式依赖于实现，并且是一个可以实现许多位或格式的地址用任何它觉得方便的方式来实现。 | R          | Undefifined | Optional   |

​	

#### **9.49 WatchLo Register (CP0 Register 18)**

**Compliance Level:** *Optional*.

​		WatchLo和WatchHi寄存器一起提供了到监视点调试设施的接口，如果指令或数据访问与寄存器中指定的地址匹配该设施启动一个监视异常。因此，他们复制EJTAG调试解决方案的一些功能。EXL和ERL位为零时，才会发生监视异常状态寄存器。如果任一位为1，则在Cause寄存器中设置WP位，并且watch异常延迟到EXL和ERL位都为零。

​		一个实现可以提供零对或多对WatchLo和WatchHi寄存器，通过MTC0/MFC0指令中的select字段，并且每对Watch寄存器可以专用于特定类型的参考(如指令或数据)。软件可以确定是否至少有一对WatchLo和WatchHi寄存器是通过Config1寄存器的WR位实现的。参见WatchHi寄存器中关于M位的讨论下面的描述。

​		WatchLo寄存器指定基虚拟地址和引用的类型(指令获取、加载、存储)匹配。如果特定的Watch注册表只支持引用类型的子集，则未实现的启用必须是写时忽略，读时返回零。软件可以确定某个特定的Watch支持哪些启用功能通过设置所有三个使能位，并将它们读回以查看哪些是实际设置的寄存器对。

​		它的实现取决于数据监视是由预取、CACHE还是SYNCI(发布2和该指令的地址与Watch寄存器地址匹配条件相匹配。

**Figure 9-47 WatchLo Register Format**

![image-20211123194326357](https://gitee.com/halley5/documents/raw/master/img/CP0/12.png)

**Table 9.61 WatchLo Register Field Descriptions**

| Name  | Bits  | 描述                                                         | Read/Write | Reset State | Compliance |
| ----- | ----- | ------------------------------------------------------------ | ---------- | ----------- | ---------- |
| VAddr | 31..3 | 该字段指定要匹配的虚拟地址，这是一个双字地址。               | R/W        | Undefifined | Required   |
| I     | 2     | 如果此位为1，则为启用监视异常，如果这个位没有被实现，写入必须被实现忽略，且read必须返回零。 | R/W        | 0           | Optional   |
| R     | 1     | 如果此位为1，则在加载时启用监视异常和地址匹配的。如果这个位没有被实现，写入必须被实现忽略，且read必须返回零。 | R/W        | 0           | Optional   |
| W     | 0     | 如果此位为1，则为存储启用监视异常和地址匹配的。如果这个位没有被实现，写入必须被实现忽略，且read必须返回零。 | R/W        | 0           | Optional   |



#### **9.50 WatchHi Register (CP0 Register 19)**

**Compliance Level:** *Optional*.

​		WatchLo和WatchHi寄存器一起提供了到监视点调试设施的接口，如果指令或数据访问与寄存器中指定的地址匹配该设施启动一个监视异常。因此，他们复制EJTAG调试解决方案的一些功能。EXL和ERL位为零时，才会发生监视异常状态寄存器。如果任一位为1，则在Cause寄存器中设置WP位，并且watch异常延迟到EXL和ERL位都为零。

​		一个实现可以提供零对或多对WatchLo和WatchHi寄存器，通过MTC0/MFC0指令中的select字段，并且每对Watch寄存器可以专用于特定类型的参考(如指令或数据)。软件可以确定是否至少有一对WatchLo和WatchHi寄存器是通过Config1寄存器的WR位实现的。如果M位是WatchHi寄存器引用中的1select字段' n '，另一个WatchHi/WatchLo对使用select字段' n+1 '实现。

​		WatchHi寄存器包含限定WatchLo寄存器中指定的虚拟地址的信息ASID，一个G(全局)位，一个可选的地址掩码，以及三个位(I, R和W)，它们表示导致看寄存器匹配。如果G位为1，则引起任何与指定地址匹配的虚拟地址引用手表例外。如果G位为零，则只有那些虚拟地址引用的ASID值WatchHi寄存器匹配EntryHi寄存器中的ASID值会导致watch异常。可选掩码字段提供地址屏蔽以限定WatchLo中指定的地址。

​		当满足相应的手表寄存器条件时，处理器设置I、R和W位并指示哪个监视寄存器对(如果实现多个寄存器对)以及哪个条件匹配。当由进程设置时，每个位保持设置直到被软件清除。所有三位都是“写一清清”之类的软件必须在位上写一个1以清除其值。执行此操作的典型方法是将从WatchHi注册回WatchHi。这样做，只有那些在寄存器被读取时设置的位才会被清除当寄存器被写回时。

**Figure 9-48 WatchHi Register Format**

![image-20211123195141700](https://gitee.com/halley5/documents/raw/master/img/CP0/13.png)

**Table 9.62 WatchHi Register Field Descriptions**

| Name | Bits          | 描述                                                         | Read/Write                      | Reset State                            | Compliance        |
| ---- | ------------- | ------------------------------------------------------------ | ------------------------------- | -------------------------------------- | ----------------- |
| M    | 31            | 如果这个位是1，WatchHi/WatchLo寄存器对使用select字段' n+1 '实现 | R                               | Preset                                 | Required          |
| G    | 30            | 如果此位为1，表示与指定的地址相匹配的任何地址在WatchLo寄存器中会导致一个监视异常。如果这个位是零，是WatchHi寄存器的ASID字段必须匹配EntryHi寄存器的ASID字段导致监视异常。 | R/W                             | Undefifined                            | Required          |
| EAS  | 25:24         | 如果Config4AE = 1，那么这些位扩展ASID字段这个寄存器。<br>如果Config4AE = 0，则必须写为0;返回零在读。 | 如果Config4AE = 1为R / W其他的0 | 如果Config4AE= 1那么Undefifined其他的0 | Required          |
| ASID | 23..16        | 如果WatchHi寄存器中的G位为零，则ASID值需要匹配EntryHi寄存器  | R/W                             | Undefifined                            | Required          |
| Mask | 11..3         | 可选位掩码限定监视寄存器的地址，如果实现此字段，则表示任何位如果这个字段是1，则禁止相应的地址参与地址匹配的位。如果未实现此字段，则必须实现对其的写入忽略，且read必须返回零。软件可以通过将掩码写入这个字段然后读取来确定有多少掩码位被实现回的结果。 | R/W                             | Undefifined                            | Optional          |
| I    | 2             | 这个位是由硬件在存储条件下设置的匹配监视寄存器对中的值。当设置时,该位保持设置直到被软件清除，也就是通过在比特上写1来完成。 | W1C                             | Undefifined                            | Required（版本2） |
| R    | 1             | 这个位是由硬件在存储条件下设置的匹配监视寄存器对中的值。当设置时,该位保持设置直到被软件清除，也就是通过在比特上写1来完成。 | W1C                             | Undefifined                            | Required（版本2） |
| W    | 0             | 这个位是由硬件在存储条件下设置的匹配监视寄存器对中的值。当设置时,该位保持设置直到被软件清除，也就是通过在比特上写1来完成。 | W1C                             | Undefifined                            | Required（版本2） |
| 0    | 29..26,15..12 | 必须写成零;读取时返回0。                                     | 0                               | 0                                      | Reserved          |



#### **9.51 Reserved for Implementations (CP0 Register 22, all Select values)**

**Compliance Level:** *依赖实现*.

CP0寄存器22是为依赖于实现的使用而保留的，并不是由体系结构定义的。



#### **9.52 Debug Register (CP0 Register 23, Select 0 )**

**Compliance Level:** *Optional*.

调试寄存器是EJTAG规范的一部分。的格式和描述请参阅该规范这个寄存器。



#### **9.53 Debug2 Register (CP0 Register 23, Select 6)**

**Compliance Level:** *Optional*

Debug2寄存器是EJTAG规范的一部分。格式和描述请参阅该规范这个寄存器。



#### **9.54 DEPC Register (CP0 Register 24)**

**Compliance Level:** *Optional*.

​		DEPC寄存器是一个读写寄存器，它包含一个地址，在一个调试异常被服务后，处理在这个地址继续进行。它是EJTAG规范的一部分，这里的读者需要了解注册表的格式和描述。DEPC寄存器的所有位都是重要的，必须是可写的。

当一个调试异常发生时，处理器写入DEPC寄存器，

- 直接导致异常的指令的虚拟地址，或
- 当引起异常的指令位于分支延迟槽中，并且在Cause寄存器中设置了分支延迟位时，前一个分支或跳转指令的虚拟地址。

处理器读取DEPC寄存器作为DERET指令执行的结果。

软件可以写DEPC寄存器来改变处理器的恢复地址，并读取DEPC寄存器来确定处理器将在什么地址恢复。

##### **9.54.1**通过特殊处理处理器中DEPC寄存器来实现MIPS16e ASE或microMIPS32 Base架构

在实现MIPS16e ASE或microMIPS32基础架构的处理器中，DEPC寄存器需要特殊处理。

当处理器写入DEPC寄存器时，它将继续处理的地址与的值组合在一起ISA模式寄存器:

`DEPC ← resumePC31..1 || ISAMode0`

当处理器读取DEPC寄存器时，它将这些位分配到PC和ISA模式寄存器:

`PC ← DEPC31..1 || 0`

`ISAMode ← DEPC0`

软件读取的DEPC寄存器简单地返回到一个GPR的最后值写没有解释。软件写入到DEPC寄存器存储一个新值，处理器如上所述解释该值。



#### **9.55 Performance Counter Register (CP0 Register 25)**

**Compliance Level:** *Recommended*

​		体系结构支持实现相关的性能计数器，这些计数器提供计数功能用于性能分析的事件或周期。如果实现了性能计数器，则每个性能计数器由一对寄存器组成:一个32位控制寄存器和一个32位计数器寄存器。为了提供额外的功能，可能实现多个性能计数器。

​		可以将性能计数器配置为计数与实现相关的事件或特定集合下的周期由性能计数器的控制寄存器确定的条件。计数器寄存器递增对每个启用的事件执行一次。当计数器寄存器的最重要位为1时(计数器溢出)，则性能计数器有选择地请求中断。在该体系结构的第1版的实现中，这种内部中断以一种依赖于实现的方式与硬件中断5结合在一起。在架构的第2版中，来自所有性能计数器的挂起的中断都是或在一起的，并成为Cause寄存器中的PCI位按照处理器的中断模式进行优先级排序。计数器寄存器结束后继续计数流，无论是否请求或接受中断。

​		每个性能计数器被映射到PerfCnt寄存器的偶奇数选择值:偶选择访问控制寄存器，偶选择访问计数器寄存器。表9.63显示了两个性能计数器的示例以及它们如何映射到PerfCnt寄存器的选择值。

**Figure 9-49 Performance Counter Control Register Format**

![image-20211123203317723](https://gitee.com/halley5/documents/raw/master/img/CP0/14.png)

**Table 9.64 Performance Counter Control Register Field Descriptions**

| Name     | Bits   | 描述                                                         | Read/Write | Reset State                         | Compliance                               |
| -------- | ------ | ------------------------------------------------------------ | ---------- | ----------------------------------- | ---------------------------------------- |
| M        | 31     | 如果这个位是1，另一对性能计数器控制和计数器寄存器是在mt0上<br>实现的或MFC0选择' n+2 '和' n+3 '字段值。 | R          | Preset                              | Required                                 |
| 0        | 30     | 预留给MIPS64/microMIPS64处理器。未使用的在MIPS32/microMIPS32处理器上。 | R          | Preset                              | Required                                 |
| Impl     | 29：25 | 此字段是与实现相关的,如果不被实现使用，必须写成零;读取时返回0。 |            | Undefifined<br>0:如果不是使用的实现 | Optional                                 |
| 0        | 24..16 | 必须写成零;读取时返回零                                      | 0          | 0                                   | Reserved                                 |
| PCTD     | 15     | 禁用性能指标跟踪。这位用于禁用指定的性能计数器当性能计数器跟踪是启用，并触发一个性能计数器跟踪事件。0:对此计数器启用跟踪。1:对此计数器禁用跟踪。 | RW         | 0                                   | Required:PDTrace性能计数器跟踪功能实现。 |
| EventExt | 14..11 | 在一些实现中，在6位Event字段中可能支持超过64个编码，EventExt字段作为事件字段的扩展，任何未实现的位被读取为零，而被读取为零忽略了在写。 | RW         | Undefifined                         | Optional                                 |
| Event    | 10..5  | 选择Counter寄存器要统计的事件，事件列表是实现依赖，但典型的事件包括周期、指令、内存引用指令、分支指令、缓存和TLB错过等。 | R/W        | Undefifined                         | Required                                 |
| IE       | 4      | 中断使能。调用时启用中断请求对应的计数器溢出，这个位只是启用中断请求。实际的中断仍然由正常中断控制在状态寄存器中掩码和启用。0：禁用性能计数器中断，1：启用性能计数器中断 | R/W        | 0                                   | Required                                 |
| U        | 3      | 在用户模式中启用事件计数。0：在用户模式下禁用事件计数；1：在“用户模式”中启用事件计数 | R/W        | Undefifined                         | Required                                 |
| S        | 2      | 在管理器模式中启用事件计数，如果处理器没有实现管理模式，这个位在写时必须被忽略，读时返回0。0：在管理器模式中禁用事件计数，1：在管理器模式中启用事件计数 | R/W        | Undefifined                         | Required                                 |
| K        | 1      | 在内核模式下启用事件计数。0：在内核中禁用事件计数模式，1：在内核中启用事件计数模式 | R/W        | Undefifined                         | Required                                 |
| EXL      | 0      | 启用事件计数时，EXL位在状态register为1,Status寄存器中的ERL位为零。0：当EXL =时禁用事件计数1, erl = 0； 1：EXL = 1时启用事件计数，ERL = 0 | R/W        | Undefifined                         | Required                                 |



对于每个启用的事件，与每个性能计数器关联的计数器寄存器增加一次。图9-50显示性能指标计数器登记册的格式;表9.65描述了性能指标柜台注册字段。

**Figure 9-50 Performance Counter Counter Register Format**

31                                                                                                                                                                                                                                                                          0

| Event Count |
| :---------: |

**Table 9.65 Performance Counter Counter Register Field Descriptions**

| Name        | Bits  | 描述                                                         | Read/Write | Reset State | Compliance |
| ----------- | ----- | ------------------------------------------------------------ | ---------- | ----------- | ---------- |
| Event Count | 31..0 | 对象所启用的每个事件递增一次相应的控制寄存器。当最重要的位是1时，<br>中止的中断请求是或的与来自其他性能计数器的数据，并由Cause寄存器中的PCI位指示。 | R/W        | Undefifined | Required   |

程序功能说明:

在体系结构的第2版中，EHB指令可以用来使在IE中控件寄存器中断状态变化的字段可见或计数器寄存器的事件计数字段被写入。



#### **9.56 ErrCtl Register (CP0 Register 26, Select 0)**

**Compliance Level:** *Optional*

​		ErrCtl寄存器提供了一个具有错误检测机制，由处理器实现依赖于实现的诊断接口。这个寄存器在以前的实现中被用来读写奇偶校验或与特定的编码一起，进出主缓存数据阵列或从缓存数据阵列的ECC信息Cache指令或其他依赖于实现的方法。ErrCtl寄存器的确切格式取决于实现，而不是由体系结构指定。请参阅处理器规范的格式寄存器和字段的描述。



#### **9.57 CacheErr Register (CP0 Register 27, Select 0)**

**Compliance Level:** *Optional.*

​		CacheErr寄存器提供了一个接口，用于缓存错误检测逻辑，可以由处理器实现。CacheErr寄存器的确切格式取决于实现，而不是由体系结构指定。请参考向处理器规范中提供该寄存器的格式和字段的描述。



#### **9.58 TagLo Register (CP0 Register 28, Select 0, 2)**

**Compliance Level:** *Required*： *如果缓存被实现;*   否则是*Optional* 

​		TagLo和TagHi寄存器是作为缓存标记阵列接口的读写寄存器。索引存储标签和索引加载标签操作的CACHE指令使用TagLo和TagHi寄存器分别作为源或标记信息的水槽。

​		TagLo和TagHi寄存器的确切格式取决于实现。请参阅处理器规范以获取该寄存器的格式和字段的描述。

​		然而，软件必须能够将0写入TagLo和TagHi寄存器，然后使用Index Store标记启动时初始化缓存标记到有效状态的缓存操作

​		是否有一个TagLo寄存器作为所有缓存的接口，或者是一个每个缓存的专用TagLo寄存器。如果实现了多个TagLo寄存器，它们将占用偶数选择该寄存器编码的值，select 0寻址指令缓存，select 2寻址数据缓存。无论是否为每个缓存实现了单独的TagLo寄存器，处理器都必须接受写入0TagLo的select 0和select 2作为启动时初始化缓存标记的软件过程的一部分。



#### **9.59 DataLo Register (CP0 Register 28, Select 1, 3)**

**Compliance Level:** *Optional*

​		DataLo和DataHi寄存器是作为缓存数据阵列接口的寄存器，用于诊断操作。CACHE指令的索引加载标记操作将相应的数据读取到DataLo和DataHi寄存器中。

​		DataLo和DataHi寄存器的确切格式和操作取决于具体的实现。有关该寄存器的格式和字段的描述，请参阅处理器规范。

​		是否有一个单独的DataLo寄存器充当所有缓存的接口，或者是一个专用的DataLo寄存器用于每个缓存。如果实现了多个DataLo寄存器，它们将占用奇数选择值，选择1寻址指令缓存，选择3寻址数据缓存。



#### **9.60 TagHi Register (CP0 Register 29, Select 0, 2)**

**Compliance Level:** *Optional*.

​		DataLo和DataHi寄存器是作为缓存数据阵列接口的寄存器，用于诊断操作。CACHE指令的索引加载标记操作将相应的数据值读入DataLo和DataHi寄存器。

​		DataLo和DataHi寄存器的确切格式和操作取决于具体的实现。有关该寄存器的格式和字段的描述，请参阅处理器规范。



#### **9.62 ErrorEPC (CP0 Register 30, Select 0)**

Compliance Level: Required.

​		errrepc寄存器是一个读写寄存器，类似于EPC寄存器，在重置后处理在此寄存器继续，软复位，不可屏蔽中断(NMI)或缓存错误异常(统称为错误异常)。与EPC寄存器不同，errorrep寄存器没有相应的分支延迟槽指示。所有的errorrepc寄存器是重要的，并且必须是可写的。

​		当错误异常发生时，处理器将errorrep寄存器写入:

- ​			直接导致异常的指令的虚拟地址，或
- ​			当导致错误的指令为时，前一个分支或跳转指令的虚拟地址在分支延迟槽。

​		处理器读取errrepc寄存器作为执行ERET指令的结果。

​		软件可以写errerepc寄存器来改变处理器的恢复地址，并将errerepc寄存器读到确定处理器将恢复的地址

**Figure 9-51 ErrorEPC Register Format**

31                                                                                                                                                                                                                                                                          0

| ErrorEPC |
| :------: |

**Table 9.66 ErrorEPC Register Field Descriptions**

| Name     | Bits  | 描述               | Read/Write | Reset State | Compliance |
| -------- | ----- | ------------------ | ---------- | ----------- | ---------- |
| ErrorEPC | 31..0 | 错误异常程序计数器 | R/W        | Undefifined | Required   |

##### **9.62.1**对处理器中errrepc寄存器进行特殊处理来实现MIPS16e ASE或microMIPS32 Base架构

​		在实现MIPS16e ASE或microMIPS32基础架构的处理器中，errorrep寄存器需要特殊处理

​		当处理器写入errorrepc寄存器时，它将继续处理的地址与ISA Mode寄存器的值:

​			`ErrorEPC ← resumePC31..1 || ISAMode0`

​		当处理器读取errrepc寄存器时，它将这些位分配给PC和ISAMode寄存器:

​			`PC ← ErrorEPC31..1 || 0`

​			`ISAMode ← ErrorEPC0`

​		软件读取的错误repc寄存器简单地返回到GPR的最后一个值写没有解释。软软件写入到errrepc寄存器存储一个新值，处理器如上所述解释该值。



#### **9.63 DESAVE Register (CP0 Register 31)**

**Compliance Level:** *Optional*.

​		DESAVE寄存器是EJTAG规范的一部分。的格式和描述请参阅该规范这个寄存器。

​		DESAVE寄存器只能在调试模式下使用。如果内核模式软件使用这个寄存器，它会与调试内核模式软件冲突。因此，强烈建议使用内核模式软件不使用这个寄存器。如果实现了KScratch*寄存器，内核软件就可以使用这些寄存器。



#### **9.64 KScratch**n **Registers (CP0 Register 31, Selects 2 to 7)**

**Compliance Level:** *Optional, KScratch1 and KScratch2 at selects 2, 3 are recommended.*

​		KScratchn寄存器是内核模式软件用于scratch pad存储的读写寄存器。这些对于32位处理器，寄存器的宽度是32位，对于64位处理器，寄存器的宽度是64位。

​		这些寄存器的存在是由Config4寄存器中的KScrExist字段指示的。KScrExist字段指定哪些选择用内核临时寄存器填充。

​		调试模式软件不应该使用这些寄存器，相反，调试软件应该使用DESAVE寄存器。如果实现了EJTAG，不应该将选择0用于KScratch寄存器。选择1是为将来的调试保留的使用，不应用于KScratch寄存器。

**Figure 9-52 KScratch**n **Register Format**

31                                                                                                                                                                                                                                                                           0

| Data |
| :--: |

**Table 9.67 KScratch**n   **Register Field Descriptions**

| Name | Bits  | 描述                       | Read/Write | Reset State | Compliance |
| ---- | ----- | -------------------------- | ---------- | ----------- | ---------- |
| Data | 31..0 | 由内核软件保存的临时数据。 | R/W        | Undefifined | Optional   |

