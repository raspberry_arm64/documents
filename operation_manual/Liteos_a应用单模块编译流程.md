# Liteos_a应用单模块编译流程

步骤1：[v3.0源码下载](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/Readme.md)

步骤2：[编译可参考该链接 2.1.2 代码编译](https://gitee.com/wenfei6316/harmony_os/blob/master/SummaryDocs/Harmony%E4%BB%A3%E7%A0%81%E4%B8%8B%E8%BD%BD%E7%BC%96%E8%AF%91%E5%92%8C%E7%83%A7%E5%BD%95.md)

步骤3：[新建应用程序，参考官方实例](https://device.harmonyos.com/cn/docs/documentation/guide/quickstart-lite-steps-hi3516-running-0000001151888681)

#### 注意重点如下

应用编写完毕后，执行编译时做如下替换

全版本编译：

```
hb build -f
```

单模块编译：

```
hb build -T 应用程序targets属性值
```

实例：

```
hb build -T //applications/sample/camera/apps:hello-OHOS
```

应用程序targets属性值查找，进入上述步骤3，查看新建应用程序步骤3添加新组件，即可看到。

##### 说明：冒号后面的值hello-OHOS需要和新建应用程序步骤2新建编译组织文件里面的lite_component("hello-OHOS")名称对应。

回车执行命令，2-3s即可生成可执行的二进制文件,到此，单模块编译就完成了，

编译生成的文件目录如下：以hispark_taurus平台为例

```
out/hispark_taurus/ipcamera_hispark_taurus/bin/
```

后续可参考[tftp服务的使用](https://gitee.com/halley5/documents/blob/master/operation_manual/%E4%BD%BF%E7%94%A8tftp%E5%9C%A8windows%E4%B8%8E%E5%BC%80%E5%8F%91%E6%9D%BF%E4%B9%8B%E9%97%B4%E4%BC%A0%E8%BE%93%E6%96%87%E4%BB%B6.md)，进行文件传输至单板，即可调试。