# svn使用指导

## 一、创建svn用户

```shell
sudo useradd -d /home/svn -r -m -s /bin/bash svn
sudo passwd svn
```

## 二、创建svn项目

```shell
su svn
cd ~
mkdir project1
svnadmin create project1
```

## 三、授权用户及权限统一管理（非svn配置者严禁操作）

```shell
su svn
cd ~
mv ./project1/conf/authz .authz
mv ./project1/conf/passwd .passwd
```

## 四、设置项目权限

```shell
su svn
cd ~
vim ./project1/conf/svnserve.conf
27行 # password-db = passwd ---> password-db = ../../.passwd
36行 # authz-db = authz ---> authz-db = ../../.authz

vim /home/svn/.passwd
#最下面添加账号和密码：
root = 123456
user = 123456
	
vim /home/svn/.authz
[groups] 下添加群组（非必须）
admin = root,user
最后添加如下内容：
[project1:/]:
@admin = rw
root = rw
* = r
```

## 五、开启服务器

我们的svn服务器是保存在10.20.14.56电脑上，当系统重启后需要重新启动svn服务器，

```shell
sudo svn
cd ~
svnserve -d -r .
```

## 六、查看服务器是否打开：

​	

```shell
netstat -antp | grep svnserve
显示如下：
tcp	0 0 0.0.0.0:3690	0.0.0.0:*	LISTEN	7688/svnserve
```

## 七、关闭 svnserve 服务

```shell
pkill svnserve
```

## 八、svn下载

```shell
svn co svn://10.20.14.56/project1
```

## 九、svn一键添加未版本化的文件

```shell
svn st | grep '^\?' | tr '^\?' ' ' | sed 's/[ ]*//' | sed 's/[ ]/\\ /g' | xargs svn add
```

## 十、svn提交

```shell
svn ci -m"message"
```

## 十一、svn更新

```shell
svn update
```

## 十二、当前svn仓

```
#存放君正开发板相关的资料
svn://10.20.14.56/ingenic
#存放项目相关的一些文档
svn://10.20.14.56/HarmonyDoc
#存放鸿蒙入门适配和平时培训的录屏
svn://10.20.14.56/HarmonyVideo
#存放新员工装机必备的一些常用软件和开发软件
svn://10.20.14.56/tools
```

