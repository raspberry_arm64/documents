# 使用tftp在windows与开发板之间传输文件

TFTP（Trivial File Transfer Protocol,简单文件传输协议）是TCP/IP协议族中的一个用来在客户机与服务器之间进行简单文件传输的协议，提供不复杂、开销不大的文件传输服务。端口号为69。

## 一、准备工作

1.请下载并安装[tftpd64](https://bitbucket.org/phjounin/tftpd64/downloads/)。

2.开发板已烧录完成并与windows连接，可在串口输入输出信息。

3.查看并修改windows或开发板ip地址使得二者在同一网段，例如开发板地址为192.168.1.10则可修改windows地址为192.168.1.33

开发板查看ip地址：

```
ifconfig
```

## 二、开始传输

1.打开tftpd64，点击Browse选择待传输文件所在目录（未非全英文目录可能有影响，建议为全英文目录），选择完毕后点击Show Dir可看到该目录下的文件

Server Interfaces一栏选择本机ip地址，不要选择127.0.0.1

2.串口输入命令：

```
./bin/tftp -g -l xxx1 -r xxx2 ipaddress
```

其中-g意为get，从windows下载至开发板（若需要开发板上传至windows则-g改为-p）

-l 为local，指本地文件，-r 为remote，指另一端文件

在下载时 xxx1 为文件存储位置和文件重命名，若为/bin/abc即存放在bin目录下并将文件重命名为abc

xxx2 为windows上文件名称，请确认tftpd64点击Show Dir可看到该文件。

在上传时 xxx1 为开发板本地文件路径和名称， xxx2 为windows接收后的重命名。

ipaddress 为windows端ip地址，需要与之前Server Interfaces一栏选择的一致。

注意：tftp无法一条命令传输多个文件，但可以将多个文件放入同一文件夹，传输该文件夹。

## 三、常见问题

1.各步骤无任何问题但传输失败，ls命令可以查看到传输文件，但文件大小明显较小。

可能是指定的目录空间不足，如我之前所用的bin目录下只有数百k的容量，改为userdata目录即可正常传输。

2.超时报错，请检查传输文件目录是否为全英文，检查二者是否在同一网段并重启tftp工具。

3.待更新
