# 1、代码中常用的python.py脚本命令







## 简介

```
我们实际上可以导入其他脚本中的 Python
	如果你处理的是大型项目，需要将代码整理成多个文件并重复利用这些文件中的代码，则导入脚本很有用。
	
	如果你要导入的 Python 脚本与当前脚本位于同一个目录下，只需输入 import，然后是文件名，无需扩展名 .py。
		import   useful_functions
		Import 语句写在 Python 脚本的顶部，每个导入语句各占一行。
		该 import 语句会创建一个模块对象，叫做 useful_functions。
		
	模块是包含定义和语句的 Python 文件。要访问导入模块中的对象，需要使用点记法。
		import   useful_functions                                  
		useful_functions.add_five([1,2,3,])   
		与                        
		import   useful_functions as uf
		us.add_five([1,2,3,])
	from module_name import object_name            
		要从模块中导入单个函数或类
	from module_name import first_object, second_object        
		要从模块中导入多个单个对象：
	import module_name as new_name                    
		要重命名模块：
	from module_name import object_name as new_name         
		要从模块中导入对象并重命名
	from module_name import *             
		要从模块中单个地导入所有对象（请勿这么做）：
	如果你真的想使用模块中的所有对象，请使用标准导入 module_name 语句并使用点记法访问每个对象。
```



```
在Python中，特殊名称用于两个重要的构造：`__main__`

1. 程序的顶级环境的名称，可以使用表达式进行检查;和`__name__ == '__main__'`
2. Python 包中的文件。`__main__.py`

	 `__main__`是运行顶级代码的环境的名称。"顶级代码"是第一个用户指定的开始运行的Python模块。
	 它是"顶级"的，因为它导入程序需要的所有其他模块。有时，"顶级代码"称为应用程序的*入口点*。 

	某些模块包含仅用于脚本使用的代码，例如分析命令行参数或从标准输入中提取数据。
	如果从其他模块导入这样的模块，例如对其进行单元测试，则脚本代码也会无意中执行。

	这就是使用代码块派上用场的地方。除非在顶级环境中执行模块，否则此块中的代码将不会运行。
	`if __name__ == '__main__'`

	使用 if main 块   避免了运行从其他脚本中作为模块导入的脚本中的可执行语句
	将这些行包含在 if __name__ == "__main__" 块中。或者，将它们包含在函数 main() 中
	并在 if main 块中调用该函数。
	每当我们运行此类脚本时，Python 实际上会为所有模块设置一个特殊的内置变量 __name__。
	当我们运行脚本时，Python 会将此模块识别为主程序，并将此模块的 __name__ 变量设为字符串 "__main__"。
	对于该脚本中导入的任何模块，这个内置 __name__ 变量会设为该模块的名称。
	因此，条件 if __name__ == "__main__"会检查该模块是否为主程序。 


```



```
if语句      	 
	当条件成立时运行语句块。经常与else, elif(相当于else if) 配合使用。
for语句     	 	
	遍历列表、字符串、字典、集合等迭代器，依次处理迭代器中的每个元素。
while语句   	 
	当条件为真时，循环运行语句块。
try语句     	 
	与except,finally配合使用处理在程序运行中出现的异常情况。
class语句   	 
	用于定义类型。
def语句      		
	用于定义函数和类型的方法。
pass语句    	 
	表示此行为空，不运行任何操作。
assert语句   	
	用于程序调适阶段时测试运行条件是否满足。
with语句    	 
	Python2.6以后定义的语法，在一个场景中运行语句块。比如，运行语句块前加密，然后在语句块运行退出后解密。
yield语句    	
	在迭代器函数内使用，用于返回一个元素。自从Python 2.5版本以后。这个语句变成一个运算符。
raise语句    	
	制造一个错误。
import语句   		
	导入一个模块或包。
from import语句  
	从包导入模块或从模块导入某个对象。
import as语句    
	将导入的对象赋值给一个变量。
in语句         
	判断一个对象是否在一个字符串/列表/元组里。
```



## 1、sys模块

### 1、简介

```
sys模块主要是针对与Python解释器相关的变量和方法，不是主机操作系统。
导入方式：import sys
```



### 2、命令

```
sys.argv    
	是一个脚本执行参数列表，列表的第一个元素是脚本名称，从第二个元素开始才是真正的参数。
sys.exit(n)
	退出Python程序，exit(0)表示正常退出。当参数非0时，会引发一个SystemExit异常，可以在程序中捕获该异常
sys.version 
	获取Python解释程器的版本信息	
sys.maxsize 
	最大的Int值，64位平台是2**63 - 1
sys.path    
	在Python启动时，sys.path根据内建规则和PYTHONPATH变量进行初始化。
	返回模块的搜索路径
	其中第一个元素（path[0]）的值是最初调用Python解释器的脚本所在的绝对路径；
	如果是在交互式环境下查看sys.path的值，就会得到一个空字符串。
	它引导Python首先在当前目录中搜索模块。
sys.platform    
	返回操作系统平台名称
sys.stdin   
	Python的标准输入通道。
	通过改变这个属性为其他的类文件（file-like）对象，可以实现输入的重定向
	也就是说可以用其他内容替换标准输入的内容。
	所谓“标准输入”，实际上就是通过键盘输入的字符。
sys.stdout  
	标准输出的属性。
	通过将这个属性的值修改为某个文件对象，可以将本来要打印到屏幕上的内容写入文件。
sys.stderr  
	标准错误，通常也是定向到屏幕的，可以粗糙地认为是一个输出错误信息的特殊的标准输出流。
sys.exc_info()  
	返回异常信息三元元组
sys.getdefaultencoding()    
	获取系统当前编码，默认为utf-8
sys.setdefaultencoding()    
	设置系统的默认编码
sys.getfilesystemencoding() 
	获取文件系统使用编码方式，默认是utf-8
sys.modules 
	以字典的形式返回所有当前Python环境中已经导入的模块
sys.builtin_module_names    
	返回一个列表，包含所有已经编译到Python解释器里的模块的名字
sys.copyright   
	当前Python的版权信息
sys.flags   
	命令行标识状态信息列表。只读。
sys.getrefcount(object) 
	返回对象的引用数量
sys.getrecursionlimit() 
	返回Python最大递归深度，默认1000
sys.setrecursionlimit()  
	后者则可以设置最大递归数目
sys.getsizeof(object[, default])    
	返回对象的大小
sys.getswitchinterval() 
	返回线程切换时间间隔，默认0.005秒
sys.setswitchinterval(interval) 
	设置线程切换的时间间隔，单位秒
sys.getwindowsversion() 
	返回当前windwos系统的版本信息
sys.hash_info   
	返回Python默认的哈希方法的参数
sys.implementation  
	前正在运行的Python解释器的具体实现，比如CPython
sys.thread_info 
	当前线程信息	
sys.byteorder  
	小端序 返回值 'little'    大端序 返回值 'big'
sys.executable    
	'E:\\Anaconda\\Anaconda\\python.exe'       
    该属性是一个字符串，在正常情况下，其值是当前运行的Python 解释器对应的可执行程序所在的绝对路径。
sys.getsizeof() 
	返回的是作用对象所占用的字节数。
sys.ps1 
	代表的是一级提示符，也就是进入Python 交互界面之后就会出现的那一个；
sys.ps2 
	是二级提示符，是在同一级内容没有输入完，换行之后新行行首的提示符。当然，两个属性都是字符串。
sys.stdout.flush()
	python的stdout是有缓冲区的，给你个例子你就知道了
		import time
		import sys
		for i in range(5):
   		print i,
    	#sys.stdout.flush()
   		time.sleep(1)
	这个程序本意是每隔一秒输出一个数字，但是如果把这句话sys.stdout.flush()注释的话
	你就只能等到程序执行完毕，屏幕上会一次性输出0，1，2，3，4。
	如果你加上sys.stdout.flush()，刷新stdout，这样就能每隔一秒输出一个数字了。


```



### 3、列子

```
sys.stdout 与 print()
	当我们用input('Please input something！')时，事实上是先输出提示信息，然后捕获输入。 
	以下两组等价：

	s = input('Please input something！')
	和
	print('Please input something！',)  
		//逗号表示不换行
	s = sys.stdin.readline()[:-1]  
		//-1 可以抛弃输入流中的'\n' 换行符

```

```
sys.stdin 与 input()
	当我们print(obj)的时候，事实上是调用了sys.stdout.write(obj+'\n')
	将内容打印到控制台（默认是显示器），然后追加一个换行符。
	以下两行等价：

	sys.stdout.write('hello'+'\n') 
	和
	print('hello')
		//从控制台重定向到文件,默认情况下sys.stdout指向控制台。
		//如果把文件对象赋值给sys.stdout，那么print ()调用的就是文件对象的write()方法。
```



## 2、os模块

### 1、简介

```
os就是“operating system”的缩写，顾名思义，os模块提供的就是各种 Python 程序与操作系统进行交互的接口。
	通过使用os模块，一方面可以方便地与操作系统进行交互，另一方面页可以极大增强代码的可移植性。
	如果该模块中相关功能出错，会抛出OSError异常或其子类异常。
	
	注意：如果是读写文件的话，建议使用内置函数open()
		 如果是路径相关的操作，建议使用os的子模块os.path
		 如果要逐行读取多个文件，建议使用fileinput模块
		 要创建临时文件或路径，建议使用tempfile模块
		 要进行更高级的文件和路径操作则应当使用shutil模块。
		 
		 当然，使用os模块可以写出操作系统无关的代码并不意味着os无法调用一些特定系统的扩展功能
		 但要切记一点：一旦这样做就会极大损害代码的可移植性。
	
	 	 此外，导入os模块时还要小心一点，千万不要为了图调用省事儿而将os模块解包导入
		 即不要使用from os import *来导入os模块
		 否则os.open()将会覆盖内置函数open()从而造成预料之外的错误。
		 
os模块中大多数接受路径作为参数的函数也可以接受“文件描述符”作为参数。
	文件描述符：file descriptor
	在 Python 文档中简记为 fd，是一个与某个打开的文件对象绑定的整数，可以理解为该文件在系统中的编号。
```



### 2、命令

```
os.WEXITSTATUS()
	os.WEXITSTATUS(status)
	
	如果os.WIFEXITED(status)是真的
	如果os.WIFEXITED(status)为False，则该方法返回无意义的值。
	
	参数：
	status:此参数采用os.system()，os.wait()方法或os.waitpid()方法返回的过程状态代码(整数值)。
	返回类型：此方法返回exit(2)系统调用中的进程使用的整数值。
```



```
os.environ         
	一个dictionary 包含环境变量的映射关系
os.name            
	显示当前使用的平台
os.sep             
	显示当前平台下路径分隔符
os.linesep         
	给出当前平台使用的行终止符
os.remove('filename')        
	删除一个文件
os.rename("oldname","newname")  
	重命名文件
os.getcwd()                     
	示当前python脚本工作路径
os.chdir(dir)                   
	改变当前目录，注意windows下用到转义
os.listdir('dirname')           
	返回指定目录下的所有文件和目录名
os.mkdir("dirname")             
	生成目录        
os.makedirs('dirname/dirname')  
	可生成多层递规目录
os.rmdir('dirname')             
	删除单级目录
os.getlogin()                   
	得到用户登录名称
os.getenv(‘key’)                
	得到环境变量配置
os.putenv(‘key’)                
	设置环境变量
os.system()                     
	运行shell命令，注意：这里是打开一个新的shell，运行命令，当命令结束后，关闭shell。       
os.access(path, mode)            
	检验权限模式
	
os.chflags(path, flags)  
	设置路径的标记为数字标记。
os.chmod(path, mode)     
	更改权限
os.chown(path, uid, gid) 
	更改文件所有者
os.chroot(path)          
	改变当前进程的根目录
os.close(fd)             
	关闭文件描述符 fd
os.closerange(fd_low, fd_high) 
	关闭所有文件描述符，从 fd_low (包含) 到 fd_high (不包含), 错误会忽略
os.dup(fd)               
	复制文件描述符 fd
os.dup2(fd, fd2)         
	将一个文件描述符 fd 复制到另一个 fd2
os.fchdir(fd)            
	通过文件描述符改变当前工作目录
os.fchmod(fd, mode)      
	改变一个文件的访问权限，该文件由参数fd指定，参数mode是Unix下的文件访问权限。
os.fchown(fd, uid, gid)  
	修改一个文件的所有权，这个函数修改一个文件的用户ID和用户组ID，该文件由文件描述符fd指定。
os.fdatasync(fd)         
	强制将文件写入磁盘，该文件由文件描述符fd指定，但是不强制更新文件的状态信息。
os.fdopen(fd[, mode[, bufsize]]) 
	通过文件描述符 fd 创建一个文件对象，并返回这个文件对象
os.fpathconf(fd, name)   
	返回一个打开的文件的系统配置信息。name为检索的系统配置的值
	它也许是一个定义系统值的字符串，这些名字在很多标准中指定（POSIX.1, Unix 95, Unix 98, 和其它）。
os.fstat(fd)             
	返回文件描述符fd的状态，像stat()。
os.fstatvfs(fd)
	返回包含文件描述符fd的文件的文件系统的信息，像 statvfs()
os.fsync(fd)
	强制将文件描述符为fd的文件写入硬盘。
os.ftruncate(fd, length)
	裁剪文件描述符fd对应的文件, 所以它最大不能超过文件大小。
os.getcwd()
	返回当前工作目录
os.getcwdu()
	返回一个当前工作目录的Unicode对象
os.isatty(fd)
	如果文件描述符fd是打开的，同时与tty(-like)设备相连，则返回true, 否则False。
os.lchflags(path, flags)
	设置路径的标记为数字标记，类似 chflags()，但是没有软链接
os.lchmod(path, mode)
	修改连接文件权限
os.lchown(path, uid, gid)
	更改文件所有者，类似 chown，但是不追踪链接。
os.link(src, dst)
	创建硬链接，名为参数 dst，指向参数 src
os.listdir(path)
	功能：返回指定目录下的所有目录和文件   	
	返回path指定的文件夹包含的文件或文件夹的名字的列表。
os.lseek(fd, pos, how)
	设置文件描述符 fd当前位置为pos, how方式修改: SEEK_SET 或者 0 
	设置从文件开始的计算的pos; SEEK_CUR或者 1 则从当前位置计算
	os.SEEK_END或者2则从文件尾部开始. 在unix，Windows中有效
os.lstat(path)
	像stat(),但是没有软链接
os.major(device)
	从原始的设备号中提取设备major号码 (使用stat中的st_dev或者st_rdev field)。
os.makedev(major, minor)
	以major和minor设备号组成一个原始设备号
os.makedirs(path[, mode])
	递归文件夹创建函数。像mkdir(), 但创建的所有intermediate-level文件夹需要包含子文件夹。
os.minor(device)
	从原始的设备号中提取设备minor号码 (使用stat中的st_dev或者st_rdev field )。
os.mkdir(path[, mode])
	以数字mode的mode创建一个名为path的文件夹.默认的 mode 是 0777 (八进制)。
os.mkfifo(path[, mode])
	创建命名管道，mode 为数字，默认为 0666 (八进制)
os.mknod(filename[, mode=0600, device])
	创建一个名为filename文件系统节点（文件，设备特别文件或者命名pipe）。
os.open(file, flags[, mode])
	打开一个文件，并且设置需要的打开选项，mode参数是可选的
os.openpty()
	打开一个新的伪终端对。返回 pty 和 tty的文件描述符。
os.pathconf(path, name)
	返回相关文件的系统配置信息。
os.pipe()
	创建一个管道. 返回一对文件描述符(r, w) 分别为读和写
os.popen(command[, mode[, bufsize]])
	从一个 command 打开一个管道
os.read(fd, n)
	从文件描述符 fd 中读取最多 n 个字节，返回包含读取字节的字符串，文件描述符 
	fd对应文件已达到结尾, 返回一个空字符串。
os.readlink(path)
	返回软链接所指向的文件
os.remove(path)
	删除路径为path的文件。如果path 是一个文件夹，将抛出OSError; 查看下面的rmdir()删除一个 directory。
os.removedirs(path)
	递归删除目录。
os.rename(src, dst)
	重命名文件或目录，从 src 到 dst
os.renames(old, new)
	递归地对目录进行更名，也可以对文件进行更名。
os.rmdir(path)
	删除path指定的空目录，如果目录非空，则抛出一个OSError异常。
os.stat(path)
	获取path指定的路径的信息，功能等同于C API中的stat()系统调用。
os.stat_float_times([newvalue])
	决定stat_result是否以float对象显示时间戳
os.statvfs(path)
	获取指定路径的文件系统统计信息
os.symlink(src, dst)
	创建一个软链接
os.tcgetpgrp(fd)
	返回与终端fd（一个由os.open()返回的打开的文件描述符）关联的进程组
os.tcsetpgrp(fd, pg)
	设置与终端fd（一个由os.open()返回的打开的文件描述符）关联的进程组为pg。
os.tempnam([dir[, prefix]])
	返回唯一的路径名用于创建临时文件。
os.tmpfile()
	返回一个打开的模式为(w+b)的文件对象 .这文件对象没有文件夹入口，没有文件描述符，将会自动删除。
os.tmpnam()
	为创建一个临时文件返回一个唯一的路径
os.ttyname(fd)
	返回一个字符串，它表示与文件描述符fd 关联的终端设备。如果fd 没有与终端设备关联，则引发一个异常。
os.unlink(path)
	删除文件路径
os.utime(path, times)
	返回指定的path文件的访问和修改的时间。
os.walk(top[, topdown=True[, onerror=None[, followlinks=False]]])
	输出在文件夹中的文件名通过在树中游走，向上或者向下。
	能够把给定的目录下的所有目录和文件遍历出来
os.write(fd, str)
	写入字符串到文件描述符 fd中. 返回实际写入的字符串长度
os.name 函数    
	功能：获取当前使用的操作系统（获取信息不够详细）          
    其中 'nt' 是 windows，'posix' 是 linux 或者 unix
	
```









### 3、os.path 模块

#### 1、简介

```
os.path     
	获取文件的属性信息，os.path编写平台无关的程序
	
```

#### 2、命令

```
os.path.join(path, name)             
	连接目录和文件名。
os.path.basename(path)               
	返回文件名
os.path.abspath()                   
	获取绝对路径
os.path.abspath("1.txt") == os.path.join(os.getcwd(),"1.txt")
os.path.split()                     
	用于分开一个目录名称中的目录部分和文件名称部分。
os.pardir                            
	表示当前平台下上一级目录的字符 ..
os.path.join(path, name)             
	连接目录和文件名。
os.path.basename(path)               
	返回文件名
os.path.dirname(path)                
	返回文件路径
os.path.getctime("/root/1.txt")      
	返回1.txt的ctime(创建时间)时间戳
os.path.exists(os.getcwd())          
	判断文件是否存在
os.path.isfile(os.getcwd())          
	判断是否是文件名，1是0否
os.path.isdir('c:\Python\temp')      
	判断是否是目录，1是0否
os.path.islink('/home/111.sql')      
	是否是符号连接，windows下不可用
os.path.ismout(os.getcwd())          
	是否是文件系统安装点，windows下不可用
os.path.samefile(os.getcwd(), '/home')  
	看看两个文件名是不是指的是同一个文件
os.path.relpath                      
	把绝对路径转换为相对路径 
	如果输入的第二个路径为空白，那么相对路径是从第三个文件夹（包括第三个文件夹）开始的   
	如果输入第二个路径为空白，但是路径中的文件夹不超过三个的情况下，相对路径默认为当前位置
os.path.walk('/home/huaying', test_fun, "a.c")          
	遍历/home/huaying下所有子目录包括本目录,对于每个目录都会调用函数test_fun。
```







os.listdir(path)                     #方法用于返回指定的文件夹包含的文件或文件夹的名字的列表。它不包括 . 和 .. 即使它在文件夹中。只支持在 Unix, Windows 下，默认是 utf8 编码，所以需要转。返回指定路径下的文件和文件夹列表。


#### 3、os.walk和os.path.walk的区别

​        

    函数声明：os.walk(top, topdown=True,οnerrοr=None)
     	1、参数top表示需要遍历的顶级目录的路径。
        2、参数topdown的默认值是“True”表示首先返回顶级目录下的文件，然后再遍历子目录中的文件。
        	当topdown的值为"False"时，表示先遍历子目录中的文件，然后再返回顶级目录下的文件。
        3、参数onerror默认值为"None"，表示忽略文件遍历时的错误。
        	如果不为空，则提供一个自定义函数提示错误信息后继续遍历或抛出异常中止遍历。
        返回值：函数返回一个元组，含有三个元素。
        这三个元素分别是：每次遍历的路径名、路径下子目录列表、目录下文件列表。





    函数声明：os.path.walk(top, func, arg)
    	1、参数top表示需要遍历的目录路径。
        2、参数func表示回调函数，即对遍历路径进行处理的函数。
        	所谓回调函数，是作为某个函数的参数使用，当某个时间触发时，程序将调用定义好的回调函数处理某个任务。
        	
        	注意：walk的回调函数必须提供三个参数：
        		第1个参数为os.path.walk的参数arg
        		第2个参数表示目录dirname
        		第3个参数表示文件列表names
        		
        	注意：os.path.walk的回调函数中的文件列表不和os.walk()那样将子目录和文件分开
        		而是混为了一谈，需要在回调函数中判断是文件还是子目录。
        	
        3、参数arg是传递给回调函数的元组，为回调函数提供处理参数，arg可以为空。
        	回调函数的第1个参数就是用来接收这个传入的元组的。
    
        4、过程：
        	以top为根的目录树中的每一个目录(包含top自身，如果它是一个目录)，以参数(arg, dirname, names)调			用回调函数funct。
       		参数dirname指定访问的目录，参数names列出在目录中的文件(从os.listdir(dirname)中得到)。
       		回调函数可以修改names改变dirname下面访问的目录的设置，例如，避免访问树的某一部分。
       		(由names关联的对象必须在合适的位置被修改，使用del或slice指派。)
       		注意：
       			符号连接到目录不被作为一个子目录处理，并且因此walk()将不访问它们。
       			访问连接的目录必须以os.path.islink(file)和os.path.isdir(file)标识它们
       			并且必须调用walk()。
    
        5、区别：
        	os.path.walk()与os.walk()产生的文件名列表并不相同。
        	os.walk()产生目录树下的目录路径和文件路径
        	而os.path.walk()只产生文件路径（是子目录与文件的混合列表）。





## 3、subprocess模块

### 1、简介

```
subprocess模块可以生成新的进程，连接到它们的input/output/error管道，同时获取它们的返回码。
```

### 2、基本操作方法

#### 1、subprocess的函数

```
		run、call、check_call、check_output、getoutput、getstatusoutput
```

##### 1、参数简介

```
1、 args：启动进程的参数，默认为字符串序列（列表或元组），也可为字符串（设为字符串时一般需将				shell参数赋值为True）；

2、 shell：shell为True，表示args命令通过shell执行，则可访问shell的特性；

3、 check：check为True时，表示执行命令的进程以非0状态码退出时会抛出；									subprocess.CalledProcessError异常；check为False时，状态码为非0退出时不会抛出异常；

4、 stdout、stdin、stderr：分别表示程序标准标输出、输入、错误信息；

5、 run函数返回值为CompletedProcess类，若需获取执行结果，可通过获取返回值的stdout和stderr来捕获；

6、check_output函数若需捕获错误信息，可通过stderr=subprocess.STDOUT来获取；

7、cmd：参数，字符串类型；
```



##### 2、subprocess.run

```
1、subprocess.run(args[, stdout, stderr, shell ...])

执行args命令，返回值为CompletedProcess类；
若未指定stdout，则命令执行后的结果输出到屏幕上，函数返回值CompletedProcess中包含有args和returncode；
若指定有stdout，则命令执行后的结果输出到stdout中，函数返回值CompletedProcess中包含有args、returncode和stdout；
若执行成功，则returncode为0；若执行失败，则returncode为1；
若想获取args命令执行后的输出结果，命令为：output = subprocess.run(args, stdout=subprocess.PIPE).stdout
```



##### 3、subprocess.call

```
1、subprocess.call(args[, stdout, ...])

执行args命令，返回值为命令执行状态码；
若未指定stdout，则命令执行后的结果输出到屏幕；
若指定stdout，则命令执行后的结果输出到stdout；
若执行成功，则函数返回值为0；若执行失败，则函数返回值为1；
（类似os.system）
```



##### 4、subprocess.check_call

```
1、subprocess.check_call(args[, stdout, ...])

执行args命令，返回值为命令执行状态码；
若未指定stdout，则命令执行后的结果输出到屏幕；
若指定stdout，则命令执行后的结果输出到stdout；
若执行成功，则函数返回值为0；若执行失败，抛出异常；
（类似subprocess.run(args, check=True)）
```

​	

##### 5、subprocess.check_output

```
1、subprocess.check_output(args[, stderr, ...])

执行args命令，返回值为命令执行的输出结果；
若执行成功，则函数返回值为命令输出结果；若执行失败，则抛出异常；
（类似subprocess.run(args, check=True, stdout=subprocess.PIPE).stdout）
```



##### 6、subprocess.getoutput

```
1、subprocess.getoutput(cmd)：执行cmd命令，返回值为命令执行的输出结果（字符串类型）；
注：执行失败，不会抛出异常（类似os.popen(cmd).read()）；
```



##### 7、subprocess.getstatusoutput

```
1、subprocess.getstatusoutput(cmd)：执行cmd命令，返回值为元组类型(命令执行状态, 命令执行的输出结果)；
元组中命令执行状态为0，表示执行成功；命令执行状态为1，表示执行失败；
```







#### 2、 subprocess.Popen类

##### 1、介绍

```
subprocess.Popen类用于在一个新进程中执行一个子程序，上述subprocess函数均是基于subprocess.Popen类；

```



##### 2.操作

```
这个模块主要就提供一个类Popen：

class subprocess.Popen( args,  bufsize=0,  executable=None,  stdin=None,  stdout=None,  stderr=None,  preexec_fn=None,  close_fds=False,  shell=False
,  cwd=None,  env=None,  universal_newlines=False,  startupinfo=None,  creationflags=0)

```



##### 3、参数

```
args 
	字符串或者列表
	（列表或元组，shell为默认值False），使用字符串时，需将shell赋值为True）；

bufsize  
	指定缓冲策略，
    0 无缓冲 不缓冲
    1 行缓冲
    其他正值 缓冲区大小
    负值 采用默认系统缓冲(一般是全缓冲) 系统默认值0
    
executable
	一般不用，args字符串或列表第一项表示程序名
	
stdin
	表示子程序标准输出
	
stdout
	表示子程序标准输入
	
stderr
	表示子程序标准错误
	stderr也可为subprocess.STDOUT：表示将子程序的标准错误输出重定向到了标准输出
	
None 
	若为None：表示没有任何重定向，子进程会继承父进程；
	
PIPE 创建管道
	代表打开通向标准流的管道，创建一个新的管道；
	一个有效的文件描述符、文件对象或None。
    文件对象
    文件描述符(整数)
    
preexec_fn
	钩子函数， 在fork和exec之间执行。(unix)
	
close_fds
    unix 下执行新进程前是否关闭0/1/2之外的文件
    windows下不继承还是继承父进程的文件描述符
    
shell
	默认为False，若args为序列时，shell=False；若args为字符串时，shell=True，表示通过shell执行命令；
    为真的话
    unix下相当于args前面添加了 "/bin/sh“ ”-c”
    window下，相当于添加"cmd.exe /c"
    
cwd
	设置工作目录
	cwd：默认值为None；若非None，则表示将会在执行这个子进程之前改变当前工作目录；
	
env
	设置环境变量
	env：用于指定子进程的环境变量。若env为None，那么子进程的环境变量将从父进程中继承；若env非None，则表示	   子程序的环境变量由env值来设置，它的值必须是一个映射对象。
	
universal_newlines
	各种换行符统一处理成 '\n'
	不同系统的换行符不同。若True，则该文件对象的stdin，stdout和stderr将会以文本流方式打开；否则以二进制流	方式打开。
	
startupinfo
	window下传递给CreateProcess的结构体
	
creationflags
	windows下，传递CREATE_NEW_CONSOLE创建自己的控制台窗口
	
```



##### 4、列子1

```
subprocess.Popen(["gedit","abc.txt"]) 
subprocess.Popen("gedit abc.txt")
	这两个之中，后者将不会工作。因为如果是一个字符串的话，必须是程序的路径才可以。
	但是下面的可以工作
	
subprocess.Popen("gedit abc.txt", shell=True)
	这是因为它相当于
subprocess.Popen(["/bin/sh", "-c", "gedit abc.txt"])
	都成了sh的参数，就无所谓了


在Windows下，下面的却又是可以工作的
subprocess.Popen(["notepad.exe", "abc.txt"])
subprocess.Popen("notepad.exe abc.txt")
	这是由于windows下的api函数CreateProcess接受的是一个字符串。
	即使是列表形式的参数，也需要先合并成字符串再传递给api函数。
	
类似上面
subprocess.Popen("notepad.exe abc.txt" shell=True)
	等价于
subprocess.Popen("cmd.exe /C "+"notepad.exe abc.txt" shell=True)
```



##### 5、列子2

```
call() 执行程序，并等待它完成
        def call(*popenargs, **kwargs):  return Popen(*popenargs, kwargs).wait()
        
check_call() 调用前面的call，如果返回值非零，则抛出异常      
        def check_call(*popenargs, kwargs):  retcode = call(*popenargs, kwargs) 
        if retcode:  cmd = kwargs.get("args")  
        raise CalledProcessError(retcode, cmd)  return 0    
        
check_output() 执行程序，并返回其标准输出
        def check_output(*popenargs, kwargs):  process = Popen(*popenargs, stdout=PIPE, **kwargs)  output, unused_err = process.communicate()  retcode = process.poll() 
        if retcode:  cmd = kwargs.get("args")  
        raise CalledProcessError(retcode, cmd, output=output)  return output    
        
```





##### 5、Popen对象

```
subprocess.Popen(args[, bufsize, stdin, stdout, stderr, ...])：
Popen类的构造函数，返回结果为subprocess.Popen对象；
	PopenObject

该对象提供有不少方法函数可用。

poll()
	PopenObject.poll() 
	用于检查命令是否已经执行结束，若结束返回状态码；若未结束返回None；

wait()
	PopenObject.wait([timeout, endtime])
	等待子进程结束，并返回状态码；若超过timeout(s)进程仍未结束，则抛出异常；

communicate()
	PopenObject.communicate([input, timeout])
	与进程进行交互（如发送数据到stdin、读取stdout和stderr数据），它会阻塞父进程，直到子进程完成；
	参数是标准输入，返回标准输出和标准出错
	
	input：表示将发送到子进程的字符串数据，默认为None；
	timeout：超时判断，若超过timeout秒后仍未结束则抛出TimeoutExpired异常；
	communicate返回值：一个元组(stdout_data, stderr_data)

send_signal()
	PopenObject.send_signal(signal)
	发送信号signal给子进程；

terminate()
	PopenObject.terminate()
	停止子进程；
	终止进程，unix对应的SIGTERM信号，windows下调用api函数TerminateProcess()

kill()
	PopenObject.kill()
	杀死进程(unix对应SIGKILL信号)，windows下同上

stdin
	若PopenObject中stdin为PIPE，则返回一个可写流对象
	若encoding或errors参数被指定或universal_newlines参数为True，则此流是一个文件流，否则为字节流。
	若PopenObject中stdin不是PIPE，则属性为None。
	stdin输入流非None，可执行写操作即PopenObject.stdin.write(s)

stdout
	若PopenObject中stdout为PIPE，则返回一个可读流对象；
	若encoding或errors参数被指定或universal_newlines参数为True，则此流是一个文件流，否则为字节流。
	若PopenObject中stdout不是PIPE，则属性为None。
	stdout输出流非None，可执行读操作即PopenObject.stdout.read()或.readlines()
	
stderr
	若PopenObject中stderr为PIPE，则返回一个可读流对象；
	若encoding或errors参数被指定或universal_newlines参数为True，则此流是一个文件流，否则为字节流。
	若PopenObject中stderr不是PIPE，则属性为None。
	stderr错误流非None，可执行读操作即PopenObject.stderr.read()或.readlines()

参数中指定PIPE时，有用

	pid

	进程id

	returncode
	进程返回值
```



## 4、shlex

```
shlex.split(s[, comments[, posix]])
	使用类似shell的语法分割字符串s，相当于特殊的tokenizer。
	经常用在subprocess.Popen的第一个参数中。
	
	其中 'nt' 是 windows，'posix' 是 linux 或者 unix
	POSIX模式下的parser规则：
		双引号被去掉，并且不分割单词:
    		 shlex.split('hello","word')
   			 ['hello,word']

		非引号转义字符保留下一个字符的字面形式
		引用符号之间的字符串保留字面形式
   			 shlex.split('hello "i am glad" word')
   			 ['hello', 'i am glad', 'word']

		转义引用符号之间的字符串保留字面形式
   			  shlex.split('hello \"i am glad\" word')
  			  ['hello', 'i am glad', 'word']


```



## 5、shutil

### 1、命令

```
目录和文件操作

shutil.copyfileobj(fsrc, fdst[, length])
	将文件类对象fsrc的内容拷贝到文件类对象fdst。 
	整数值length 给出则为缓冲区大小。
    特别地   ， length    为负值表示拷贝数据时不对源数据进行分块循环处理；
    默认情况下会分块读取数据以避免不受控制的内存消耗。 
    请注意如果fsrc对象的当前文件位置不为0，则只有从当前文件位置到文件末尾的内容会被拷贝。

shutil.copyfile(src, dst, *, follow_symlinks=True)
	将名为src的文件的内容（不包括元数据）拷贝到名为dst的文件并以尽可能高效的方式返回dst。 
	src 和 dst 均为路径类对象或以字符串形式给出的路径名。
	dst 必须是完整的目标文件名；
	对于接受目标目录路径的拷贝请参见 copy()。 
	如果 src 和 dst 指定了同一个文件，则将引发 SameFileError。

	目标位置必须是可写的；
	否则将引发 OSError 异常。 
	如果 dst 已经存在，它将被替换。 特殊文件如字符或块设备以及管道无法用此函数来拷贝。		
	如果 follow_symlinks 为假值且 src 为符号链接，则将创建一个新的符号链接而不是拷贝 src 所指向的文件。

exception shutil.SameFileError
	此异常会在 copyfile() 中的源和目标为同一文件时被引发。
	
shutil.copymode(src, dst, *, follow_symlinks=True)
	从 src 拷贝权限位到 dst。
    文件的内容、所有者和分组将不受影响。 
    src 和 dst 均为路径类对象或字符串形式的路径名。
    如果 follow_symlinks 为假值，并且 src 和 dst 均为符号链接，copymode() 将尝试修改 dst 本身的模式		（而非它所指向的文件）。 
    如果 copymode() 无法修改本机平台上的符号链接，而它被要求这样做，它将不做任何操作即返回。

shutil.copystat(src, dst, *, follow_symlinks=True)
	从 src 拷贝权限位、最近访问时间、最近修改时间以及旗标到 dst。 
	在 Linux上，copystat() 还会在可能的情况下拷贝“扩展属性”。 
	文件的内容、所有者和分组将不受影响。 src 和 dst 均为路径类对象或字符串形式的路径名。

	如果 follow_symlinks 为假值，并且 src 和 dst 均指向符号链接，copystat() 将作用于符号链接本身而非			该符号链接所指向的文件 — 从 src 符号链接读取信息，并将信息写入 dst 符号链接。
	注解 并非所有平台者提供检查和修改符号链接的功能。 
	Python 本身可以告诉你哪些功能是在本机上可用的。
	如果os.chmod in os.supports_follow_symlinks 为True
		则 copystat() 可以修改符号链接的权限位。
	如果 os.utime in os.supports_follow_symlinks 为 True
		则 copystat() 可以修改符号链接的最近访问和修改时间。
	如果 os.chflags in os.supports_follow_symlinks 为 True
		则 copystat() 可以修改符号链接的旗标。 (os.chflags 不是在所有平台上均可用。)
	
shutil.copy(src, dst, *, follow_symlinks=True)
	将文件 src 拷贝到文件或目录 dst。 src 和 dst 应为 路径类对象 或字符串。
    如果 dst 指定了一个目录，文件将使用 src 中的基准文件名拷贝到 dst 中。 
    将返回新创建文件所对应的路径。

	如果 follow_symlinks 为假值且 src 为符号链接，则 dst 也将被创建为符号链接。
    如果 follow_symlinks 为真值且 src 为符号链接，dst 将成为 src 所指向的文件的一个副本。

	copy() 会拷贝文件数据和文件的权限模式 (参见 os.chmod())。
    其他元数据，例如文件的创建和修改时间不会被保留。 要保留所有原有的元数据，请改用 copy2() 。
    
shutil.copy2(src, dst, *, follow_symlinks=True)
	类似于 copy()，区别在于 copy2() 还会尝试保留文件的元数据。
	当 follow_symlinks 为假值且 src 为符号链接时
		opy2() 会尝试将来自 src 符号链接的所有元数据拷贝到新创建的 dst 符号链接。 
	但是，此功能不是在所有平台上均可用。 
	在此功能部分或全部不可用的平台上，copy2() 将尽量保留所有元数据；
	copy2() 一定不会由于无法保留文件元数据而引发异常。

shutil.ignore_patterns(*patterns)
	这个工厂函数会创建一个函数，它可被用作 copytree() 的 ignore 可调用对象参数
	以忽略那些匹配所提供的 glob 风格的 patterns 之一的文件和目录。 

shutil.copytree(src, dst, symlinks=False, ignore=None, copy_function=copy2, 				ignore_dangling_symlinks=False, dirs_exist_ok=False)

	将以 src 为根起点的整个目录树拷贝到名为 dst 的目录并返回目标目录。 
	dirs_exist_ok 指明是否要在 dst 或任何丢失的父目录已存在的情况下引发异常。
	目录的权限和时间会通过 copystat() 来拷贝，单个文件则会使用 copy2() 来拷贝。
	如果 symlinks 为真值，源目录树中的符号链接会在新目录树中表示为符号链接，
	并且原链接的元数据在平台允许的情况下也会被拷贝
	如果为假值或省略，则会将被链接文件的内容和元数据拷贝到新目录树。

	当 symlinks 为假值时，如果符号链接所指向的文件不存在
	则会在拷贝进程的末尾将一个异常添加到 Error 异常中的错误列表。 
	如果你希望屏蔽此异常那就将可选的 ignore_dangling_symlinks 旗标设为真值。 

	如果给出了 ignore，它必须是一个可调用对象
	该对象将接受 copytree() 所访问的目录以及 os.listdir() 所返回的目录内容列表作为其参数。 
	由于 copytree() 是递归地被调用的，ignore 可调用对象对于每个被拷贝目录都将被调用一次。
    该可调用对象必须返回一个相对于当前目录的目录和文件名序列（即其第二个参数的子集）
    随后这些名称将在拷贝进程中被忽略。 
    ignore_patterns() 可被用于创建这种基于 glob 风格模式来忽略特定名称的可调用对象。

shutil.rmtree(path, ignore_errors=False, onerror=None)
	删除一个完整的目录树；path 必须指向一个目录（但不能是一个目录的符号链接）。
    如果 ignore_errors 为真值，删除失败导致的错误将被忽略
    如果为假值或是省略，此类错误将通过调用由 onerror 所指定的处理程序来处理
    	或者如果此参数被省略则将引发一个异常。
    	
	如果提供了 onerror，它必须为接受三个形参的可调用对象: function, path 和 excinfo。
	第一个形参 function 是引发异常的函数；它依赖于具体的平台和实现。
    第二个形参 path 将是传递给 function 的路径名。
    第三个形参 excinfo 将是由 sys.exc_info() 所返回的异常信息。 
    由 onerror 所引发的异常将不会被捕获。
    
rmtree.avoids_symlink_attacks
	指明当前平台和实现是否提供防御符号链接攻击的 rmtree() 版本。 
	目前它仅在平台支持基于 fd 的目录访问函数时才返回真值。

shutil.move(src, dst, copy_function=copy2)
	递归地将一个文件或目录 (src) 移至另一位置 (dst) 并返回目标位置。
	如果目标是已存在的目录，则 src 会被移至该目录下。
    如果目标已存在但不是目录，它可能会被覆盖，具体取决于 os.rename() 的语义。

	如果目标是在当前文件系统中，则会使用 os.rename()。 
	在其他情况下，src 将被拷贝至 dst，使用的函数为 copy_function，然后目标会被移除。 
	对于符号链接，则将在 dst 之下或以其本身为名称创建一个指向 src 目标的新符号链接，并且 src 将被移除。

	如果给出了 copy_function，则它必须为接受两个参数 src 和 dst 的可调用对象，
	并将在 os.rename() 无法使用时被用来将 src 拷贝到 dst。 
	如果源是一个目录，则会调用 copytree()，并向它传入 copy_function()。 
	默认的 copy_function 是 copy2()。 
	

shutil.disk_usage(path)
	返回给定路径的磁盘使用统计数据，形式为一个 named tuple，其中包含 total, used 和 free 属性
	分别表示总计、已使用和未使用空间的字节数。 path 可以是一个文件或是一个目录。

shutil.chown(path, user=None, group=None)
	修改给定 path 的所有者 user 和/或 group。

	user 可以是一个系统用户名或 uid；group 同样如此。 
	
shutil.which(cmd, mode=os.F_OK | os.X_OK, path=None)
	返回当给定的 cmd 被调用时将要运行的可执行文件的路径。 如果没有 cmd 会被调用则返回 None。
	mode 是一个传递给 os.access() 的权限掩码，在默认情况下将确定文件是否存在并且为可执行文件。
	当未指定 path 时，将会使用 os.environ() 的结果，返回 "PATH" 的值或回退为 os.defpath。

exception shutil.Error
	此异常会收集在多文件操作期间所引发的异常。 
	对于 copytree()，此异常参数将是一个由三元组 (srcname, dstname, exception) 构成的列表。	
	
```



## 6、argparse模块

### 1、简介

```
argparse是一个Python模块：命令行选项、参数和子命令解析器。
	argparse 模块可以让人轻松编写用户友好的命令行接口。程序定义它需要的参数
		然后 argparse 将弄清如何从 sys.argv 解析出那些参数。 
	argparse 模块还会自动生成帮助和使用手册，并在用户给程序传入无效参数时报出错误信息。
```

### 2、使用方法

```
创建解析器
	parser = argparse.ArgumentParser(description='Process some integers.')
	使用 argparse 的第一步是创建一个 ArgumentParser 对象。
	ArgumentParser 对象包含将命令行解析成 Python 数据类型所需的全部信息。
	
添加参数
	parser.add_argument('integers', metavar='N', type=int, nargs='+', help='an integer 		for the accumulator')
	
	给一个 ArgumentParser 添加程序参数信息是通过调用 add_argument() 方法完成的。
解析参数
	 parser.parse_args(['--sum', '7', '-1', '42'])
 	 Namespace(accumulate=<built-in function sum>, integers=[7, -1, 42])

	 ArgumentParser 通过 parse_args() 方法解析参数。
```

### 3、ArgumentParser 对象

```
class argparse.ArgumentParser(prog=None, usage=None, description=None, epilog=None, 		parents=[], formatter_class=argparse.HelpFormatter, prefix_chars='-', 					fromfile_prefix_chars=None, argument_default=None, conflict_handler='error', 			add_help=True, allow_abbrev=True)
	
prog - 程序的名称（默认：sys.argv[0]）
usage - 描述程序用途的字符串（默认值：从添加到解析器的参数生成）
description - 在参数帮助文档之前显示的文本（默认值：无）
epilog - 在参数帮助文档之后显示的文本（默认值：无）
parents - 一个 ArgumentParser 对象的列表，它们的参数也应包含在内
formatter_class - 用于自定义帮助文档输出格式的类
prefix_chars - 可选参数的前缀字符集合（默认值：’-’）
fromfile_prefix_chars - 当需要从文件中读取其他参数时，用于标识文件名的前缀字符集合（默认值：None）
argument_default - 参数的全局默认值（默认值： None）
conflict_handler - 解决冲突选项的策略（通常是不必要的）
add_help - 为解析器添加一个 -h/--help 选项（默认值： True）
allow_abbrev - 如果缩写是无歧义的，则允许缩写长选项 （默认值：True）


```

### 4、add_argument() 方法

```
ArgumentParser.add_argument(name or flags...[, action][, nargs][, const][, default][, 		type][, choices][, required][, help][, metavar][, dest])

name or flags - 一个命名或者一个选项字符串的列表，例如 foo 或 -f, --foo。
action - 当参数在命令行中出现时使用的动作基本类型。
nargs - 命令行参数应当消耗的数目。
const - 被一些 action 和 nargs 选择所需求的常数。
default - 当参数未在命令行中出现时使用的值。
type - 命令行参数应当被转换成的类型。
choices - 可用的参数的容器。
required - 此命令行选项是否可省略 （仅选项可用）。
help - 一个此选项作用的简单描述。
metavar - 在使用方法消息中使用的参数值示例。
dest - 被添加到 parse_args() 所返回对象上的属性名。
```

## 7、参考网址

https://cloud.tencent.com/developer/section/1370514

