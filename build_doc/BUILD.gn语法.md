# BUILD.gn

## 1.简介



```
gn 意思是 generate ninja，即生成 Ninja 所需的文件（meta data），所以 gn 自称为元数据构建（meta-build）系统，也是 google chromium 团队出品，gn 的文件后缀为 .gn、.gni。

gn 类似 cmake，ninja 类似 make，cmake 最终也是生成 makefile，gn 则会生成 ninja 文件，都是为了减少手工写 make/ninja 文件的工作量。
```



```
gn 总入口  

鸿蒙把 .gn路径设计为 gn 的 root，已经替我们生成好了相关的 gn 文件，.gn文件就是 gn 编译 Harmony 的总入口 

`.gn` 和 `BUILDCONFIG.gn`

.gn

 buildconfig = "//build/lite/config/BUILDCONFIG.gn"

 root = "//build/lite" 

 buildconfig = "//build/lite/config/BUILDCONFIG.gn" root = "//build/lite" 

整个 BUILDCONFIG.gn 文件主要完成了 2 件事：

1. 定义 toochain
2. 定义 4 个 target：executable、static_library、shared_library、source_set

为后续的 gn 操作准备最基础的内容。 执行完 `.gn` 后，就要执行 root 下的 BUILD.gn 文件了 

 鸿蒙是最初进入 gn 的 root 之前，到底还做了什么？那就是下面 hb 要做的事情了。 

 `build_lite/hb` 是个 Python Package，其 setup.py 中可见其生成了一个 `hb` 的命令，入口地址是 `build/lib/hb/__main__.py` 文件中的 main 函数。 

1. 命令行使用：`hb [set|env|build]`

2. `python build.py [build|clean|debug|release]` 直接使用 `build/lite` 目录下的 build.py 文件

   hb 将每个子命令的实现放在一个文件夹中：set、build、clean、env……

   当执行 hb set、hb build 的时候进入每个文件夹中执行 exec_command() 函数。

   hb set
   执行 build/lite/hb/set/set.py 中的 exec_command() 函数:
    `set_root_path()` 和 `set_product()` 分别解析出 root 路径和产品相关信息，写入 ohos_config.json 文件中。 

    另外一个函数 `set_product()` 即是为了配置 Product，`Product` 是 hb 为产品定义的 class，包含几个静态方法，基本都是解析出配置值，写入 ohos_config.json 文件： 

   get_products()`: 获取产品信息，递归查找 `vender/` 下包含 config.json 文件的目录，每找到一个即算一个 Product，其中的 config.json 通常包括 vender 预先定义好的发行版配置。

    `repo sync` 获取的源码中的 vender 情况，所以在执行 `hb set` 时会提示 3 个选项： 

   get_device_info()`、`get_features()`、`get_components()`: 获取 vender 定义的 config.json 中的各种信息，比如:

   前面 hb set 给出的 3 个选项是这里的 product_name。device_info 包括上面的 device、board、kernel；features 和 components 是每个 subsystems 中的信息。

   每个 subsystem 对应一个源代码的目录，component 是它依赖的模块，统一放在 ohos_bundles 下面。

   hb build
   执行 build/lite/hb/build/build.py 中的 exec_command() 函数，该函数主要处理用户的入参，如：

   -b：debug 或 release
   -c：指定编译器，默认是 clang
   -t：是否编译 test suit
   -f：full，编译全部代码
   -t：是否编译 ndk，本地开发包，这也是 @ohos/build_lite 组件的一部分
   -T：单模块编译
   -v：verbose

   实例化后调用 build.build()，它会依次调用 check_in_device()、gn_build() 和 ninja_build()。

   check_in_device()：读取编译配置，根据产品选择的开发板，读取开发板 config.gni 文件内容，主要包括编译工具链、编译链接命令和选项等。
   gn_build()：调用 gn gen 命令，读取产品配置生成产品解决方案 out 目录和 ninja 文件。核心代码如下：
   ninja_build()`：调用 ninja -C out/board/product 启动编译。核心代码如下： 

   系统镜像打包：将组件编译产物打包，设置文件属性和权限，制作文件系统镜像。

    根目录下的 build.py 通常是 build/lite/build.py 的软连接，执行 `python build.py` 时会运行到 build.py 的 `build()` 函数： 
```

 ![在这里插入图片描述](https://img-blog.csdnimg.cn/f2ec1ad3f65648229c99efedf8db55c2.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2tldmluODgx,size_16,color_FFFFFF,t_70#pic_center) 



 [(参考) 鸿蒙源码导读-02：编译构建子系统_孤星泪-CSDN博客_ohos鸿蒙](https://blog.csdn.net/kevin881/article/details/119298709) 

 ![鸿蒙系统的编译流程及分析v1.0](https://dl-harmonyos.51cto.com/images/202104/b124cbc09b13c6ffedb3578f354a1126fd0611.png) 

 ![鸿蒙系统的编译流程及分析v1.0](https://dl-harmonyos.51cto.com/images/202104/68f6536807043ed729c6691cebd9560292c687.png) 

 [鸿蒙系统的编译流程及分析v1.0（更新） - HarmonyOS技术社区 - 博客园 (cnblogs.com)](https://www.cnblogs.com/HarmonyOS/p/14694150.html) 

### 1.1 .gn file



```
当gn开始的时候，它会去在当前目录以及父级目录下去查找名称为".gn"的文件。

".gn"文件所在的目录就是源根目录，也就是"//"所指的目录。 你也可以通过 --root指定别的源根目录。

".gn"文件将会在源根目录("//")下执行。

如果你指定了 --root，那么GN就会去这个目录下找 ".gn"文件。

如果你想指定不同的文件，也就是不想用 ".gn"文件了，那么你可以使用 --dotfile来指定自己的 gn 文件：

 gn gen out/Debug --root=/home/build --dotfile=/home/my_gn_file.gn
.gn 文件中的变量：



buildconfig是必须的，其余都是可选的。

arg_file_template  —— 文件路径。文件内容应该被作为默认 args.gn文件的内容当你运行 "gn args"时。
buildconfig —— 构建配置文件的路径。这个文件用于为每个toolchain设置构建的执行环境。
check_targets —— 一个标签或者标签模式列表，当运行 "gn check"或者"gn gen --check"的时候就会被检查。如果<check_targets>和<no_check_targets>都没有被指定，那么所有的目标都会被检查。如果都指定就会出错。如果这个值是空的，那么就都不检查。
no_check_targets —— 与上面的<check_targets>相反，被指定的目标标签在运行 "gn check"或者"gn gen --check"时不会被检查。所有其它的目标都会被检查。
check_system_includes —— 布尔值，控制是否系统路径(尖括号包含的路径，而不是双引号)会被检查当运行 "gn check"或者"gn gen --check"时。可以显式地检查系统文件使用 "gn check --check_system" 或者 "gn gen --check=system"。
exec_script_whitelist —— .gn/.gni文件列表，这两种文件中可以执行exec_script()函数。如果这个表被设置了，那么在调用exec_script的时候就会按照这个表来检查，如果当前执行exec_script函数的文件不在列表中，那么GN就会失败。如果未指定，那么执行exec_script的能力就不受限制。通配符是不受支持的。
      Example:
        exec_script_whitelist = [
          "//base/BUILD.gn",
          "//build/my_config.gni",
        ]
root —— 根构建目标的标签。GN构建将通过加载包含此目标名称的构建文件开始。这个默认为"//:"将会加载 "//BUILD.gn"。
script_executable —— 在<action>/<action_foreach>和exec_script调用中使用的特定Python可执行文件或其他解释器的路径。GN默认会搜索python的路径来执行这些脚本。如果为空字符串，那么在<action>/<action_foreach>和exec_script中指定的路径就会被直接执行。
secondary_cource —— 寻找输入文件的备用路径。当搜索BUILD.gn文件时，会先从源根路径(//)下面找。如果没有找到，就从辅助源根路径下面找。辅助源根必须在主源树中。
default_args —— scope({}) 包含已声明参数的默认覆盖。这些值优先于在declare_args()块中指定的默认值，但是可以被 gn --args或者 args.gn文件重写。
build_file_extension —— 如果设置为非空字符串，则将其添加到要加载的所有构建文件的名称中。GN会寻找文件名为"BUILD.$build_file_extension.gn"的构建文件。这个变量的目的是在迁移期间或者在同一个目录下面有两个独立的GN构建文件时使用。
ninja_required_version —— 设置时将指定所需要的ninja的最低版本。默认需要的版本是1.7.2。指定更高的版本可以启用一些可以使构建更加高效的新特性。
.gn 文件的例子：

  buildconfig = "//build/config/BUILDCONFIG.gn"

  check_targets = [
    "//doom_melon/*",  # Check everything in this subtree.
    "//tools:mind_controlling_ant",  # Check this specific target.
  ]

  root = "//:root"

  secondary_source = "//build/config/temporary_buildfiles/"

  default_args = {

//Default to release builds for this project.

​    is_debug = false
​    is_component_build = false
  }
```



### 1.2 标识



```
	标识是有着预定义格式的字符串，依赖图中所有的元素（目标，配置和工具链）都由标识唯一识别。通常情况下，标识看去是以下样子。“//base/test:test_support”
​	它由三部分组成：source-tree绝对路径，冒号和名称。上面这个标识指示到/base/test/BUILD.gn中查找名称是“test_support”的标识。
​	当加载构建文件时，如果在相对于source root给定的路径不存在时，GN将查找build/secondary中的辅助树。
​	该树的镜像结构主存储库是一种从其它存储库添加构建文件的方式 辅助树只是备用而不是覆盖，因此正常位置中的文件始终优先。

​	完整的标识还包括处理该标识要使用的工具链。工具链通常是以继承的方式被默认指定，当然你也可以显示指定。
​	“//base/test:test_support(//build/toolchain/win:msvc)”
​	上面这个标识会去“//build/toolchain/win”文件查到名叫”msvc”的工具链定义，那个定义会知道如何处理“test_support”这个标识。
​	如果你指向的标识就在此个build文件，你可以省略路径，而只是从冒号开始。“:base”
​	你可以以相对于当前目录的方式指定路径。标准上说，要引用非本文件中标识时，除了它要在不同上下文运行，我们建议使用绝对路径。什么是要在不同上下文运行？举个列子，一个项目它既要能构造独立版本，又可能是其它项目的子模块。
```



## 2.构建配置

### 	2.1总体构建流程



​		

```
	在当前目录查找 ".gn"文件，如果当前目录找不到，就沿着目录树往上找，直到找到这个文件。

设置".gn"文件所在的目录为"source root"(也就是源根目录，用 "//"表示)，并且解释这个文件(.gn)以寻找到构建配置的名字(buildconfig指定的文件)。
执行 .gn 文件识别到的构建配置buildconfig文件，

来设置全局变量和和默认工具链的名称(默认工具链设置函数为"set_default_toolchain()"),

在chromium中是//build/config/BUILDCONFIG.gn; 
加载root目录下的BUILD.gn文件;。任何在这个文件中设置的参数，变量，默认值等等对构建过程中所有的文件都是可见的。
根据root目录下的BUILD.gn内容加载其依赖的其它目录下的BUILD.gn文件，如果在指定位置找不到一个gn文件，GN将查找 build/secondary 的相应位置；
加载 //BUILD.gn文件。(在源根目录下)
递归评估规则，并且加载所有解决依赖关系所需要的存在于其它目录下的BUILD.gn文件。如果GN在指定位置没有找到BUILD.gn文件，GN将查找dotfile中定义的secondary_source中的对应位置。(查看帮助 "gn help dotfile")

当一个目标的依赖关系被解决后，就会把".ninja"后缀的文件写到磁盘上。
当所有目标都被解决后，就会写出一个根文件build.ninja。
注意，BUILD.gn文件名可以由 .gn参数调整，例如 build_file_extension
当一个目标的依赖都解决了，编译出.ninja文件保存到out_dir/dir，
		例如./out/arm/obj/ui/web_dialogs/web_dialogs.ninja;
		当所有的目标都解决了， 编译出一个根 build.ninja 文件存放在out_dir根目录
```



### 2.2构建配置文件



```
		第一个要执行的是构建配置文件，它在指示源码库根目录的“.gn”文件中指定。Chrome源码树中该文件是“//build/config/BUILDCONFIG.gn”。
​		整个系统有且只有一个构造配置文件。
​		除设置其它build文件执行时的工作域外，该文件还设置参数、变量、默认值，等等。设置在该文件的值对所有build文件可见。
```



### 2.3构建参数

​	

```
	参数可以从命令行传入。 你可以通过declare_args声明接受哪些参数和指定默认值。
​		有关参数是如何工作的，请参阅gn help buildargs。 有关声明它们的细节，请参阅gn help declare_args。
​		在给定作用域内多次声明给定的参数是一个错误。 通常，参数将在导入文件中声明（在构建的某些子集之间共享它们）或在主构建配置文件中（使它们是全局的）。
​		
​		构建参数(build arguments)如何设置：
首先，系统默认参数是基于当前系统设置的，内置的参数有：

host_cpu

 host_os

current_cpu

current_os

target_cpu

target_os

接下来，应用特定项目的重写值。这些可以在 .gn文件中的 default_args中设定。查看帮助 "gn help dotfile"

如果指定，则使用来自 -- args 命令行的参数。如果没有指定那个标志，那么来自于之前build文件中的参数就会被使用。(在build目录下的 args.gn 文件)。也就是如果没有 --args 传递参数，就使用 args.gn文件中的参数。

最后，对于使用非默认工具链编译的目标，则使用工具链中的重写值。这些是在toolchain定义的时候由

<toolchain_args>设置。





如果你要重写的值在 declare_args中没有出现过，就会有非致命性的错误出现。

例子：

  gn args out/FooBar
      Create the directory out/FooBar and open an editor. You would type
      something like this into that file:
          enable_doom_melon=false
          os="android"

  gn gen out/FooBar --args="enable_doom_melon=true os=\"android\""
      This will overwrite the build directory with the given arguments. (Note
      that the quotes inside the args command will usually need to be escaped
      for your shell to pass through strings values.)
      
构建参数(build arguments)如何使用：

如果你想使用一个参数，你就用 declare_args() 指定默认值。如果在 上面"构建参数(build arguments)如何设置"一节中列出的步骤都不适用于给定的参数，则这些默认值将应用，但是这些默认值不会重写它们中的任何一个值。

通常，根构建配置(the root build config)会定义全局参数，这些参数会被传递给所有的build文件(BUILD.gn)。单独的构建文件也可以指定仅适用于这些文件的参数。在使用 "import" 导入的文件中定义参数是很有用的，如果你想让这些参数应用到多个构建文件上。
```



### 2.4默认目标



​		

```
		您可以为给定的目标类型设置一些默认值。 这通常在构建配置文件中完成，以设置一个默认配置列表，它定义每个目标类型的构建标志和其他设置信息。
​		请参阅gn help set_defaults。
​		例如，当您声明static_library时，将应用静态库的目标默认值。 这些值可以由目标覆盖，修改或保留 ,这个调用通常在构建配置文件中
​		set_defaults("static_library") {
​		  configs = [ "//build:rtti_setup", "//build:extra_warnings" ]
​		}
​		在你目录的build.gn文件中
​		static_library("mylib") 
​		  默认值但可以修改
​		  configs -= "//build:extra_warnings"  
​		  configs += ":mylib_config"  
​		}
​		用于设置目标默认值的其他用例是，当您通过模板定义自己的目标类型并想要指定某些默认值。
```



### 2.5目标

​	

​			

```
	目标是构造表中的一个节点，通常用于表示某个要产生的可执行或库文件。目标经常会依赖其它目标，以下是内置的目标类型（参考gn help ）：
​		action：运行一个脚本来生成一个文件。
​		action_foreach：循环运行脚本依次产生文件。
​		bundle_data：产生要加入Mac/iOS包的数据。
​		create_bundle：产生Mac/iOS包。
​		executable：生成一个可执行文件。
​		group：包含一个或多个目标的虚拟节点（目标）。
​		shared_library：一个.dll或的.so。
​		loadable_module：一个只用于运行时的.dll或.so。
​		source_set：一个轻量的虚拟静态库（通常指向一个真实静态库）。
​		static_library：一个的.lib或某文件（正常情况下你会想要一个source_set代替）。
```



### 	2.6 CONFIGS

​		

​		

```
配置是指定标志集，包含目录和定义的命名对象。 它们可以应用于目标并推送到依赖目标。

​		config("myconfig") {
​		  includes = [ "src/include" ]
​		  defines = [ "ENABLE_DOOM_MELON" ]
​		}

​		将配置应用于目标：
​		executable("doom_melon") {
​		  configs = [ ":myconfig" ]
​		}

​		build config文件通常指定设置默认配置列表的目标默认值。 根据需要，目标可以添加或删除到此列表。
​		所以在实践中,你通常使用configs + =“：myconfig”附加到默认列表。
​		有关如何声明和应用配置的更多信息，

​		目标可以将设置应用于依赖它的目标。 最常见的例子是第三方目标，它需要一些定义或包含目录来使其头文件正确include。 
​		希望这些设置适用于第三方库本身，以及使用该库的所有目标。
​		为此，我们需要为要应用的设置编写config：
​		config("my_external_library_config") {
​		  includes = "."
​		  defines = [ "DISABLE_JANK" ]
​		}
​		然后将这个配置被添加到public_configs。 它将应用于该目标以及直接依赖该目标的目标。

​		shared_library("my_external_library") {
​		  ...

//依赖这个得到配置的目标

​		  public_configs = [ ":my_external_library_config" ]
​		}

​		依赖目标可以通过将你的目标作为“公共”依赖来将该依赖树转发到另一个级别。

​		static_library("intermediate_library") {
​		  ...

//依赖这个从 "my external library"配置得到的目标

​		  public_deps = [ ":my_external_library" ]
​		}

​		目标可以将配置转发到所有依赖项，直到达到链接边界，将其设置为all_dependent_config。 但建议不要这样做，因为它可以喷涂标志和定义超过必要的更多的构建。 
​		相反，使用public_deps控制哪些标志适用于哪里。在Chrome中，更喜欢使用构建标志头系统（build/buildflag_header.gni）来防止编译器定义导致的大多数编译错误。
```



### 	2.7工具链

​	

```
	Toolchains 是一组构建命令来运行不同类型的输入文件和链接的任务。
​		可以设置有多个 Toolchains 的 build。 不过最简单的方法是每个 toolchains 分开 build同时在他们之间加上依赖关系。 
​		这意味着,例如,32 位 Windows 建立可能取决于一个 64 位助手的 target。 他们每个可以依赖“//base:base”将 32 位基础背景下的 32 位工具链,和 64 位背景下的 64 位工具链。
​		当 target 指定依赖于另一个 target,当前的 toolchains 是继承的,除非它是明确覆盖

​		当你有一个简单的版本只有一个 toolchain，build config 文件是在构建之初只加载一次。它必须调用 set_default_toolchain 告诉 GN toolchain 定义的 label 标签。 此 toolchain 定义了需要用的编译器和连接器的命令。 
​		toolchain 定义的 toolchain_args 被忽略。当 target 对使用不同的 toolchain target 的依赖， GN 将使用辅助工具链来解决目标开始构建。 
​		GN 将加载与工具链定义中指定的参数生成配置文件。 由于工具链已经知道， 调用 set_default_toolchain 将被忽略。所以 oolchain configuration 结构是双向的。
​		在默认的 toolchain（即主要的构建 target）的配置从构建配置文件的工具链流向： 构建配置文件着眼于构建（操作系统类型， CPU 架构等）的状态， 并决定使用哪些 toolchain（通过 set_default_toolchin）。
​		在二次 toolchain，配置从 toolchain 流向构建配置文件：在 toolchain 定义 toolchain_args 指定的参数重新调用构建。

​		eg:
​		假设默认的构建是一个 64 位版本。 无论这是根据当前系统默认的 CPU 架构， 或者用户在命令行上传递 target_cpu=“64”。
​		build config file 应该像这样设置默认的工具链：

//设置默认工具链，只有在上下文中运行时才有效

​		默认工具链，根据当前cpu选择正确的cpu
​		if (target_cpu == "x64") {
​		  set_default_toolchain("//toolchains:64")
​		} else if (target_cpu == "x86") {
​		  set_default_toolchain("//toolchains:32")
​		}

​		如果一个 64 位的 target 要依靠一个 32 位二进制数， 它会使用 data_deps 指定的依赖关系
​		（ data_deps依赖库在运行时才需要链接时不需要， 因为你不能直接链接 32 位和 64位的库）。

​		executable("my_program") {
​		  ...
​		  if (target_cpu == "x64") {

//The 64-bit build needs this 32-bit helper.

​			data_deps = [ ":helper(//toolchains:32)" ]
​		  }
​		}

​		if (target_cpu == "x86") {

//Our helper library is only compiled in 32-bits.

​		  shared_library("helper") {
​			...
​		  }
​		}
​		上述（引用的工具链文件toolchains/BUILD.gn）将定义两个工具链：

​		toolchain("32") {
​		  tool("cc") {
​			...
​		  }
​		  ... more tools ...

//Arguments to the build when re-invoking as a secondary toolchain.

​		  toolchain_args = {
​			current_cpu = "x86"
​		  }
​		}

​		toolchain("64") {
​		  tool("cc") {
​			...
​		  }
​		  ... more tools ...

//Arguments to the build when re-invoking as a secondary toolchain.

​		  toolchain_args = {
​			current_cpu = "x64"
​		  }

​		工具链args明确指定CPU体系结构，因此如果目标依赖于使用该工具链的东西，那么在重新调用该生成时，将设置该cpu体系结构。 
​		这些参数被忽略为默认工具链，因为当他们知道的时候，构建配置已经运行。 通常，工具链args和用于设置默认工具链的条件应该一致。
​		有关多版本设置的好处是， 你可以写你的目标条件语句来引用当前 toolchain 的状态。构建文件将根据每个 toolchain 不同的状态重新运行。
​		对于上面的例子 my_program， 你可以看到它查询 CPU 架构， 加入了只依赖该程序的 64 位版本。 32 位版本便不会得到这种依赖性。

​		工具链均使用 toolchain 的命令声明， 它的命令用于每个编译和链接操作。 该toolchain 在执行时还指定一组参数传递到 build config 文件。 
​		这使您可以配置信息传递给备用 toolchain。
```



### 		2.8.template

​		

```
模板是 GN 重复使用代码的主要方式。 通常， 模板会扩展到一个或多个其他目标类型。

​		声明一个将IDL文件编译为源文件的脚本，然后编译这些文件
​		源文件。
​		template("idl") 

​		总是基于目标名的辅助目标，这样它们是唯一的。
​		目标名称#将是调用模板时作为名称传递的字符串。
​		  idl_target_name = "${target_name}_generate"
​		  action_foreach(idl_target_name) {
​			...
​		  }
​		 你的模板应该总是定义一个名为target name的目标。#当其他目标依赖于你的模板调用时，这将是该依赖的目地
​		  source_set(target_name) {
​			...
​			deps = [ ":$idl_target_name" ]  # Require the sources to be compiled.
​		  }
​		}

​		通常情况下你的模板定义在一个.gni 文件中， 用户 import 该文件看到模板的定义：

​		import("//tools/idl_compiler.gni")

​		idl("my_interfaces") {
​		  sources = [ "a.idl", "b.idl" ]
​		}

​		声明模板会在当时在范围内的变量周围创建一个闭包。 当调用模板时，魔术变量调用器用于从调用作用域读取变量。 模板通常将其感兴趣的值复制到其自己的范围中：

​		template("idl") {
​		  source_set(target_name) {
​			sources = invoker.sources
​		  }
​		}

​		模板执行时的当前目录将是调用构建文件的目录，而不是模板源文件。 这是从模板调用器传递的文件将是正确的（这通常说明大多数文件处理模板）。 但是，如果模板本身有文件（也许它生成一个运行脚本的动作），你将需要使用绝对路径（“// foo / …”）来引用这些文件， 当前目录在调用期间将不可预测。 有关更多信息和更完整的示例，请参阅gn帮助模板。
```



### 	2.9其他功能

#### 		1 Imports

​	

```
		您可以 import 导入 .gni 文件到当前文件中。 这不是 C++中的 include。 
​			Import 的文件将独立执行并将执行的结果复制到当前文件中（C ++执行的时候， 当遇到 include 指令时才会在当前环境中 include 文件）。
​			Import 允许导入的结果被缓存， 并且还防止了一些“creative”的用途包括像嵌套 include 文件。
​			通常一个.gni 文件将定义 build 的参数和模板。
​			.gni 文件可以定义像_this 名字前使用一个下划线的临时变量， 从而它不会被传出文件外。
```



#### 		2 路径处理

​			

```
通常你想使一个文件名或文件列表名相对于不同的目录。 这在运行 scripts 时特别常见的， 当构建输出目录为当前目录执行的时候， 
​			构建文件通常是指相对于包含他们的目录的文件。您可以使用 rebase_path 转化目录。
​			rebase_path: 将文件或目录改为另一个位置。

​			converted = rebase_path(input,
​					  new_base = "",
​					  current_base = ".")

​			input
​			表示文件或目录名称的字符串或字符串列表。这些可以是相对路径("foo/bar.txt")，系统绝对路径("/foo/bar.txt")或源绝对路径("//foo/bar.txt")。

​			new_base
​			要转换要相对于的路径的目录。这可以是一个绝对路径或相对路径(将被视为相对路径)到当前build文件的目录)。
​			作为一种特殊情况，如果new_base是空字符串(默认值)，则all路径将被转换为系统本地风格的绝对路径路径分隔符。
​			这对于调用外部程序很有用。

​			current_base
​			表示输入中相对路径的基的目录。如果这不是绝对路径，它将被视为相对于当前的构建文件。使用“.”(默认值)
​			将路径从当前构建文件的目录。

​			Return value
​			返回值的类型将与输入值相同(可以是字符串或字符串列表)。所有的相对和源-绝对文件名系统绝对路径将被转换为相对于请求的输出
​			是不变的。输出路径是否以斜杠结尾将匹配对应的输入路径以斜杠结尾。它将返回"。"或"。/"(取决于输入是否以斜杠结尾)以避免返回空
​			字符串。这意味着如果您希望根路径("//"或"/")不是以斜杠，你可以添加一个点("//.")。
```



#### 		3 模式



```
			Patterns 被用来在一个部分表示一个或多个标签。

​			命令： gn help set_sources_assignment_filter 
​				   gn help label_pattern 查看更多信息。
```



#### 		4 执行脚本

​		

```
	有两种方式来执行脚本。GN中的所有外部脚本都在Python中。第一种方式是构建步骤。这样的脚本将需要一些输入并生成一些输出作为构建的一部分。
​			调用脚本的目标使用“action”目标类型声明（请参阅gn help action）。
​			
​			在构建文件执行期间，执行脚本的第二种方式是同步的。
​			在某些情况下，这是必要的，以确定要编译的文件集，或者获取构建文件可能依赖的某些系统配置。构建文件可以读取脚本的stdout并以不同的方式对其执行操作。
​			同步脚本执行由exec_script函数完成，因为同步执行脚本需要暂停当前的buildfile执行，直到Python进程完成执行，所以依赖外部脚本很慢，应该最小化。
​			为了防止滥用，允许调用exec_script的文件可以在toplevel .gn文件中列入白名单。 
​			Chrome会执行此操作，需要对此类添加进行其他代码审核。请参阅gn help dotfile。

​				
```




## 3函数调用列子

```
print("hello, world")
​	assert(is_win, "This should only be executed on Windows") # 如果is_win为真,就打印后面的内容
​	static_library("mylibrary") { 
​	  sources = [ "a.cc" ]
​	}
​	解读:

​	print,assert,static_library都是库函数,或者叫内置函数.
​	static_library的调用有点奇怪,它是个函数,函数内部会使用sources这个内置变量
​	sources = [ "a.cc" ]相当于先传参给这个函数的内置变量,并调用这个函数.学习gn得习惯这种语法方式,被大量的使用.

​	模板 | Templates

​	gn提供了很多内置函数,想自己定义函数怎么办? 答案是模板,模板就是自定义函数.

​	#定义模板, 文件路径: //tools/idl_compiler.gni, 后缀.gni 代表这是一个 gn import file
​	template("idl") { #自定义一个名称为 "idl"的函数
​	source_set(target_name) { #调用内置函数 source_set
​	sources = invoker.sources #invoker为内置变量,含义为调用者内容 即:[ "a", "b" ]的内容
​	}
​	}
​	#如何使用模板, 用import,类似 C语言的 #include
​	
​	import("//tools/idl_compiler.gni")
​	idl("my_interfaces") { #等同于调用 idl
​	  sources = [ "a", "b" ] #给idl传参, 参数的接收方是 invoker.sources
​	}
​	
​	明白了模板的使用,阅读鸿蒙gn代码就不会有太大的障碍.

Targets中的一个节点。它通常表示将生成某种可执行文件或库文件。整个构建是由一个个的目标组成.
	目标包含:

​	action: 运行脚本以生成文件
​	executable: 生成可执行文件
​	group: 生成依赖关系组
​	shared_library: 生成.dll或.so动态链接库
​	static_library: 生成.lib或.a 静态链接库
​	...
​	配置项 | Configs

​	记录完成目标项所需的配置信息,例如:

​	config("myconfig") {#创建一个标签为`myconfig`的配置项
​		include_dirs = [ "include/common" ]
​		defines = [ "ENABLE_DOOM_MELON" ]
​	  }

​	executable("mything") {#生成可执行文件
​	  configs = [ ":myconfig" ]#使用标签为`myconfig`的配置项来生成目标文件
​	}
```


​		
## 	gn在鸿蒙中的使用

```
	有了以上基础铺垫,正式开始gn在openharomny中的使用.

​	从哪开始
​	在构建工具篇中已经说清楚了 hb的python部分有个工作任务是生成gn命令所需参数. gn生成ninja的命令是 gn gen ...

​	  /home/tools/gn gen /home/openharmony/code-v1.1.1-LTS/out/hispark_aries/ipcamera_hispark_aries \
​	  --root=/home/openharmony/code-v1.1.1-LTS \
​	  --dotfile=/home/openharmony/code-v1.1.1-LTS/build/lite/.gn \
​	  --script-executable=python3 \
​	  '--args=ohos_build_type="debug" \
​			  ohos_build_compiler_specified="clang" \
​			  ohos_build_compiler_dir="/home/tools/llvm" \
​			  product_path="/home/openharmony/code-v1.1.1-LTS/vendor/hisilicon/hispark_aries" \
​			  device_path="/home/openharmony/code-v1.1.1-LTS/device/hisilicon/hispark_aries/sdk_liteos" \
​			  ohos_kernel_type="liteos_a" \
​			  enable_ohos_appexecfwk_feature_ability = false \
​			  ohos_full_compile=true'
​	解读

​	root,dotfile,script-executable是gn内置的固定参数,一切从dotfile指向的文件开始.即//.gn 相当于main()函数的作用,
​	打开 .gn看下内容,只有两句,先配置好内部参数再工作.
​	

//The location of the build configuration file. #1.完成gn的配置工作

​	buildconfig = "//build/lite/config/BUILDCONFIG.gn" 

//The source root location. #2.完成gn的编译工作

​	root = "//build/lite" 

​		args为用户自定义的参数,它们将会在解析BUILD.gn,BUILDCONFIG.gn过程中被使用.
​		BUILDCONFIG.gn | 构建配置项
​		BUILDCONFIG.gn为BUILD.gn做准备,填充好编译所需的配置信息.即生成配置项
​		详细请查看 build/lite/config/BUILDCONFIG.gn 文件全部内容,本篇只贴出部分.
```

		import("//build/lite/ohos_var.gni")
		import("${device_path}/config.gni")
		....
		arch = "arm"
		if (ohos_kernel_type == "liteos_a") {
		  target_triple = "$arch-liteos"
		} else if (ohos_kernel_type == "linux") {
		  target_triple = "$arch-linux-ohosmusl"
		}
		...
		template("executable") { #生成可执行文件
		  executable(target_name) {
			forward_variables_from(invoker, Variables_Executable)
			if (!defined(invoker.deps)) {
			  deps = [ "//build/lite:prebuilts" ]
			} else {
			  deps += [ "//build/lite:prebuilts" ]
			}
			if (defined(invoker.configs)) {
			  configs = []
			  configs += invoker.configs
			}
		  }
		}
		set_defaults("executable") {#设置目标类型的默认值
		  configs = default_executable_configs
		  configs += [ "//build/lite/config:board_exe_ld_flags" ]
		}
		...
		解读
	
		${device_path}为命令行参数,本篇为home/openharmony/v3/device/hisilicon/hispark_aries/sdk_liteos
		查看构建-配置内容,BUILDCONFIG主要任务就是对其中的内置变量赋值.例如:
		指定编译器 clang,
		如何生成可执行文件的方法等
		BUILD.gn | 启动构建
		查看 build/lite/BUILD.gn 文件,文件注解较多.


​		
​		

		#目的是要得到项目各个模块的编译入口
		group("ohos") {
		  deps = []
			if (ohos_build_target == "") {
			# Step 1: Read product configuration profile.
			# 第一步:读取配置文件product_path的值来源于根目录的ohos_config.json,如下,内容由 hb set 命令生成
			# {
			#  "root_path": "/home/openharmony",
			#  "board": "hispark_aries",
			#  "kernel": "liteos_a",
			#  "product": "ipcamera_hispark_aries",
			#  "product_path": "/home/openharmony/vendor/hisilicon/hispark_aries",
			#  "device_path": "/home/openharmony/device/hisilicon/hispark_aries/sdk_liteos",
			#  "patch_cache": null
			#}
			product_cfg = read_file("${product_path}/config.json", "json")
	
			# Step 2: Loop subsystems configured by product.
			# 第二步:循环处理各自子系统,config.json中子系统部分格式如下hb
			#"subsystems": [
			#  {
			#    "subsystem": "aafwk",
			#    "components": [
			#      { "component": "ability", "features":[ "enable_ohos_appexecfwk_feature_ability = false" ] }
			#    ]
			#  },
			#  ...
			#  {
			#    "subsystem": "distributed_schedule",
			#    "components": [
			#  	{ "component": "system_ability_manager", "features":[] },
			#  	{ "component": "foundation", "features":[] },
			#  	{ "component": "distributed_schedule", "features":[] }
			#    ]
			#  },
			#  {
			#      "subsystem": "kernel",
			#      "components": [
			#        { "component": "liteos_a", "features":[] }
			#      ]
			#   },
			#]
			foreach(product_configed_subsystem, product_cfg.subsystems) {#对子系统数组遍历操作
			  subsystem_name = product_configed_subsystem.subsystem	#读取一个子系统 aafwk,hiviewdfx,security ==
			  subsystem_info = {
			  }
	
			  # Step 3: Read OS subsystems profile.
				# 第三步: 读取各个子系统的配置文件
			  subsystem_info =
				  read_file("//build/lite/components/${subsystem_name}.json", "json")
	
			  # Step 4: Loop components configured by product.
			  # 第四步: 循环读取子系统内各控件的配置信息
			  # 此处以内核为例://build/lite/components/kernel.json"
			  # "components": [
			  #   {
			  #     "component": "liteos_a",              # 组件名称
			  #     "description": "liteos-a kernel",     # 组件一句话功能描述
			  #     "optional": "false",                  # 组件是否为最小系统必选
			  #     "dirs": [                             # 组件源码路径
			  #       "kernel/liteos_a"
			  #     ],
			  #     "targets": [                          # 组件编译入口
			  #       "//kernel/liteos_a:kernel"
			  #     ],
			  #     "rom": "1.98MB",                      # 组件ROM值
			  #     "ram": "",                            # 组件RAM估值
			  #     "output": [                           # 组件编译输出
			  #       "liteos.bin"
			  #     ],
			  #     "adapted_board": [                    # 组件已适配的主板
			  #       "hispark_aries",
			  #       "hispark_taurus",
			  #       "hi3518ev300",
			  # 	  "hi3516dv300",
			  #     ],
			  #     "adapted_kernel": [ "liteos_a" ],     # 组件已适配的内核
			  #     "features": [],                       # 组件可配置的特性
			  #     "deps": {
			  #       "components": [],                   # 组件依赖的其他组件
			  #       "third_party": [                    # 组件依赖的三方开源软件
			  #         "FreeBSD",
			  #         "musl",
			  #         "zlib",
			  #         "FatFs",
			  #         "Linux_Kernel",
			  #         "lwip",
			  #         "NuttX",
			  #         "mtd-utils"
			  #       ]
			  #     }
			  #   },
			  # ]
			  foreach(product_configed_component,
					  product_configed_subsystem.components) { #遍历项目控件数组
				# Step 5: Check whether the component configured by product is exist.
					# 第五步: 检查控件配置信息是否存在
				component_found = false #初始为不存在
				foreach(system_component, subsystem_info.components) {#项目控件和子系统中的控件遍历对比
				  if (product_configed_component.component ==
					  system_component.component) { #找到了liteos_a
					component_found = true
				  }
				}
					#如果没找到的信息,则打印项目控件查找失败日志
				assert(
					component_found,
					"Component \"${product_configed_component.component}\" not found" +
						", please check your product configuration.")
				
				# Step 6: Loop OS components and check validity of product configuration.
				# 第六步: 检查子系统控件的有效性并遍历控件组,处理各个控件
				foreach(component, subsystem_info.components) {
				  kernel_valid = false	#检查内核
				  board_valid = false	#检查开发板
	
				  # Step 6.1: Skip component which not configured by product.
				  if (component.component == product_configed_component.component) {
					# Step 6.1.1: Loop OS components adapted kernel type.
					foreach(component_adapted_kernel, component.adapted_kernel) {
					  if (component_adapted_kernel == product_cfg.kernel_type && 
						  kernel_valid == false) { #内核检测是否已适配
						kernel_valid = true
					  }
					}
					# 如果内核未适配,则打印未适配日志
					assert(
						kernel_valid,
						"Invalid component configed, ${subsystem_name}:${product_configed_component.component} " + "not available for kernel: ${product_cfg.kernel_type}!")
	
					# Step 6.1.2: Add valid component for compiling.
					# 添加有效组件进行编译
					foreach(component_target, component.targets) {//遍历组件的编译入口
					  deps += [ component_target ] #添加到编译列表中
					}
				  }
				}
			  }
			}
	
			# Step 7: Add device and product target by default.
			# 第七步: 添加设备和项目的编译单元
			# "product_path": "/home/openharmony/vendor/hisilicon/hispark_aries",
			# "device_path": "/home/openharmony/device/hisilicon/hispark_aries/sdk_liteos",
			  deps += [
			  "${device_path}/../", #添加 //device/hisilicon/hispark_aries 进入编译项
			  "${product_path}"		#添加 //vendor/hisilicon/hispark_aries 进入编译项
			]
		  } else {#编译指定的组件,例如 hb build -T targetA&&targetB
			deps += string_split(ohos_build_target, "&&")
		  }
		}
		解读
	
		有三个概念贯彻整个鸿蒙系统,子系统(subsystems),组件(components),功能(features).理解它们的定位和特点是解读鸿蒙的关键所在.
		先找到product_path下的 配置文件 config.json,里面配置了项目所要使用的子系统和组件.
		再遍历项目所使用的组件是否能再 //build/lite/components/*.json组件集中能找到.
		将找到的组件targets加入到编译列表deps中.targets指向了要编译的组件目录.例如内核组件时指向了://kernel/liteos_a:kernel,
		import("//build/lite/config/component/lite_component.gni") #组件模板函数
		import("//build/lite/config/subsystem/lite_subsystem.gni") #子系统模板函数
		lite_subsystem("kernel") {#编译内核子系统/组件入口
		  subsystem_components = []
	
		  if (enable_ohos_kernel_liteos_a_ext_build == false) {
			subsystem_components += [
			  "//kernel/liteos_a/kernel",
			  "//kernel/liteos_a/net",
			  "//kernel/liteos_a/lib",
			  "//kernel/liteos_a/compat",
			  "//kernel/liteos_a/fs",
			  "//kernel/liteos_a/arch:platform_cpu",
			]
			if (LOSCFG_SHELL) {
			  subsystem_components += [ "//kernel/liteos_a/shell" ]
			}
		  } else {
			deps = [ ":make" ]
		  }
		} 
		lite_subsystem是个模板函数(自定义函数),再查看lite_subsystem.gni函数原型,它的目的只有一个填充 deps,deps是私有链接依赖关系,最终会形成一颗依赖树.gn会根据这些依赖关系生成最终的.ninja文件.
		# 定义一个子系统
		# lite_subsystem template模板定义了子系统中包含的所有模块
		# 参数
		#   subsystem_components (必须))
		#     [范围列表] 定义子系统的所有模块.
		template("lite_subsystem") {
			assert(defined(invoker.subsystem_components), "subsystem_components in required.")
	
			lite_subsystem_components = invoker.subsystem_components
	
			group(target_name) {
				deps = []
				if(defined(invoker.deps)) {
					deps += invoker.deps
				}
				# add subsystem packages
				foreach(pkg_label, lite_subsystem_components) {
					deps += [ pkg_label ]
				}
			}
		}
		生成了哪些文件
		执行后gn gen会生成如下文件和目录
	
		turing@ubuntu:/home/openharmony/code-v1.1.1-LTS/out/hispark_aries/ipcamera_hispark_aries$ ls
		args.gn  build.ninja  build.ninja.d  NOTICE_FILE  obj  test_info  toolchain.ninja
		build.ninja.d中记录依赖的 BUILD.gn文件路径
	
		build.ninja: ../../../base/global/resmgr_lite/frameworks/resmgr_lite/BUILD.gn \
					 ../../../base/hiviewdfx/hilog_lite/frameworks/featured/BUILD.gn \
					 ../../../base/hiviewdfx/hilog_lite/services/apphilogcat/BUILD.gn \
					 ....
		gn根据这些组件的BUILD.gn在obj目录下对应生成了每个组件的.ninja文件.此处列出鸿蒙L1所有的 .ninja文件, 
		具体ninja是如何编译成最终的库和可执行文件的,将在后续篇中详细介绍其语法和应用.





		turing@ubuntu:/home/openharmony/code-v1.1.1-LTS/out/hispark_aries/ipcamera_hispark_aries/obj$ tree
		├── base
		│   ├── global
		│   │   └── resmgr_lite
		│   │       └── frameworks
		│   │           └── resmgr_lite
		│   │               └── global_resmgr.ninja
		│   ├── hiviewdfx
		│   │   └── hilog_lite
		│   │       ├── frameworks
		│   │       │   └── featured
		│   │       │       ├── hilog_shared.ninja
		│   │       │       └── hilog_static.ninja
		│   │       └── services
		│   │           ├── apphilogcat
		│   │           │   ├── apphilogcat.ninja
		│   │           │   └── apphilogcat_static.ninja
		│   │           └── hilogcat
		│   │               ├── hilogcat.ninja
		│   │               └── hilogcat_static.ninja
		│   ├── security
		│   │   ├── appverify
		│   │   │   └── interfaces
		│   │   │       └── innerkits
		│   │   │           └── appverify_lite
		│   │   │               ├── products
		│   │   │               │   └── ipcamera
		│   │   │               │       └── verify_base.ninja
		│   │   │               ├── unittest
		│   │   │               │   └── app_verify_test.ninja
		│   │   │               └── verify.ninja
		│   │   ├── deviceauth
		│   │   │   └── frameworks
		│   │   │       └── deviceauth_lite
		│   │   │           └── source
		│   │   │               └── hichainsdk.ninja
		│   │   ├── huks
		│   │   │   └── frameworks
		│   │   │       └── huks_lite
		│   │   │           └── source
		│   │   │               └── huks.ninja
		│   │   └── permission
		│   │       └── services
		│   │           └── permission_lite
		│   │               ├── ipc_auth
		│   │               │   └── ipc_auth_target.ninja
		│   │               ├── pms
		│   │               │   └── pms_target.ninja
		│   │               ├── pms_base
		│   │               │   └── pms_base.ninja
		│   │               └── pms_client
		│   │                   └── pms_client.ninja
		│   └── startup
		│       ├── appspawn_lite
		│       │   └── services
		│       │       ├── appspawn.ninja
		│       │       └── test
		│       │           └── unittest
		│       │               └── common
		│       │                   └── appspawn_test.ninja
		│       ├── bootstrap_lite
		│       │   └── services
		│       │       └── source
		│       │           └── bootstrap.ninja
		│       ├── init_lite
		│       │   └── services
		│       │       ├── init.ninja
		│       │       └── test
		│       │           └── unittest
		│       │               └── common
		│       │                   └── init_test.ninja
		│       └── syspara_lite
		│           └── frameworks
		│               ├── parameter
		│               │   └── src
		│               │       └── sysparam.ninja
		│               ├── token
		│               │   └── token_shared.ninja
		│               └── unittest
		│                   └── parameter
		│                       └── ParameterTest.ninja
		├── build
		│   └── lite
		│       └── config
		│           └── component
		│               ├── cJSON
		│               │   ├── cjson_shared.ninja
		│               │   └── cjson_static.ninja
		│               ├── openssl
		│               │   ├── openssl_shared.ninja
		│               │   └── openssl_static.ninja
		│               └── zlib
		│                   ├── zlib_shared.ninja
		│                   └── zlib_static.ninja
		├── drivers
		│   ├── adapter
		│   │   └── uhdf
		│   │       ├── manager
		│   │       │   └── hdf_core.ninja
		│   │       ├── platform
		│   │       │   └── hdf_platform.ninja
		│   │       ├── posix
		│   │       │   └── hdf_posix_osal.ninja
		│   │       └── test
		│   │           └── unittest
		│   │               ├── common
		│   │               │   └── hdf_test_common.ninja
		│   │               ├── config
		│   │               │   └── hdf_adapter_uhdf_test_config.ninja
		│   │               ├── manager
		│   │               │   ├── hdf_adapter_uhdf_test_door.ninja
		│   │               │   ├── hdf_adapter_uhdf_test_ioservice.ninja
		│   │               │   ├── hdf_adapter_uhdf_test_manager.ninja
		│   │               │   └── hdf_adapter_uhdf_test_sbuf.ninja
		│   │               ├── osal
		│   │               │   └── hdf_adapter_uhdf_test_osal.ninja
		│   │               └── platform
		│   │                   └── hdf_adapter_uhdf_test_platform.ninja
		│   └── peripheral
		│       ├── input
		│       │   └── hal
		│       │       └── hdi_input.ninja
		│       └── wlan
		│           ├── client
		│           │   └── wifi_driver_client.ninja
		│           ├── hal
		│           │   └── wifi_hal.ninja
		│           └── test
		│               ├── performance
		│               │   └── hdf_peripheral_wlan_test_performance.ninja
		│               └── unittest
		│                   └── hdf_peripheral_wlan_test.ninja
		├── foundation
		│   ├── aafwk
		│   │   └── aafwk_lite
		│   │       ├── frameworks
		│   │       │   ├── ability_lite
		│   │       │   │   └── ability.ninja
		│   │       │   ├── abilitymgr_lite
		│   │       │   │   └── abilitymanager.ninja
		│   │       │   └── want_lite
		│   │       │       └── want.ninja
		│   │       └── services
		│   │           └── abilitymgr_lite
		│   │               ├── abilityms.ninja
		│   │               ├── tools
		│   │               │   └── aa.ninja
		│   │               └── unittest
		│   │                   └── test_lv0
		│   │                       └── page_ability_test
		│   │                           └── ability_test_pageAbilityTest_lv0.ninja
		│   ├── ai
		│   │   └── engine
		│   │       ├── services
		│   │       │   ├── client
		│   │       │   │   ├── ai_client.ninja
		│   │       │   │   ├── client_executor
		│   │       │   │   │   └── client_executor.ninja
		│   │       │   │   └── communication_adapter
		│   │       │   │       └── ai_communication_adapter.ninja
		│   │       │   ├── common
		│   │       │   │   ├── platform
		│   │       │   │   │   ├── dl_operation
		│   │       │   │   │   │   └── dlOperation.ninja
		│   │       │   │   │   ├── event
		│   │       │   │   │   │   └── event.ninja
		│   │       │   │   │   ├── lock
		│   │       │   │   │   │   └── lock.ninja
		│   │       │   │   │   ├── os_wrapper
		│   │       │   │   │   │   └── ipc
		│   │       │   │   │   │       └── aie_ipc.ninja
		│   │       │   │   │   ├── semaphore
		│   │       │   │   │   │   └── semaphore.ninja
		│   │       │   │   │   ├── threadpool
		│   │       │   │   │   │   └── threadpool.ninja
		│   │       │   │   │   └── time
		│   │       │   │   │       └── time.ninja
		│   │       │   │   ├── protocol
		│   │       │   │   │   └── data_channel
		│   │       │   │   │       └── data_channel.ninja
		│   │       │   │   └── utils
		│   │       │   │       └── encdec
		│   │       │   │           └── encdec.ninja
		│   │       │   └── server
		│   │       │       ├── ai_server.ninja
		│   │       │       ├── communication_adapter
		│   │       │       │   └── ai_communication_adapter.ninja
		│   │       │       ├── plugin_manager
		│   │       │       │   └── plugin_manager.ninja
		│   │       │       └── server_executor
		│   │       │           └── server_executor.ninja
		│   │       └── test
		│   │           ├── common
		│   │           │   ├── ai_test_common.ninja
		│   │           │   └── dl_operation
		│   │           │       └── dl_operation_so
		│   │           │           └── dlOperationSo.ninja
		│   │           ├── function
		│   │           │   ├── ai_test_function.ninja
		│   │           │   └── death_callback
		│   │           │       ├── testDeathCallbackLibrary.ninja
		│   │           │       └── testDeathCallback.ninja
		│   │           ├── performance
		│   │           │   └── ai_test_performance_unittest.ninja
		│   │           └── sample
		│   │               ├── asyncDemoPluginCode.ninja
		│   │               ├── sample_plugin_1.ninja
		│   │               ├── sample_plugin_2.ninja
		│   │               └── syncDemoPluginCode.ninja
		│   ├── appexecfwk
		│   │   └── appexecfwk_lite
		│   │       ├── frameworks
		│   │       │   └── bundle_lite
		│   │       │       └── bundle.ninja
		│   │       └── services
		│   │           └── bundlemgr_lite
		│   │               ├── bundle_daemon
		│   │               │   └── bundle_daemon.ninja
		│   │               ├── bundlems.ninja
		│   │               └── tools
		│   │                   └── bm.ninja
		│   ├── communication
		│   │   ├── ipc_lite
		│   │   │   └── liteipc_adapter.ninja
		│   │   └── softbus_lite
		│   │       └── softbus_lite.ninja
		│   ├── distributedschedule
		│   │   ├── dmsfwk_lite
		│   │   │   ├── dmslite.ninja
		│   │   │   └── moduletest
		│   │   │       └── dtbschedmgr_lite
		│   │   │           └── distributed_schedule_test_dms.ninja
		│   │   ├── safwk_lite
		│   │   │   └── foundation.ninja
		│   │   └── samgr_lite
		│   │       ├── communication
		│   │       │   └── broadcast
		│   │       │       └── broadcast.ninja
		│   │       ├── samgr
		│   │       │   ├── adapter
		│   │       │   │   └── samgr_adapter.ninja
		│   │       │   ├── samgr.ninja
		│   │       │   └── source
		│   │       │       └── samgr_source.ninja
		│   │       ├── samgr_client
		│   │       │   └── client.ninja
		│   │       ├── samgr_endpoint
		│   │       │   ├── endpoint_source.ninja
		│   │       │   └── store_source.ninja
		│   │       └── samgr_server
		│   │           └── server.ninja
		│   ├── graphic
		│   │   ├── surface
		│   │   │   ├── surface.ninja
		│   │   │   └── test
		│   │   │       └── lite_surface_unittest.ninja
		│   │   ├── ui
		│   │   │   └── ui.ninja
		│   │   ├── utils
		│   │   │   ├── graphic_hals.ninja
		│   │   │   ├── graphic_utils.ninja
		│   │   │   └── test
		│   │   │       ├── graphic_test_color.ninja
		│   │   │       ├── graphic_test_container.ninja
		│   │   │       ├── graphic_test_geometry2d.ninja
		│   │   │       ├── graphic_test_math.ninja
		│   │   │       └── graphic_test_style.ninja
		│   │   └── wms
		│   │       ├── wms_client.ninja
		│   │       └── wms_server.ninja
		│   └── multimedia
		│       ├── audio_lite
		│       │   └── frameworks
		│       │       └── audio_capturer_lite.ninja
		│       ├── camera_lite
		│       │   └── frameworks
		│       │       └── camera_lite.ninja
		│       ├── media_lite
		│       │   ├── frameworks
		│       │   │   ├── player_lite
		│       │   │   │   └── player_lite.ninja
		│       │   │   └── recorder_lite
		│       │   │       └── recorder_lite.ninja
		│       │   ├── interfaces
		│       │   │   └── kits
		│       │   │       └── player_lite
		│       │   │           └── js
		│       │   │               └── builtin
		│       │   │                   └── audio_lite_api.ninja
		│       │   └── services
		│       │       └── media_server.ninja
		│       └── utils
		│           └── lite
		│               └── media_common.ninja
		├── test
		│   ├── developertest
		│   │   ├── examples
		│   │   │   └── lite
		│   │   │       └── cxx_demo
		│   │   │           └── test
		│   │   │               └── unittest
		│   │   │                   └── common
		│   │   │                       └── CalcSubTest.ninja
		│   │   └── third_party
		│   │       └── lib
		│   │           └── cpp
		│   │               ├── gtest_main.ninja
		│   │               └── gtest.ninja
		│   └── xts
		│       ├── acts
		│       │   ├── aafwk_lite
		│       │   │   └── ability_posix
		│       │   │       └── module_ActsAbilityMgrTest.ninja
		│       │   ├── ai_lite
		│       │   │   └── ai_engine_posix
		│       │   │       └── base
		│       │   │           ├── module_ActsAiEngineTest.ninja
		│       │   │           └── src
		│       │   │               └── sample
		│       │   │                   ├── asyncDemoPluginCode.ninja
		│       │   │                   ├── sample_plugin_1_sync.ninja
		│       │   │                   ├── sample_plugin_2_async.ninja
		│       │   │                   └── syncDemoPluginCode.ninja
		│       │   ├── appexecfwk_lite
		│       │   │   └── bundle_mgr_posix
		│       │   │       └── module_ActsBundleMgrTest.ninja
		│       │   ├── communication_lite
		│       │   │   ├── lwip_posix
		│       │   │   │   └── module_ActsLwipTest.ninja
		│       │   │   └── softbus_posix
		│       │   │       └── module_ActsSoftBusTest.ninja
		│       │   ├── distributed_schedule_lite
		│       │   │   └── samgr_posix
		│       │   │       └── module_ActsSamgrTest.ninja
		│       │   ├── graphic_lite
		│       │   │   ├── graphic_utils
		│       │   │   │   ├── a
		│       │   │   │   │   └── module_ActsUiInterfaceTest1.ninja
		│       │   │   │   ├── color_posix
		│       │   │   │   │   └── module_ActsColorTest.ninja
		│       │   │   │   ├── geometry2d_posix
		│       │   │   │   │   └── module_ActsGeometyr2dTest.ninja
		│       │   │   │   ├── graphic_math_posix
		│       │   │   │   │   └── module_ActsGraphicMathTest.ninja
		│       │   │   │   ├── heap_base_posix
		│       │   │   │   │   └── module_ActsHeapBaseTest.ninja
		│       │   │   │   ├── list_posix
		│       │   │   │   │   └── module_ActsListTest.ninja
		│       │   │   │   ├── mem_api_posix
		│       │   │   │   │   └── module_ActsGraphMemApiTest.ninja
		│       │   │   │   ├── rect_posix
		│       │   │   │   │   └── module_ActsRectTest.ninja
		│       │   │   │   ├── transform_posix
		│       │   │   │   │   └── module_ActsTransformTest.ninja
		│       │   │   │   └── version_posix
		│       │   │   │       └── module_ActsGraphVersionTest.ninja
		│       │   │   ├── surface
		│       │   │   │   └── surface_posix
		│       │   │   │       └── module_ActsSurfaceTest.ninja
		│       │   │   └── ui
		│       │   │       ├── a
		│       │   │       │   └── module_ActsUiInterfaceTest.ninja
		│       │   │       ├── animator_posix
		│       │   │       │   └── module_ActsAnimatorTest.ninja
		│       │   │       ├── easing_equation_posix
		│       │   │       │   └── module_ActsEasingEquationTest.ninja
		│       │   │       ├── events_posix
		│       │   │       │   └── module_ActsEventsTest.ninja
		│       │   │       ├── flexlayout_posix
		│       │   │       │   └── module_ActsFlexlaoutTest.ninja
		│       │   │       ├── gridlayout_posix
		│       │   │       │   └── module_ActsGridLayoutTest.ninja
		│       │   │       ├── image_posix
		│       │   │       │   └── module_ActsImageTest.ninja
		│       │   │       ├── interpolation_posix
		│       │   │       │   └── module_ActsInterpoliationTest.ninja
		│       │   │       ├── layout_posix
		│       │   │       │   └── module_ActsLayoutTest.ninja
		│       │   │       ├── listlayout_posix
		│       │   │       │   └── module_ActsListlayoutTest.ninja
		│       │   │       ├── screen_posix
		│       │   │       │   └── module_ActsScreenTest.ninja
		│       │   │       ├── style_posix
		│       │   │       │   └── module_ActsStyleTest.ninja
		│       │   │       ├── theme_posix
		│       │   │       │   └── module_ActsThemeTest.ninja
		│       │   │       ├── ui_abstract_progress_posix
		│       │   │       │   └── module_ActsUIAbstractProgressTest.ninja
		│       │   │       ├── ui_analog_clock_posix
		│       │   │       │   └── module_ActsUIAnalogClockTest.ninja
		│       │   │       ├── uianimator_posix
		│       │   │       │   └── module_ActsUIAnimatorTest.ninja
		│       │   │       ├── ui_arc_lable_posix
		│       │   │       │   └── module_ActsUIArcLabelTest.ninja
		│       │   │       ├── ui_axis_posix
		│       │   │       │   └── module_ActsUIAxisTest.ninja
		│       │   │       ├── ui_box_porgress_posix
		│       │   │       │   └── module_ActsUIBoxProgressTest.ninja
		│       │   │       ├── ui_button_posix
		│       │   │       │   └── module_ActsUIButtonTest.ninja
		│       │   │       ├── ui_canvas_posix
		│       │   │       │   └── module_ActsUICanvasTest.ninja
		│       │   │       ├── ui_chart_posix
		│       │   │       │   └── module_ActsUIChartTest.ninja
		│       │   │       ├── ui_checbox_posix
		│       │   │       │   └── module_ActsUICheckboxTest.ninja
		│       │   │       ├── ui_circle_progress_posix
		│       │   │       │   └── module_ActsUICircleProgressTest.ninja
		│       │   │       ├── ui_digital_clock_posix
		│       │   │       │   └── module_ActsUIDigitalClockTest.ninja
		│       │   │       ├── ui_image_animator_posix
		│       │   │       │   └── module_ActsUIImageAnimatorTest.ninja
		│       │   │       ├── ui_image_posix
		│       │   │       │   └── module_ActsUIImageTest.ninja
		│       │   │       ├── ui_label_button_posix
		│       │   │       │   └── module_ActsUILabelButtonTest.ninja
		│       │   │       ├── ui_label_posix
		│       │   │       │   └── module_ActsUILabelTest.ninja
		│       │   │       ├── ui_list_posix
		│       │   │       │   └── module_ActsUIListTest.ninja
		│       │   │       ├── ui_picker_posix
		│       │   │       │   └── module_ActsUIPickerTest.ninja
		│       │   │       ├── ui_radio_button_posix
		│       │   │       │   └── module_ActsUIRadioButtonTest.ninja
		│       │   │       ├── ui_repeat_button_posix
		│       │   │       │   └── module_ActsUIRepeatButtonTest.ninja
		│       │   │       ├── ui_screenshot_posix
		│       │   │       │   └── module_ActsUIScreenshotTest.ninja
		│       │   │       ├── ui_scroll_view_posix
		│       │   │       │   └── module_ActsUIScrollViewTest.ninja
		│       │   │       ├── ui_slider_posix
		│       │   │       │   └── module_ActsUISliderTest.ninja
		│       │   │       ├── ui_surface_view_posix
		│       │   │       │   └── module_ActsUISurfaceViewTest.ninja
		│       │   │       ├── ui_swipe_view_posix
		│       │   │       │   └── module_ActsUISwipeViewTest.ninja
		│       │   │       ├── ui_text_posix
		│       │   │       │   └── module_ActsUITextTest.ninja
		│       │   │       ├── ui_texture_mapper_posix
		│       │   │       │   └── module_ActsUITextureMapperTest.ninja
		│       │   │       ├── ui_time_picker_posix
		│       │   │       │   └── module_ActsUITimePickerTest.ninja
		│       │   │       ├── ui_toggle_button_posix
		│       │   │       │   └── module_ActsUIToggleButtonTest.ninja
		│       │   │       ├── ui_view_group_posix
		│       │   │       │   └── module_ActsUIViewGroupTest.ninja
		│       │   │       └── ui_view_posix
		│       │   │           └── module_ActsUIViewTest.ninja
		│       │   ├── hiviewdfx_lite
		│       │   │   └── hilog_posix
		│       │   │       └── module_ActsHilogTest.ninja
		│       │   ├── kernel_lite
		│       │   │   ├── dyload_posix
		│       │   │   │   └── module_ActsDyloadTest.ninja
		│       │   │   ├── fs_posix
		│       │   │   │   ├── jffs
		│       │   │   │   │   └── module_ActsJFFS2Test.ninja
		│       │   │   │   ├── nfs
		│       │   │   │   │   └── module_ActsNFSTest.ninja
		│       │   │   │   ├── vfat
		│       │   │   │   │   └── module_ActsVFATTest.ninja
		│       │   │   │   └── vfat_storage
		│       │   │   │       └── module_ActsVFATstorageTest.ninja
		│       │   │   ├── futex_posix
		│       │   │   │   └── module_ActsFutexApiTest.ninja
		│       │   │   ├── io_posix
		│       │   │   │   └── module_ActsIoApiTest.ninja
		│       │   │   ├── ipc_posix
		│       │   │   │   ├── message_queue
		│       │   │   │   │   └── module_ActsIpcMqTest.ninja
		│       │   │   │   ├── pipe_fifo
		│       │   │   │   │   └── module_ActsIpcPipeTest.ninja
		│       │   │   │   ├── semaphore
		│       │   │   │   │   └── module_ActsIpcSemTest.ninja
		│       │   │   │   ├── shared_memory
		│       │   │   │   │   └── module_ActsIpcShmTest.ninja
		│       │   │   │   └── signal
		│       │   │   │       └── module_ActsIpcSignalTest.ninja
		│       │   │   ├── math_posix
		│       │   │   │   ├── complexTest.ninja
		│       │   │   │   └── module_ActsMathApiTest.ninja
		│       │   │   ├── mem_posix
		│       │   │   │   └── module_ActsMemApiTest.ninja
		│       │   │   ├── net_posix
		│       │   │   │   └── module_ActsNetTest.ninja
		│       │   │   ├── process_posix
		│       │   │   │   └── module_ActsProcessApiTest.ninja
		│       │   │   ├── sched_posix
		│       │   │   │   └── module_ActsSchedApiTest.ninja
		│       │   │   ├── sys_posix
		│       │   │   │   └── module_ActsSysApiTest.ninja
		│       │   │   ├── time_posix
		│       │   │   │   └── module_ActsTimeApiTest.ninja
		│       │   │   ├── util_posix
		│       │   │   │   └── module_ActsUtilApiTest.ninja
		│       │   │   └── utils
		│       │   │       ├── libfs.ninja
		│       │   │       ├── libmt_utils.ninja
		│       │   │       └── libutils.ninja
		│       │   ├── multimedia_lite
		│       │   │   └── media_lite_posix
		│       │   │       └── recorder_native
		│       │   │           └── module_ActsMediaRecorderTest.ninja
		│       │   ├── security_lite
		│       │   │   ├── datahuks_posix
		│       │   │   │   └── module_ActsSecurityDataTest.ninja
		│       │   │   └── permission_posix
		│       │   │       ├── capability
		│       │   │       │   ├── capability_shared.ninja
		│       │   │       │   ├── jffs
		│       │   │       │   │   └── module_ActsJFFS2CapabilityTest.ninja
		│       │   │       │   └── vfat
		│       │   │       │       └── module_ActsVFATCapabilityTest.ninja
		│       │   │       ├── dac
		│       │   │       │   ├── jffs
		│       │   │       │   │   └── module_ActsJFFS2DACTest.ninja
		│       │   │       │   └── vfat
		│       │   │       │       └── module_ActsVFATDACTest.ninja
		│       │   │       └── pms
		│       │   │           └── module_ActsPMSTest.ninja
		│       │   ├── startup_lite
		│       │   │   ├── bootstrap_posix
		│       │   │   │   └── module_ActsBootstrapTest.ninja
		│       │   │   └── syspara_posix
		│       │   │       └── module_ActsParameterTest.ninja
		│       │   └── utils_lite
		│       │       └── kv_store_posix
		│       │           └── module_ActsKvStoreTest.ninja
		│       └── tools
		│           └── lite
		│               ├── hcpptest
		│               │   ├── gmock_main.ninja
		│               │   ├── gmock.ninja
		│               │   ├── hcpptest_main.ninja
		│               │   └── hcpptest.ninja
		│               └── others
		│                   └── query
		│                       └── query.ninja
		├── third_party
		│   ├── bounds_checking_function
		│   │   ├── libsec_shared.ninja
		│   │   └── libsec_static.ninja
		│   ├── freetype
		│   │   └── freetype.ninja
		│   ├── giflib
		│   │   └── libgif.ninja
		│   ├── iniparser
		│   │   └── iniparser.ninja
		│   ├── libjpeg
		│   │   └── libjpeg.ninja
		│   ├── libpng
		│   │   └── libpng.ni│   ├── mbedtls
		│   │   ├── mbedtls_gt.ninja
		│   │   ├── mbedtls_shared.ninja
		│   │   └── mbedtls_static.ninja
		│   └── qrcodegen
		│       └── qrcodegen.ninja
		├── utils
		│   └── native
		│       └── lite
		│           ├── kv_store
		│           │   └── src
		│           │       └── utils_kv_store.ninja
		│           └── os_dump
		│               └── os_dump.ninja
		└── vendor
			└── hisilicon
				└── hispark_aries
					└── hals
						├── security
						│   └── permission_lite
						│       └── hal_pms.ninja
						└── utils
							├── sys_param
							│   └── hal_sysparam.ninja
							└── token
								└── haltoken_shared.ninja


		参考文献：
		https://blog.csdn.net/u014786330/article/details/108690110
	https://blog.csdn.net/Vincent95/article/details/78499883
	https://zhuanlan.zhihu.com/p/33218385
	https://zhuanlan.zhihu.com/p/424401397


​	
​		https://blog.csdn.net/yujiawang/article/details/72627138#gn%E8%AF%AD%E6%B3%95%E5%92%8C%E6%93%8D%E4%BD%9C
​		https://www.cnblogs.com/weharmony/p/15253630.html
