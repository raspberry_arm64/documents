# Kconfig语法

## 1、简介



```
1、2.6内核的源码树目录下一般都会有两个文件：Kconfig和Makefile。分布在各目录下的
	Kconfig构成了一个分布式的内核配置数据库，每个Kconfig分别描述了所属目录源文件相关的内核配置菜单。
	在内核配置make menuconfig时，所有配置工具都是通过读取"arch/$(ARCH)Kconfig"文件来生成配置界面，这个文件就是所有配置的总入口，它会包含其他目录的Kconfig
	从Kconfig中读出配置菜单，出现内核配置界面，用户配置完后保存到.config(在顶层目录下生成)中。
	在内核编译时，主Makefile调用这个.config，就知道了用户对内核的配置情况。
```



## 2、kconfig语法

​	

```
Kconfig每个菜单项都有一个关键字标识，最常见的就是config。
	语法：config symboloptions<!--[if!supportLineBreakNewLine]--><!--[endif]
	1、symbol就是新的菜单项
	2、options是在这个新的菜单项下的属性和选项
	其中options部分有：config 下面几行定义了该配置选项的属性。
	属性可以是该配置选项的 - 类型 - 输入提示 - 依赖关系 - 默认值 - 帮助信息
	3、依赖型定义dependson或requires指此菜单的出现是否依赖于另一个定义
		config HELLO_MODULE
		bool "hellotestmodule"
		depends on ARCH_PXA
		这个例子表明HELLO_MODULE这个菜单项只有在选择了ARCH_PXA，该菜单才可见(可配置)。
		如果定义了多个依赖关系，它们之间用 '&&' 间隔。依赖关系也可以应用到该菜单中所有的其它选项。并且			depends on与if等价，如下等价：

 		  bool "foo" if BAR
 		  default y if BAR
			和
 		  depends on BAR
 		  bool "foo"
 		  default y
  		 depends on BAR

	4、帮助性定义只是增加帮助用关键字help或---help---
	5、default 表示配置选项的默认值，bool 类型的默认值可以是 y/n。
	"default" <expr> ["if" <expr>]
	一个配置选项可以有任意多个默认值。如果有多个默认值，那么只有第一个被定义的值是可用的。如果用户没有设置(通	过上面的输入提示)，配置选项的值就是显示输入也即默认值。如果可以显示输入提示的话，就会把默认值显示给用户，并	可以让用户进行修改。
	默认值的依赖关系可以用 "if" 添加。

	6、select 是反向依赖关系的意思，即当前配置选项被选中，则 RT_USING_PIN 就会被选中。

	 - 反向依赖项: "select" <symbol> ["if" <expr>]
 	  一个正常的依赖项可以降低symbol的上限值，而反向依赖项则用来给一个symbol限定一个下限值。当前菜单			symbol的	值是当前可以设定的<symbol>最小值。如果<symbol>被选择了多次，那么它的值限定为最大的那一个。
   只有boolean和tristate类型的symbol可以使用反向依赖。
   7、help 帮助信息。
   "help" or "---help---"
   帮助信息的结束就由缩进的水平决定的，这也就意味着信息是在第一个比帮助信息开始行的缩进小的行结束。
   "---help---" 和 "help" 在实现的作用上没有区别，"---help---" 有助于将文件中的配置逻辑与给开发人员的	提示分开。
	
```

### 1、类型定义：

```
	每个config菜单项都要有类型定义，
		bool：布尔类型,取值范围y/n  ,n不选中，y选中
	eg：
		config BSP_USING_WDT
			bool "Enable Watchdog Timer"
			select RT_USING_WDT
			default n
			
	类型定义可以接受输入提示，所以这两个例子是等效的：
		bool "Networking support"
		和
		bool
		prompt "Networking support"

	输入提示：变量的类型后面跟着输入提示。输入提示也可以单独做一行，使用prompt，格式为：prompt [if ]，
	其中if用来表示该提示的依赖关系。
```



```
	tristate：三态类型，取值范围y内建、m（手动添加内核）模块、n移除
	tristate类型的菜单项多了编译成内核模块的选项，
	假如选择编译成内核模块，则会在.config中生成一个CONFIG_HELLO_MODULE=m的配置
	假如选择内建，就是直接编译成内核影响，就会在.config中生成一个CONFIG_HELLO_MODULE=y的配置
	使用方式与bool相似
```

```
string：字符串类型
	eg：
	config RT_CONSOLE_DEVICE_NAME
		string "the device name for console"
		default "uart1"
```

​	

```
	integer：整型
	eg：
		config BSP_I2C1_SCL_PIN
		int "I2C1 scl pin number"
		range 1 176
		default 116
```

```
	hex：十六进制类型
	使用方式与int类型相似
```




配置文件描述了菜单选项，每行都是以一关键字开头(除了帮助信息)，下一个关键字结束此菜单选项：

​		

### 2、config

	1、config 定义了一组新的配置选项
		eg：
		config BSP_USING_GPIO
			bool "Enable GPIO"
			select RT_USING_PIN
			default y
			help
			config gpio
		分析：config 表示一个配置选项的开始，紧跟着的 BSP_USING_GPIO 是配置选项的名称，

### 3、menuconfig		

​	

```
2、menuconfig 带菜单的配置项
	menu "Hardware Drivers Config"
		menuconfig BSP_USING_UART
			bool "Enable UART"
			default y
			select RT_USING_SERIAL
			if BSP_USING_UART
				config BSP_USING_UART1
					bool "Enable UART1"
					default y
	 
				config BSP_UART1_RX_USING_DMA
					bool "Enable UART1 RX DMA"
					depends on BSP_USING_UART1 && RT_SERIAL_USING_DMA
					default n
			endif
	endmenu
	分析：menuconfig 这个语句和 config 语句很相似，但它在 config 的基础上要求所有的子选项作为独立的行显示。 
	
	depends on 表示依赖某个配置选项
	depends on BSP_USING_UART1 && RT_SERIAL_USING_DMA 表示只有当 BSP_USING_UART1 和 RT_SERIAL_USING_DMA 配置选项同时被选中时，当前配置选项的提示信息才会出现，才能设置当前配置选项
```

### 4、choice/endchoice

```
4、choice/endchoice 单选配置项   将多个类似的配置选项组合在一起，供用户选择一组配置项
	menu "Hardware Drivers Config"
		menuconfig BSP_USING_ONCHIP_RTC
			bool "Enable RTC"
			select RT_USING_RTC
			select RT_USING_LIBC
			default n
			if BSP_USING_ONCHIP_RTC
				choice
					prompt "Select clock source"
					default BSP_RTC_USING_LSE
	 
					config BSP_RTC_USING_LSE
						bool "RTC USING LSE"
	 
					config BSP_RTC_USING_LSI
						bool "RTC USING LSI"
				endchoice
			endif
	endmenu
	分析： prompt 给出提示信息，光标选中后回车进入就可以看到多个 config 条目定义的配置选项； - choice/endchoice 给出选择项，中间可以定义多个配置项供选择，但是在配置界面只能选择一个配置项
```

### 5、comment

```
5、comment 注释  出现在界面第一行，用于定义一些提示信息，这里定义了在配置过程中显示给用户的注释，该注释还将写进输出文件中。
	menu "Hardware Drivers Config"
		comment "uart2 pin conflict with Ethernet and PWM"
		config BSP_USING_COM2
			bool "Enable COM2"
			select BSP_USING_UART
			select BSP_USING_UART2
			default n
	endmenu
	分析：会在该界面的Enable COM2 选项上面显示conment的提示信息
```

### 6、menu/endmenu

```
6、menu/endmenu 这里定义了一个菜单，所有依赖于此菜单的选项都是它的子选项。
	eg：
	menu "Hardware Drivers Config"
		config BSP_USING_COM2
			bool "Enable COM2 (uart2 pin conflict with Ethernet and PWM)"
			select BSP_USING_UART
			select BSP_USING_UART2
			default n
		config BSP_USING_COM3
			bool "Enable COM3 (uart3 pin conflict with Ethernet)"
			select BSP_USING_UART3
			default n
	endmenu
	分析：menu后面字符串为菜单名
	进去菜单后有：Enable COM2   Enable COM3等选项
```

### 7、if/endif

```
7、if/endif 条件判断
	menu "Hardware Drivers Config"
		menuconfig BSP_USING_CAN
			bool "Enable CAN"
			default n
			select RT_USING_CAN
			if BSP_USING_CAN
				config BSP_USING_CAN1
					bool "Enable CAN1"
					default n
			endif
	endmenu
	分析：如果没有选BSP_USING_CAN这个选项，那么Enable CAN1选项就不会显示
```



### 8、source 

```
8、source 读取其他配置文件
	source 语句用于读取另一个文件中的 Kconfig 文件，如：

​	source "../libraries/HAL_Drivers/Kconfig"
​	上述语句用于读取当前 Kconfig 文件所在路径的上一级文件夹 libraries/HAL_Drivers 下的 Kconfig 文件


```



		参考文献：
		https://www.jianshu.com/p/aba588d380c2
		https://blog.csdn.net/c_hnie/article/details/109393488
		https://blog.csdn.net/qq_32220231/article/details/52927685
		https://blog.csdn.net/mcsbary/article/details/100389823
		https://mp.weixin.qq.com/s?src=11&timestamp=1637487006&ver=3450&signature=7pra27601-sEhDJeVebzhSq8hj*yc8Fx9HBogK3Ziu7pzI3vgLMh-faGpFh-9qSUF*mTdgwC2XCGEgmBAP-q6EvG1G*vUe8PuvU1JytxdaUASn4mQbIFu1RyW6MK1aba&new=1



### 9、实例


		实例：
		menu "Hardware Drivers Config"      #定义1级菜单
	
		config SOC_STM32F429IG              #选择MCU
			bool                            #bool数据类型后面为空，表示配置界面不会显示
			select SOC_SERIES_STM32F4
			default y
	
		menu "Onboard Peripheral Drivers"   #定义2级菜单
	
			config BSP_USING_USB_TO_USART   #2级菜单中的一行新的配置项，BSP_USING_USB_TO_USART只是标识
				bool "Enable USB TO USART (uart1)"   #配置项显示的内容，配置类型为bool
				select BSP_USING_UART       #
				select BSP_USING_UART1      #如果使能该配置项，select表示会在rtconfig.h文件增加宏定义
				default y                   #使能配置项，决定上面宏定义是否增加
	
			config BSP_USING_COM2
				bool "Enable COM2 (uart2 pin conflict with Ethernet and PWM)"
				select BSP_USING_UART
				select BSP_USING_UART2
				default n
	
			config BSP_USING_COM3
				bool "Enable COM3 (uart3 pin conflict with Ethernet)"
				select BSP_USING_UART3
				default n
	
			config BSP_USING_SDRAM
				bool "Enable SDRAM"
				select BSP_USING_FMC
				default n
	
			config BSP_USING_SPI_FLASH
				bool "Enable SPI FLASH (W25Q256 spi5)"
				select BSP_USING_SPI
				select BSP_USING_SPI5
				select RT_USING_SFUD
				select RT_SFUD_USING_SFDP
				default n
	
			config BSP_USING_MPU9250
				bool "Enable MPU 9250 (i2c1)"
				select BSP_USING_I2C1
				select PKG_USING_MPU6XXX
				default n
	
			config PHY_USING_LAN8720A
				bool
	
			config BSP_USING_ETH
				bool "Enable Ethernet"
				select BSP_USING_I2C1
				select PKG_USING_PCF8574
				select RT_USING_LWIP
				select PHY_USING_LAN8720A
				default n
	
			config BSP_USING_SDCARD
				bool "Enable SDCARD (sdio)"
				select BSP_USING_SDIO
				select RT_USING_DFS
				select RT_USING_DFS_ELMFAT
				default n
	
		endmenu                             # 2级菜单的结束
	
		menu "On-chip Peripheral Drivers"   # 新的2级菜单
	
			config BSP_USING_GPIO
				bool "Enable GPIO"
				select RT_USING_PIN
				default y
	
			menuconfig BSP_USING_UART       # 带菜单的配置项，配置后可以进入该菜单继续配置
				bool "Enable UART"
				default y                   # 没有select 选项，说明该菜单配置项强制选中
				select RT_USING_SERIAL
				if BSP_USING_UART           # 带菜单的配置项如果被配置，则if生效
					config BSP_USING_UART1
						bool "Enable UART1"
						default y
	
					config BSP_UART1_RX_USING_DMA
						bool "Enable UART1 RX DMA"
						depends on BSP_USING_UART1 && RT_SERIAL_USING_DMA  #依赖两个宏定义
						default n
	
					config BSP_USING_UART2
						bool "Enable UART2"
						default n
	
					config BSP_UART2_RX_USING_DMA
						bool "Enable UART2 RX DMA"
						depends on BSP_USING_UART2 && RT_SERIAL_USING_DMA
						default n
						
					config BSP_USING_UART3
						bool "Enable UART3"
						default n
		   
					config BSP_UART3_RX_USING_DMA
						bool "Enable UART3 RX DMA"
						depends on BSP_USING_UART3 && RT_SERIAL_USING_DMA
						default n  
				endif
	
			config BSP_USING_ON_CHIP_FLASH
				bool "Enable on-chip FLASH"
				default n
	
			menuconfig BSP_USING_SPI
				bool "Enable SPI BUS"
				default n
				select RT_USING_SPI
				if BSP_USING_SPI
					config BSP_USING_SPI1
						bool "Enable SPI1 BUS"
						default n
	
					config BSP_SPI1_TX_USING_DMA
						bool "Enable SPI1 TX DMA"
						depends on BSP_USING_SPI1
						default n
						
					config BSP_SPI1_RX_USING_DMA
						bool "Enable SPI1 RX DMA"
						depends on BSP_USING_SPI1
						select BSP_SPI1_TX_USING_DMA
						default n
	
					config BSP_USING_SPI2
						bool "Enable SPI2 BUS"
						default n  
						
					config BSP_SPI2_TX_USING_DMA
						bool "Enable SPI2 TX DMA"
						depends on BSP_USING_SPI2
						default n
						
					config BSP_SPI2_RX_USING_DMA
						bool "Enable SPI2 RX DMA"
						depends on BSP_USING_SPI2
						select BSP_SPI2_TX_USING_DMA
						default n
						
					config BSP_USING_SPI5
						bool "Enable SPI5 BUS"
						default n        
	
					config BSP_SPI5_TX_USING_DMA
						bool "Enable SPI5 TX DMA"
						depends on BSP_USING_SPI5
						default n
						
					config BSP_SPI5_RX_USING_DMA
						bool "Enable SPI5 RX DMA"
						depends on BSP_USING_SPI5
						select BSP_SPI5_TX_USING_DMA
						default n  
				endif
	
			menuconfig BSP_USING_I2C1
				bool "Enable I2C1 BUS (software simulation)"
				default n
				select RT_USING_I2C
				select RT_USING_I2C_BITOPS
				select RT_USING_PIN
				if BSP_USING_I2C1
					comment "Notice: PH4 --> 116; PH5 --> 117"   #配置项的注释说明
					config BSP_I2C1_SCL_PIN
						int "I2C1 scl pin number"                #配置项类型为整型
						range 1 176                              #配置项的值取值范围设定
						default 116
					config BSP_I2C1_SDA_PIN
						int "I2C1 sda pin number"
						range 1 176
						default 117
				endif
	
			menuconfig BSP_USING_TIM
				bool "Enable timer"
				default n
				select RT_USING_HWTIMER
				if BSP_USING_TIM
					config BSP_USING_TIM11
						bool "Enable TIM11"
						default n
	
					config BSP_USING_TIM13
						bool "Enable TIM13"
						default n
	
					config BSP_USING_TIM14
						bool "Enable TIM14"
						default n
				endif
	
			menuconfig BSP_USING_PWM
				bool "Enable pwm"
				default n
				select RT_USING_PWM
				if BSP_USING_PWM
				menuconfig BSP_USING_PWM2
					bool "Enable timer2 output pwm"
					default n
					if BSP_USING_PWM2
						config BSP_USING_PWM2_CH4
							bool "Enable PWM2 channel4"
							default n
					endif
				endif
	
			menuconfig BSP_USING_ADC
				bool "Enable ADC"
				default n
				select RT_USING_ADC
				if BSP_USING_ADC
					config BSP_USING_ADC1
						bool "Enable ADC1"
						default n
				endif
	
			menuconfig BSP_USING_ONCHIP_RTC
				bool "Enable RTC"
				select RT_USING_RTC
				select RT_USING_LIBC
				default n
				if BSP_USING_ONCHIP_RTC               #条件判断
					choice                            #选择配置项，其值可选，类似单选框功能
						prompt "Select clock source"  #配置项的显示名称
						default BSP_RTC_USING_LSE     #默认选择的配置
	
						config BSP_RTC_USING_LSE      #配置选项
							bool "RTC USING LSE"
	
						config BSP_RTC_USING_LSI
							bool "RTC USING LSI"
					endchoice                         #结束选择配置项
				endif
	
			config BSP_USING_WDT
				bool "Enable Watchdog Timer"
				select RT_USING_WDT
				default n
	
			config BSP_USING_SDIO
				bool "Enable SDIO"
				select RT_USING_SDIO
				select RT_USING_DFS
				default n
	
			config BSP_USING_FMC
				bool
				default n
		endmenu
	
		menu "Board extended module Drivers"
	
		endmenu
	
		endmenu