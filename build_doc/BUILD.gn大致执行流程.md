# 

# 1 开始时间 

2021年12月7号 



# 2 个人理解 

1 使用 gn gen 命令时

进入gn 入口  .gn

​		buildconfig = "//build/config/BUILDCONFIG.gn"

​		root = "//build/core/gn"

​		script_executable = "/usr/bin/env"

配置环境

第一个build.gn执行路径

根据第一个BULID.gn的内容完成编译

2 使用 hb build -f

不会用到.gn

第一个执行build.gn路径    build/lite

配置                                  build/lite/config/CONFIGBUILD.gn



BUILD.gn 通过deps  public_deps  执行对应路径的BUILD.gn

通过读取.json，执行其组件中target对应的路径的BUILD.gn

# 3  初步总结



## 1 执行步骤             

ipcamera_hispark_taurus

1 hb set  生成 ohos_config.json 

2 加载  vendor/hisilicon/hispark_taurus/config.json

3 加载  build/lite/config/BUILDCONFIG.gn







4 执行 build/lite  下的  BUILD.gn

5 prebuilts/lite/sysroot/build/BUILD.gn   与  prebuilts/lite/sysroot/BUILD.gn 

与   build/lite/BUILD.gn  形成循环依赖    提供编译所需

6 prebuilts/lite/sysroot/build/makefile     提供编译所需





7 BUILD.gn  中  group("ohos")

8 遍历 vendor/hisilicon/hispark_taurus/config.json

9 获取 config.json 中所有 subsystem_name

10 读取 build/lite/components  下的所有subsystem_name.json 

11 遍历subsystem_name.json 下所有的components

12 编译所有的components 的 所有target



## 2 执行makefile文件

```
if (ohos_build_type == "debug" && product != "") {
  action("gen_testfwk_info") {
    outputs = [ "$root_out_dir/gen_testfwk_info.log" ]
    script = "//build/lite/testfwk/gen_testfwk_info.py"
    archive_dir_name = "test_info"
    args = [
      "--component-info-file",
      rebase_path("${product_path}/config.json"),
      "--output-json-fold",
      rebase_path("${root_out_dir}/${archive_dir_name}/build_configs/"),
      "--output-json-file-name",
      "infos_for_testfwk.json",
      "--output-module-list-files-fold",
      rebase_path("${root_out_dir}/${archive_dir_name}/module_list_files/"),
    ]
  }
}
```





# build/lite/BUILD.gn代码

```
import("//build/lite/ndk/ndk.gni")

#要求gn version >= 1714

​```
assert(gn_version >= 1714, "GN version 1714 required, please upgrade!")
​```

#写版本信息。 

​```
version_info = [
  "VERSION=\"$ohos_version\"",
  "BUILD_TIME=\"$ohos_build_datetime\"",
]
write_file("$root_build_dir/etc/version-info", version_info)
​```

#这个小组将依赖的目标标记为预构建   

​```
group("mark_as_prebuilts") {
}

group("prebuilts") {
  public_deps = [ "//prebuilts/lite/sysroot" ]
}
​```



# group("ohos")

group("ohos") {
  deps = []
  if (ohos_build_target == "") {

1读取配置文件product_path的值来源于根目录的ohos_config.json,如下,内容由 hb set 命令生成

​    product_cfg = read_file("${product_path}/config.json", "json")

2#循环处理各自子系统,config.json中子系统部分格式如下hb

#对子系统数组遍历操作

    foreach(product_configed_subsystem, product_cfg.subsystems) {

#读取一个子系统  如aafwk,hiviewdfx,security

      subsystem_name = product_configed_subsystem.subsystem
      subsystem_info = {
      }

第三步: 读取各个子系统的配置文件

      subsystem_info =
          read_file("//build/lite/components/${subsystem_name}.json", "json")

第四步: 循环读取子系统内各控件的配置信息 

​```
#遍历项目控件数组
  foreach(product_configed_component,   product_configed_subsystem.components) {    

			第五步: 检查控件配置信息是否存在
			#初始为不存在
    		component_found = false
			项目控件和子系统中的控件遍历对比
    foreach(system_component, subsystem_info.components) {
      if (product_configed_component.component ==
          system_component.component) {
			找到了liteos_a
        component_found = true
      }
    }
    		如果没找到的信息,则打印项目控件查找失败日志
    assert(
        component_found,
        "Component \"${product_configed_component.component}\" not found" +
            ", please check your product configuration.")
            第六步: 检查子系统控件的有效性并遍历控件组,处理各个控件
     	foreach(component, subsystem_info.components) {
     			 kernel_valid = false
     			 board_valid = false
				跳过产品未配置的组件。  
     		 if (component.component == product_configed_component.component) {
        		# Step 6.1.1: Loop OS components adapted kernel type.
        		foreach(component_adapted_kernel, component.adapted_kernel) {
         		 if (component_adapted_kernel == product_cfg.kernel_type &&
         		  kernel_valid == false) {
         		  内核检测是否已适配
            kernel_valid = true
          }
        }
        	 如果内核未适配,则打印未适配日志
          assert(
            kernel_valid,
            "Invalid component configed, 						${subsystem_name}:${product_configed_component.component} " + "not available for kernel: ${product_cfg.kernel_type}!")

        添加有效组件进行编译
         # Skip kernel target for userspace only scenario.
        #跳过只使用用户空间的内核目标  
        if (!ohos_build_userspace_only ||
            (ohos_build_userspace_only && subsystem_name != "kernel" &&
             subsystem_name != "vendor")) {
          foreach(component_target, component.targets) {
            deps += [ component_target ]
          }
        }
      }
    }
  }
}
​```

 #仅使用用户空间时跳过设备目标  

Skip device target for userspace only scenario.

       if (!ohos_build_userspace_only) {

 Step 7: Add device and product target by default.

第七步: 添加设备和项目的编译单元

添加 //device/hisilicon/hispark_aries 进入编译项

      deps += [ "${device_path}/../" ]
    }

  } else {

编译指定的组件,例如 hb build -T targetA&&targetB

    deps += string_split(ohos_build_target, "&&")

  }
}

group("product") {
  deps = []

 构建产品，跳过构建单个组件场景。   

  if (ohos_build_target == "") {
    deps += [ "${product_path}" ]
  }
}

group("ndk") {

添加本机API目标。

  deps = []
  if (ohos_build_ndk) {
    deps += [ "//build/lite/ndk:ndk" ]
  }
}

#group("AAAAA") {

deps = [

"//src:cdev",

"//src:cdispatch",

]

#}



if (ohos_build_type == "debug" && product != "") {
  action("gen_testfwk_info") {
    outputs = [ "$root_out_dir/gen_testfwk_info.log" ]
    script = "//build/lite/testfwk/gen_testfwk_info.py"
    archive_dir_name = "test_info"
    args = [
      "--component-info-file",
      rebase_path("${product_path}/config.json"),
      "--output-json-fold",
      rebase_path("${root_out_dir}/${archive_dir_name}/build_configs/"),
      "--output-json-file-name",
      "infos_for_testfwk.json",
      "--output-module-list-files-fold",
      rebase_path("${root_out_dir}/${archive_dir_name}/module_list_files/"),
    ]
  }
}


```



#  prebuilts

1 拉取的代码 中有/prebuilts/lite 文件

2 prebuilts/lite/sysroot/build/BUILD.gn   与  prebuilts/lite/sysroot/BUILD.gn 与   build/lite/BUILD.gn  形成循环依赖

prebuilts/lite/sysroot/build/makefile    

  与编译环境有关       是生成可执行文件必须的   

3 删除prebuilts   ,使用 hb build -f 命令  会生成 prebuilts  但没有/prebuilts/lite 文件



# hb build

执行 build/lite/hb/build/build.py 中的 exec_command() 函数，该函数主要处理用户的入参，如：

-b：debug 或 release
-c：指定编译器，默认是 clang
-t：是否编译 test suit
-f：full，编译全部代码
-t：是否编译 ndk，本地开发包，这也是 @ohos/build_lite 组件的一部分
-T：单模块编译
-v：verbose



#  参考网站

 [鸿蒙编译构建丨hb工具分析【附源码】_HarmonyOS技术社区_51CTO博客](https://blog.51cto.com/harmonyos/2896471) 



 [(3条消息) 鸿蒙源码导读-02：编译构建子系统_孤星泪-CSDN博客_ohos鸿蒙](https://blog.csdn.net/kevin881/article/details/119298709) 





























































