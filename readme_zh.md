# 文档汇总介绍

该仓主要是学习过程中的总结文档汇总，方便大家学习和查阅

documents仓权限申请链接

https://gitee.com/halley5/documents/invite_link?invite=dadd14997da9bf7986a171142a06eba77abcbf92f63f78b9b7993663152babc552898748f5f8cefa5f318cd36bbddc3a

## 一、文档列表

该目录存放平时大家学习总结的内容，当前包含的文件

| 文档名                                                       | 文档介绍                          |
| :----------------------------------------------------------- | --------------------------------- |
| [操作手册文档汇集](./operation_manual/操作手册文档汇集.md)   | 主要放置一些基本的操作文档        |
| [code代码介绍](./code/code代码介绍.md)                       | 代码模板                          |
| [img目录](./img)                                             | 该目录存放所有md文档中的图片      |
| [ace相关文档汇集](./ace_doc/ace相关文档汇集.md)              | ace相关的文档汇集                 |
| [arm64相关文档汇集](./arm64_doc/arm64相关文档汇集.md)        | arm64架构中的一些资料和总结文档   |
| [binder相关文档汇集](./binder_doc/binder相关文档汇集.md)     | binder通信文档的汇集              |
| [hdf相关文档汇集](./hdf_doc/hdf相关文档汇集.md)              | Harmony HDF驱动框架相关的文档汇集 |
| [君正x2000代码配置更新](./ingenic_doc/君正x2000代码配置更新.md) | 君正的总结文档入口                |
| [ipc相关文档汇集](./ipc_doc/ipc相关文档汇集.md)              | 进程间通信ipc相关的文档和总结文档 |
| [lmk相关文档汇集](./lmk_doc/lmk相关文档汇集.md)              | Lower memory kill相关的文档       |
| [mips参考资料汇集](./mips_doc/mips参考资料汇集.md)           | mips架构的资料文档                |
| [树莓派相关文档汇集](./raspberry_doc/树莓派相关文档汇集.md)  | 树莓派相关的资料文档              |
| [tmp目录](./tmp)                                             | 临时文件存放                      |
|                                                              |                                   |
|                                                              |                                   |
|                                                              |                                   |
|                                                              |                                   |
|                                                              |                                   |
|                                                              |                                   |
|                                                              |                                   |
|                                                              |                                   |
|                                                              |                                   |

## 二、文档编写规范
