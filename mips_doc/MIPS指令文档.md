# MIPS指令

处理器需要实现的指令包括所有非浮点MIPS I指令（除去4条非对齐访存指令LWL、LWR、SWL和SWR）以及MIPS32中的ERET指令，有14条算术运算指令、8条逻辑运算指令，6条移位指令、12条分支跳转指令、4条数据移动指令、2条自陷指令、8条访存指令、3条特权指令，共计57条。下面分类给出各部分指令的简要功能介绍。

表1算术运算指令

| 指令名称格式            | 指令功能                   |
| ----------------------- | -------------------------- |
| ADD rd, rs, rt          | 加（可产生溢出例外）       |
| ADDI rt, rs, immediate  | 加立即数（可产生溢出例外） |
| ADDU rd, rs, rt         | 加（不产生溢出例外）       |
| ADDIU rt, rs, immeidate | 加立即数（不产生溢出例外） |
| SUB rd. rs, rt          | 减（可产生溢出例外）       |
| SUBU rd, rs, rt         | 减（不产生溢出例外）       |
| SLT rd, rs, rt          | 有符号小于置1              |
| SLTI rt, rs, immediate  | 有符号小于立即数设置1      |
| SLTU rd, rs, rt         | 无符号小于设置1            |
| SLTIU rt, rs, immediate | 无符号小于立即数[1]设置1   |
| DIV rs, rt              | 有符号字除                 |
| DIVU rs,rt              | 无符号字除                 |
| MULT rs, rt             | 有符号字乘                 |
| MULTU rs, rt            | 无符号字乘                 |

 

表2逻辑运算指令

| 指令名称格式           | 指令功能               |
| ---------------------- | ---------------------- |
| AND rd, rs, rt         | 位与                   |
| ANDI rt, rs, immediate | 立即数位与             |
| LUI rt,immediate       | 寄存器高半部分置立即数 |
| NOR rd, rs, rt         | 位或非                 |
| OR rd, rs, rt          | 位或                   |
| ORI rt, rs, immediate  | 立即数位或             |
| XOR rd, rs, rt         | 位异或                 |
| XORI rt, rs, immediate | 立即数位异或           |

 

表3移位指令

| 指令名称格式    | 指令功能       |
| --------------- | -------------- |
| SLL rd, rt, sa  | 立即数逻辑左移 |
| SLLV rd, rs, rt | 变量逻辑左移   |
| SRA rd, rt, sa  | 立即数算术右移 |
| SRAV rd, rs, rt | 变量算术右移   |
| SRL rd, rt, sa  | 立即数逻辑右移 |
| SRLV rd, rs, rt | 变量逻辑右移   |



表4分支跳转指令

| 指令名称格式       | 指令功能                                 |
| ------------------ | ---------------------------------------- |
| BEQ rs, rt, offset | 相等转移                                 |
| BNE rs, rt, offset | 不等转移                                 |
| BGEZ rs, offset    | 大于等于0转移                            |
| BGTZ rs, offset    | 大于0转移                                |
| BLEZ rs, offset    | 小于等于0转移                            |
| BLTZ rs, offset    | 小于0转移                                |
| BLTZAL rs, offset  | 小于0调用子程序并保存返回地址            |
| BGEZAL rs, offset  | 大于等于0调用子程序并保存返回地址        |
| J target           | 无条件直接跳转                           |
| JAL target         | 无条件直接跳转至子程序并保存返回地址     |
| JR rs              | 无条件寄存器跳转                         |
| JALR rd, rs        | 无条件寄存器跳转至子程序并保存返回地址下 |

 

表5数据移动指令

| 指令名称格式 | 指令功能             |
| ------------ | -------------------- |
| MFHI rd      | HI寄存器至通用寄存器 |
| MFLO rd      | LO寄存器至通用寄存器 |
| MTHI rs      | 通用寄存器至HI寄存器 |
| MTLO rs      | 通用寄存器至LO寄存器 |



表6自陷指令

| 指令名称格式 | 指令功能                  |
| ------------ | ------------------------- |
| BREAK        | 断点                      |
| SYSCALL      | 系统调用                  |
| ERET         | 例外处理返回              |
| MFC0         | 读CP0寄存器值至通用寄存器 |
| MTC0         | 通用寄存器值写入CP0寄存器 |

 

表7访存指令

| 指令名称格式         | 指令功能         |
| -------------------- | ---------------- |
| LB rt, offset(base)  | 取字节有符号扩展 |
| LBU rt, offset(base) | 取字节无符号扩展 |
| LH rt, offset(base)  | 取半字有符号扩展 |
| LHU rt, offset(base) | 取半字无符号扩展 |
| LW rt, offset(base)  | 取字             |
| SB rt, offset(base)  | 存字节           |
| SH rt, offset(base)  | 存半字           |
| SW rt, offset(base)  | 存字             |

## 1、算术运算指令

### 1.1 ADD

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps51E.tmp.jpg) 

汇编格式：ADD rd, rs, rt

功能描述：将寄存器rs的值与寄存器rt的值相加，结果写入寄存器rd中。如果产生溢出，则触发整型溢出例外（IntegerOverflow）（带符号数相加）

公式说明：rd = rs + rt

举例：add $1, $2, $3  表示：$1 = $2 + $3

 

### 1.2 ADDI

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps52F.tmp.jpg) 

汇编格式：ADDI rt, rs, imm

功能描述：将寄存器rs的值与有符号扩展至32位的立即数imm相加，结果写入rt寄存器中。如果产生溢出，则触发整型溢出例外（IntegerOverflow）。

公式说明：rt = rs + imm

举例：addi $1, $1, 2  表示：$1 = $1 + 2

 

### 1.3 ADDU

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps530.tmp.jpg) 

汇编格式：ADDU rd, rs, rt

功能描述：将寄存器rs的值与寄存器rt的值相加，结果写入rd寄存器中。（无符号数相加）

公式说明：rd = rs +rt

举例：addu $1, $2, $3  表示：$1 = $2 + $3

 

### 1.4 ADDIU

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps531.tmp.jpg) 

汇编格式：ADDIU rt, rs, imm 

功能描述：将寄存器rs的值与有符号扩展至32位的立即数imm相加，结果写入rt寄存器中。

公式说明：rt = rs + imm

举例：addiu $1, $1, 2  表示：$1 = $1 + 2

 

### 1.5 SUB

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps532.tmp.jpg) 

汇编格式：SUB rd, rs, rt 

功能描述：将寄存器rs的值与寄存器rt的值相减，结果写入rd寄存器中。如果产生溢出，则触发整型溢出例外（IntegerOverflow）。（有符号数相减）

公式说明：rd = rs - rt

举例：sub $1, $2, $3  表示：$1 = $2 - $3

 

### 1.6 SUBU

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps533.tmp.jpg) 

汇编格式：SUBU rd, rs, rt

功能描述：将寄存器rs的值与寄存器rt的值相减，结果写入rd寄存器中。（无符号数相减）

公式说明：rd = rs - rt

举例：subu $1, $2, $3  表示：$1 = $2 - $3

 

### 1.7 SLT

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps534.tmp.jpg) 

汇编格式：SLT rd, rt, rs 

功能描述：将寄存器rs的值与寄存器rt中的值进行有符号数比较，如果寄存器rs中的值小，则寄存器rd置1；否则寄存器rd置0。

公式说明：rd =1(rt > rs);  rd =0(rt < rs)

举例：slt $1, $2, $3  表示：if $2 > $3, $1 = 1; else, $1 = 0; 

 

### 1.8 SLT1

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps535.tmp.jpg) 

汇编格式：SLTI rt, rs, imm 

功能描述：将寄存器rs的值与有符号扩展至32位的立即数imm进行有符号数比较，如果寄存器rs中的值小，则寄存器rt置1；否则寄存器rt置0。

公式说明：rt =1(rs < imm);  rt =0(rs > imm)

举例：slti $1, $2, 10  表示：if $2 < 10, $1 = 1; else, $1 = 0; 

 

### 1.9 SLTU

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps536.tmp.jpg) 

汇编格式：SLTU rd, rs, rt 

功能描述：将寄存器rs的值与寄存器rt中的值进行无符号数比较，如果寄存器rs中的值小，则寄存器rd置1；否则寄存器rd置0。

公式说明：rd =1(rs < rt);  rd =0(rs > rt)

举例：sltu $1, $2, $3  表示：if $2 < $3, $1 = 1; else, $1 = 0;  

 

### 1.10 SLTIU

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps537.tmp.jpg) 

汇编格式：SLTIU rt, rs, imm

功能描述：将寄存器rs的值与有符号扩展至32位的立即数imm进行无符号数比较，如果寄存器rs中的值小，则寄存器rt置1；否则寄存器rt置0。

公式说明：rt =1(rs < imm);  rt =0(rs > imm)

举例：sltiu $1, $2, 10  表示：if $2 < 10, $1 = 1; else, $1 = 0; 

 

### 1.11 DIV

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps538.tmp.jpg) 

汇编格式：DIV rs, rt

功能描述：有符号除法，寄存器rs的值除以寄存器rt的值，商写入LO寄存器中，余数写入HI寄存器中。(有符号数运算时，输入的被除数和除数都是补码，做除法时需要先将补码转成原码。)

公式说明：LO←q = rs div rt  HI←r = rs mod rt

举例：div t0, t1  表示：LO = t0/t1，HI = t0%t1

 

### 1.12 DIVU

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps539.tmp.jpg) 

汇编格式：DIVU rs, rt 

功能描述：无符号除法，寄存器rs的值除以寄存器rt的值，商写入LO寄存器中，余数写入HI寄存器中。

公式说明：LO←q = rs div rt  HI←r = rs mod rt

 

### 1.13 MULT

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps53A.tmp.jpg) 

汇编格式：MULT rs, rt 

功能描述：有符号乘法，寄存器rs的值乘以寄存器rt的值，乘积的低半部分和高半部分分别写入LO寄存器和HI寄存器。

公式说明：prod ← GPR[rs]31..0× GPR[rt]31..0

LO ← prod31..0 

HI ← prod63..32

 

### 1.14 MULTU

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps53B.tmp.jpg) 

汇编格式：MULTU rs, rt

功能描述：无符号乘法，寄存器rs的值乘以寄存器rt的值，乘积的低半部分和高半部分分别写入LO寄存器和HI寄存器。

公式说明：prod ← (0 ||GPR[rs]31..0) × (0 || GPR[rt]31..0)

LO ← prod31..0 

HI ← prod63..32

 

## 2、逻辑运算指令

### 2.1 AND

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps53C.tmp.jpg) 

汇编格式：ADDU rd, rs, rt

功能描述：寄存器rs中的值与寄存器rt中的值按位逻辑与，结果写入寄存器rd中。

公式说明：rd = rs & rt

举例：addu $1, $2, $3  表示：$1 = $2 & $3



### 2.2 ANDI

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps53D.tmp.jpg) 

汇编格式：ANDI rt, rs, imm 

功能描述：寄存器rs中的值与0扩展至32位的立即数imm按位逻辑与，结果写入寄存器rt中。

公式说明：rt = rs & imm  GPR[rt] ← GPR[rs] and Zero_extend(imm)

举例：andi $1, $2, 10  表示：$1 = $2 & 10

 

### 2.3 LUI

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps53E.tmp.jpg) 

汇编格式：LUI rt, imm

功能描述：将16位立即数imm写入寄存器rt的高16位，寄存器rt的低16位置0。

公式说明：rt = imm*65536

举例：lui $1,100  表示：$1 = 100*65536

 

### 2.4 NOR

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps53F.tmp.jpg) 

汇编格式：NOR  rd, rs, rt 

功能描述：寄存器rs中的值与寄存器rt中的值按位逻辑或非，结果写入寄存器rd中。

公式说明：rd = ~(rs | rt)

举例：nor $1, $2, $3  表示：$1 = ~($2 | $3)

 

### 2.5 OR

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps540.tmp.jpg) 

汇编格式：OR rd, rs, rt 

功能描述：寄存器rs中的值与寄存器rt中的值按位逻辑或，结果写入寄存器rd中。

公式说明：rd = (rs | rt)

举例：or $1, $2, $3  表示：$1 = ($2 | $3)

 

### 2.6 ORI

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5CE.tmp.jpg) 

汇编格式：ORI rt, rs, imm

功能描述：寄存器rs中的值与0扩展至32位的立即数imm按位逻辑或，结果写入寄存器rt中。

公式说明：rt = (rs | imm)

举例：ori $1, $2, 10  表示：$1 = ($2 | 10)

 

### 2.7 XOR

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5CF.tmp.jpg) 

汇编格式：XOR rd, rs, rt

功能描述：寄存器rs中的值与寄存器rt中的值按位逻辑异或，结果写入寄存器rd中。 

公式说明：rd = rs ^ rt

举例：xor $1, $2, $3  表示：$1 = $2 ^ $3

 

### 2.8 XORI

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5D0.tmp.jpg) 

汇编格式：XORI  rt, rs, imm

功能描述：寄存器rs中的值与0扩展至32位的立即数imm按位逻辑异或，结果写入寄存器rt中。

公式说明：rt = rs ^ imm

举例：xori $1, $2, 10  表示：$1 = $2 ^ 10

 

## 3、移位指令

### 3.1 SLLV

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5D1.tmp.jpg) 

汇编格式：SLLV rd, rt, rs

功能描述：由寄存器rs中的值指定移位量，对寄存器rt的值进行逻辑左移，结果写入寄存器rd中。

公式说明：rd = rt << rs

举例：sllv $1, $2, $3  表示：$1 = $2 << $3

 

### 3.2 SLL

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5D2.tmp.jpg) 

汇编格式：SLL rd, rt, sa

功能描述：由立即数sa指定移位量，对寄存器rt的值进行逻辑左移，结果写入寄存器rd中。

公式说明：rd = rt << sa

举例：sll $1, $2, 10  表示：$1 = $2 << 10



### 3.3 SRAV

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5D3.tmp.jpg) 

汇编格式：SRAV  rd, rt, rs 

功能描述：由寄存器rs中的值指定移位量，对寄存器rt的值进行算术右移，结果写入寄存器rd中。

公式说明：rd = rt >> rs （）

举例：srav $1, $2, $3  表示：$1 = $2 >> $3

 

### 3.4 SRA

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5D4.tmp.jpg) 

汇编格式：SRA  rd, rt, sa 

功能描述：由立即数sa指定移位量，对寄存器rt的值进行算术右移，结果写入寄存器rd中。sa的范围为0-31。

公式说明：rd = rt >> sa

举例：sra $1, $2, 10  表示：$1 = $2 >> 10

 

### 3.5 SRLV

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5D5.tmp.jpg) 

汇编格式：SRLV  rd, rt, rs

功能描述：由寄存器rs指定移位量，对寄存器rt的值进行逻辑右移，结果写入寄存器rd中。

公式说明：rd = rt >> rs

举例：srlv $1, $2, $3  表示：$1 = $2 >> $3

 

### 3.6 SRL

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5D6.tmp.jpg) 

汇编格式：SRL  rd, rt, sa

功能描述：由立即数sa指定移位量，对寄存器rt的值进行逻辑右移，结果写入寄存器rd中。sa取值范围在0-31。

公式说明：rd = rt >> sa

举例：srl $1, $2, 10  表示：$1 = $2 >> 10

 

## 4、分支跳转指令

说明：MIPS体系架构中，跳转、分支和子程序调用的命名规则如下：和PC相关的相对寻址指令（地址计算依赖于PC值）称为“分支”(branch)，助记词以b开头。绝对地址指令（地址计算不依赖PC值）称为“跳转”（Jump），助记词以j开头。子程序调用为“跳转并链接”或“分支并链接”，助记词以al结尾。

### 4.1 BEQ

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5D7.tmp.jpg) 

汇编格式：BEQ rs, rt, offset

功能描述：如果寄存器rs的值等于寄存器rt的值则转移，否则顺序执行。转移目标由立即数offset左移2位并进行有符号扩展的值加上该分支指令对应的延迟槽指令的PC计算得到。

公式说明：if(rs = = rt)  PC = PC + 4 + (sign-extend)immediate << 2

举例：beq $1, $2, 10  表示：if($1 = = $2)  PC = PC + 4 + 40

 

### 4.2 BNE

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5D8.tmp.jpg) 

汇编格式：BNE rs, rt, offset

功能描述：如果寄存器rs的值不等于寄存器rt的值则转移，否则顺序执行。转移目标由立即数offset左移2位并进行有符号扩展的值加上该分支指令对应的延迟槽指令的PC计算得到。

公式说明：if(rs != rt)  PC = PC + 4 + (sign-extend)immediate << 2

举例：bne $1, $2, 10  表示：if($1 != $2)  PC = PC + 4 + 40

 

### 4.3 BGEZ

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5E8.tmp.jpg) 

汇编格式：BGEZ rs, offset

功能描述：如果寄存器rs的值大于等于0则转移，否则顺序执行。转移目标由立即数offset左移2位并进行有符号扩展的值加上该分支指令对应的延迟槽指令的PC计算得到。

公式说明：if(rs >= 0)  PC = PC + 4 + (sign-extend)immediate << 2

举例：bgez $1, 10  表示：if($1 >= 0)  PC = PC + 4 + 40

 

### 4.4 BGTZ

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5E9.tmp.jpg) 

汇编格式：BGTZ rs, offset 

功能描述：如果寄存器rs的值大于0则转移，否则顺序执行。转移目标由立即数offset左移2位并进行有符号扩展的值加上该分支指令对应的延迟槽指令的PC计算得到。

公式说明：if(rs > 0)  PC = PC + 4 + (sign-extend)immediate << 2

举例：bgtz $1, 10  表示：if($1 > 0)  PC = PC + 4 + 40

 

### 4.5 BLEZ

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5EA.tmp.jpg) 

汇编格式：BLEZ rs, offset 

功能描述：如果寄存器rs的值小于等于0则转移，否则顺序执行。转移目标由立即数offset左移2位并进行有符号扩展的值加上该分支指令对应的延迟槽指令的PC计算得到。

公式说明：if(rs <= 0)  PC = PC + 4 + (sign-extend)immediate << 2

举例：blez $1, 10  表示：if($1 <= 0)  PC = PC + 4 + 40

 

### 4.6 BLTZ

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5EB.tmp.jpg) 

汇编格式：BLTZ rs, offset

功能描述：如果寄存器rs的值小于0则转移，否则顺序执行。转移目标由立即数offset左移2位并进行有符号扩展的值加上该分支指令对应的延迟槽指令的PC计算得到。

公式说明：if(rs < 0)  PC = PC + 4 + (sign-extend)immediate << 2

举例：bltz $1, 10  表示：if($1 < 0)  PC = PC + 4 + 40

 

### 4.7 BGEZAL

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5EC.tmp.jpg) 

汇编格式：BGEZAL rs, offset

功能描述：如果寄存器rs的值大于等于0则转移，否则顺序执行。转移目标由立即数offset左移2位并进行有符号扩展的值加上该分支指令对应的延迟槽指令的PC计算得到。无论转移与否，将该分支对应延迟槽指令之后的指令的PC值保存至第31号通用寄存器中。

 

### 4.8 BLTZAL

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5ED.tmp.jpg) 

汇编格式：BLTZAL rs, offset

功能描述：如果寄存器rs的值小于0则转移，否则顺序执行。转移目标由立即数offset左移2位并进行有符号扩展的值加上该分支指令对应的延迟槽指令的PC计算得到。无论转移与否，将该分支对应延迟槽指令之后的指令的PC值保存至第31号通用寄存器中。



### 4.9 J

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5EE.tmp.jpg) 

汇编格式：J target

功能描述：无条件跳转到一个绝对目标地址。地址可寻址范围在256MB以内。如果要跳跃到大于256MB的地址，可以使用JR指令（寄存器指令跳转）

举例：j 10000  表示： goto 10000

 

### 4.10 JAL

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5EF.tmp.jpg) 

汇编格式：JAL target

功能描述：无条件跳转。跳转目标由该分支指令对应的延迟槽指令的PC的最高4位与立即数instr_index左移2位后的值拼接得到。同时将该分支对应延迟槽指令之后的指令的PC值保存至第31号通用寄存器中。即：跳转到指定地址的同时，还要保存返回地址（PC+8）到寄存器ra（$31）中。

公式说明：$31 = PC + 8 

举例：jal 10000  表示：$31 = PC + 8; goto 10000

 

### 4.11 JR

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5F0.tmp.jpg) 

汇编格式：JR rs

功能描述：无条件跳转。跳转目标为寄存器rs中的值。

公式说明：PC = rs

举例：jr $31  表示：goto $31

 

### 4.12 JALR

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5F1.tmp.jpg) 

汇编格式：JALR rd, rs    JALR rs (rd=31 implied)

功能描述：无条件跳转。跳转目标为寄存器rs中的值，同时将该分支对应延迟槽指令之后的指令的PC值（即：指令自身的地址+8）保存至寄存器rd中。

公式说明：rd = PC + 8  PC = rs

 

## 5、数据移动指令

### 5.1 MFHI

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5F2.tmp.jpg) 

汇编格式：MFHI rd

功能描述：将HI寄存器的值拷贝到寄存器rd中。

 

### 5.2 MFLO

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5F3.tmp.jpg) 

汇编格式：MFLO rd

功能描述：将LO寄存器的值拷贝到寄存器rd中。

 

### 5.3 MTHI

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5F4.tmp.jpg) 

汇编格式：MTHI rs

功能描述：将寄存器rs的值拷贝到HI寄存器中。



### 5.4 MTLO

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5F5.tmp.jpg) 

汇编格式：MTLO rs

功能描述：将寄存器rs的值拷贝到LO寄存器中。

 

## 6、自陷指令

### 6.1 BREAK

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5F6.tmp.jpg) 

汇编格式：BREAK

功能描述：触发断点例外。

公式说明：SignalException(Breakpoint)

 

### 6.2 SYSCALL

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5F7.tmp.jpg) 

汇编格式：SYSCALL

功能描述：触发系统调用例外。

公式说明：SignalException(SystemCall)

 

## 7、访存指令

说明：MIPS 体系结构采用 load/store 架构。所有运算都在寄存器上进行，只有访存指令可对主存中的数据进行访问。L开头的都是从主存加载数据操作，S开头的都是存储数据操作。操作数据的大小有B（Byte 8bit）、H（Halfword 16bit）、W（Word 32bit）、D（Double 64bit）。加载地址都是base+offset方式，offset取值范围为：-32767~32768。

### 7.1 LB

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5F8.tmp.jpg) 

汇编格式：LB rt, offset(base) 

功能描述：将base寄存器的值加上符号扩展后的立即数offset得到访存的虚地址，据此虚地址从存储器中读取1个字节的值并进行符号扩展，写入到rt寄存器中。（加载字节）

公式说明：rt = memory[base + offset]

 

### 7.2 LBU

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5F9.tmp.jpg) 

汇编格式：LBU rt, offset(base) 

功能描述：将base寄存器的值加上符号扩展后的立即数offset得到访存的虚地址，据此虚地址从存储器中读取1个字节的值并进行0扩展，写入到rt寄存器中。（加载字节，结果0扩展）

公式说明：rt = memory[base + offset]

 

### 7.3 LH

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps5FA.tmp.jpg) 

汇编格式：LH rt, offset(base)

功能描述：将base寄存器的值加上符号扩展后的立即数offset得到访存的虚地址，如果地址不是2的整数倍则触发地址错例外，否则据此虚地址从存储器中读取连续2个字节的值并进行符号扩展，写入到rt寄存器中。(加载半字)

公式说明：rt = memory[base + offset]

例外：地址最低位不为0，触发地址错例外

 

### 7.4 LHU

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps60B.tmp.jpg) 

汇编格式：LHU rt, offset(base) 

功能描述：将base寄存器的值加上符号扩展后的立即数offset得到访存的虚地址，如果地址不是2的整数倍则触发地址错例外，否则据此虚地址从存储器中读取连续2个字节的值并进行0扩展，写入到rt寄存器中。（加载半字，结果0扩展）

公式说明：rt = memory[base + offset]

例外：地址最低位不为0，触发地址错例外

 

### 7.5 LW

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps60C.tmp.jpg) 

汇编格式：LW rt, offset(base)

功能描述：将base寄存器的值加上符号扩展后的立即数offset得到访存的虚地址，如果地址不是4的整数倍则触发地址错例外，否则据此虚地址从存储器中读取连续4个字节的值并进行符号扩展，写入到rt寄存器中。(加载字)

公式说明：rt = memory[base + offset]

举例：lw $1, 10($2)  表示：$1 = memory[$2 + 10]

例外：地址最低2位不为0，触发地址错例外

 

### 7.6 SB

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps60D.tmp.jpg) 

汇编格式：SB rt, offset(base) 

功能描述：将base寄存器的值加上符号扩展后的立即数offset得到访存的虚地址，据此虚地址将rt寄存器的最低字节存入存储器中。（存字节）

公式说明：memory[base + offset] = rt

 

### 7.7 SH

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps60E.tmp.jpg) 

汇编格式：SH rt, offset(base)

功能描述：将base寄存器的值加上符号扩展后的立即数offset得到访存的虚地址，如果地址不是2的整数倍则触发地址错例外，否则据此虚地址将rt寄存器的低半字存入存储器中。（存半字）

公式说明：memory[base + offset] = rt

例外：地址最低2位不为0，触发地址错例外

 

### 7.8 SW

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps60F.tmp.jpg) 

汇编格式：SW rt, offset(base) 

功能描述：将base寄存器的值加上符号扩展后的立即数offset得到访存的虚地址，如果地址不是4的整数倍则触发地址错例外，否则据此虚地址将rt寄存器存入存储器中。（存字）

公式说明：memory[base + offset] = rt

举例：sw $1, 10($2)  表示：memory[$2 + 10] = $1

例外：地址最低4位不为0，触发地址错例外

 

## 8、协处理器0指令

### 8.1 ERET

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps610.tmp.jpg) 

汇编格式：ERET

功能描述：异常返回，返回EPC中保存的地址。

公式说明：PC = EPC

 

### 8.2 MFC0

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps611.tmp.jpg) 

汇编格式：MFC0 rt, rd, sel

功能描述：从协处理器0（CP0）获取一个32位数据到通用寄存器。（如果cp0寄存器是64位数据则只取其低32位）

公式说明：GPR[rt] = CP0[rd,sel]

举例：mfc0 t0, $1  表示：从CP0 取$1数据，存到通用寄存器t0。

 

### 8.3 MTCO

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps612.tmp.jpg) 

汇编格式：MTC0 rt, rd, sel

功能描述：向协处理器0（CP0）寄存器存值。

公式说明：CP0[rd,sel] = GPR[rt]

举例：mtco t0, $1  表示：从通用寄存器t0取$1数据，存到CP0。

 

### 8.4 MFC1

汇编格式：MFC1 rt, cs

功能描述：把一个数据从定点寄存器复制到浮点寄存器。

举例：MFC1 R1, F1  

 

### 8.5 MTC1

汇编格式：MTC1 rt, cs

功能描述：把一个数据从浮点寄存器复制到定点寄存器。

举例：MTC1 R1, F1  

 

### 8.6 DMFC0

汇编格式：DMFC0 rt, cs

功能描述：从协处理器0（CP0）寄存器取双字（64位数据）。

 

### 8.7 DMTCO

汇编格式：DMTC0 rt, cs

功能描述：向协处理器0（CP0）寄存器写双字（64位数据）。

注意：和CP0相关的指令执行都需要特权模式，比如上面的mfc0指令的运行需要root权限或者sudo才可以运行。所以在一般用户程序中很少见到这些CP0相关的控制指令。

 

## 9、 其他逻辑指令

### 9.1 LI

汇编格式：LI rt, immediate

功能描述：取立即数到寄存器。

公式说明：rt = imm

举例：li v1, 0x300000  表示：v1 = 0x300000

 

### 9.3 CLO

汇编格式：CLO rd, rs

功能描述：对地址为rs的通用寄存器的值，从其最高位开始向最低位方向检查，直到遇到值为“0”的位，将该位之前“1”的个数保存到地址为rd的通用寄存器。

如果地址为rs的通用寄存器的所有位都为1（即OxFFFFFFFF），那么将32保存到地址为rd的通用寄存器中。

 

### 9.4 DCLO

汇编格式：DCLO rd, rs

功能描述：CLO的64位运算形式

 

### 9.5 CLZ

汇编格式：CLZ rd, rs

功能描述：对地址为rs的通用寄存器的值，从其最高位开始同最低位方向检查，直到遇到值为“1”的位，将该位之前“0”的个数保存到地址为rd的通用寄存器中。如果地址为rs的通用寄存器的所有位都为0（即0x00000000），那么将32保存到地址为rd的通用寄存器中。

 

### 9.6 DCLZ

汇编格式：DCLZ rd, rs

功能描述：CLZ的64位运算形式



### 9.7 WSBH

汇编格式：WSBH rd, rt

功能描述：半字中字节交换。实现一个32位操作数rt内的两个半字内部进行字节交换，结果存入rd。类似于：rd = rt( 23..16）|| rt( 31..24 ) || rt( 7..0) || rt( 15..8 )

 

### 9.8 DSHD

汇编格式：DSHD rd, rt

功能描述：字中半字交换。类似于：rd = rt(15..0) || rt(31..16) || rt(47..32) || rt(63..48)

 

### 9.9 DSBH

汇编格式：DSBH rd, rt

功能描述：双字中的半字中字节交换。类似于：

rd = rt(55.48) || rt(63..56) || rt(39..32) || rt(47..40) || rt(23..16) || rt(31..24) || rt(7..0 )|| rt(15..8)

 

### 9.10 SEB

汇编格式：SEB rd, rt

功能描述：字节符号扩展。（B代表Byte（8位）

 

### 9.11 SEH

汇编格式：SEH rd, rt

功能描述：半字符号扩展。（H代表HalfWord（16位）

 

### 9.12 INS 

汇编格式：INS rt, rs, pos, size

功能描述：将操作数rs的低size位，插入到目标操作数的（pos到pos+size）位置。此处rs, rt均为32位数。

其中：pos取值0-31，size取值1-32 ，pos+size取值（1-32）。

举例：ins a3, t0, 0x5, 0x3  表示：将寄存器t0的低3位，放到目标寄存器a3的第5位到第7位（bit5-bit7）

 

### 9.13 DINS 

汇编格式：DINS rt, rs, pos, size

功能描述：双字位插入。将操作数rs的低size位，插入到目标操作数的（pos到pos+size）位置。此处rs, rt可以是32位数或者64位数。

其中：pos取值0-31，size取值1-32 ，pos+size取值（1-32）

 

### 9.14 DINSM

汇编格式：DINSM rt, rs, pos, size

功能描述：双字位插入。pos取值0-31，size取值2-64，pos+size取值（33-64）。

对目标操作数rt的高32位的域值设置。

 

### 9.15 DINSU 

汇编格式：DINSU rt, rs, pos, size

功能描述：双字位插入。pos取值32-63，size取值1-32，pos+size取值（33-64）。

对目标操作数rt的高32位的域值设置。

注：一般只要使用DINS指令就可以，汇编器会根据需要选择是DINSU还是DINSM。

 

### 9.16 EXT

汇编格式：EXT rt, rs, pos, size

功能描述：32位寄存器的域值提取。即：将操作数rs的第pos位到第（pos+size-1）位提取出来，结果保存到目标寄存器rt中。

其中：pos取值0-31, size取值1-32，pos+size取值（1-32）

举例：ext a3, t0, 0x5, 0x3  表示：将寄存器t0的第5位到第7位提取出来存放到寄存器a3中。

 

### 9.17 DEXT

汇编格式：DEXT rt, rs, pos, size

功能描述：64位操作数的域值部分提取。pos取值0-31, size取值1-32，pos+size取值（1-63）

 

### 9.18 DEXTM 

汇编格式：DEXTM rt, rs, pos, size

功能描述：64位操作数rs的高32位（bit32-bit63）域值的部分提取。pos取值0-31, size取值33-64，pos+size取值（33-64）

 

### 9.19 DEXTU

汇编格式：DEXTU rt, rs, pos, size

功能描述：64位操作数rs的高32位（bit32-bit63）域值的部分提取。pos取值32-63, size取值1-32，pos+size取值（33-64）

注：一般只要使用DEXT指令就可以，汇编器会根据需要选择是DEXTU还是DEXTM。



## 10、其他移位指令

### 10.1 DSLL

汇编格式：DSLL rd, rt, sa

功能描述：双字逻辑左移，rt为64位操作数，0 <= sa < 32

公式说明：rd = rt << sa

举例： dsll a3, t1, 3   表示：a3 = t1 << 3  t1为64位

 

### 10.2 DSLLV

汇编格式：DSLLV rd, rt, rs

功能描述：由寄存器rs中的值指定移位量，对寄存器rt的值进行逻辑左移，结果写入寄存器rd中。rt表示64位数。

公式说明：rd = rt << rs

举例：dsllv $1, $2, $3  表示：$1 = $2 << $3

 

### 10.3 DSLL32

汇编格式：DSLL32 rd, rt, sa

功能描述：移位量加32的双字逻辑左移。

公式说明：rd = rt << (sa+32)

举例： dsll32 a3, t1, 3   表示：a3 = t1 << (3 + 32)  t1为64位

 

### 10.4 DSRL

汇编格式：DSRL rd, rt, sa

功能描述：双字逻辑右移，rt为64位操作数，0 <= sa < 32

公式说明：rd = rt >> sa

举例： dsrl a3, t1, 3   表示：a3 = t1 >> 3  t1为64位

 

### 10.5 DSRLV

汇编格式：DSRLV rd, rt, rs

功能描述：可变的双字逻辑右移。由寄存器rs中的值指定移位量，对寄存器rt的值进行逻辑右移，结果写入寄存器rd中。rt表示64位数。

公式说明：rd = rt >> rs

举例：dsrlv $1, $2, $3  表示：$1 = $2 >> $3

 

### 10.6 DSPL32

汇编格式：DSRL32 rd, rt, sa

功能描述：移位量加32的双字逻辑右移。

公式说明：rd = rt >> (sa+32)

举例： dspl32 a3, t1, 3   表示：a3 = t1 >> (3 + 32)  t1为64位

 

### 10.7 DSRA

汇编格式：DSRA rd, rt, sa

功能描述：双字算术右移

 

### 10.8 DSRAV

汇编格式：DSRAV rd, rt, rs

功能描述：可变的双字算术右移

 

### 10.9 DSRA32

汇编格式：DSRA32 rd, rt, sa

功能描述：移位量加 32 的双字算术右移。

公式说明：rd = rt >> (sa+32)

举例： dsra 32 a3, t1, 3   表示：a3 = t1 >> (3 + 32)  t1为64位

 

### 10.10 ROTR

汇编格式：ROTR rd, rt, sa

功能描述：循环右移 temp ← GPR[rt] sa-1..0 || GPR[rt] 31..sa

 rd ← sign_extend(temp)

 

### 10.11 DRTRV

汇编格式：ROTRV rd, rt, rs

功能描述：可变的循环右移

 

### 10.11 DROTR

汇编格式：DROTR rd, rt, rs

功能描述：双字循环右移

 

### 10.12 DROTR32

汇编格式：DROTR32 rd, rt, sa

功能描述：移位量加32的双字循环右移

 

### 10.13 DROTRV

汇编格式：DROTRV rd, rt, rs

功能描述：双字可变的循环右移

 

## 11、其他访存指令

### 11.1 LWU

汇编格式：LWU rt, offset(base)

功能描述：加载字，结果0扩展。

举例：lwu v1, 32(gp)    表示：v1 = memory[gp + 32]  

即：从地址为gp+32的内存位置加载一个32位数据，结果无符号扩展到64位后存入v1寄存器。

 

### 11.2 LWL

汇编格式：LWL rt, offset(base)

功能描述： 加载字的左部。

 

### 11.3 LWR

汇编格式：LWR rt, offset(base)

功能描述：加载字的右部。

 

### 11.4 LD

汇编格式：LD rt, offset(base)

功能描述：加载双字。

举例：ld v0, -32688(gp)   表示：v0 = memory[gp-32688]

即：从地址值为gp-32688的内存位置，加载一个64位的数据赋给寄存器v0。如果地址无效，程序会收到异常信号。

 

### 11.5 LDL

汇编格式：LDL rt, offset(base)

功能描述：加载双字左部。

 

### 11.6 LDR

汇编格式：LDR rt, offset(base)

功能描述：加载双字右部。

 

### 11.7 LL

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps613.tmp.jpg) 

汇编格式：LL rt, offset(base)

功能描述：从内存中读取一个字，以实现接下来的 RMW（Read-Modify-Write） 操作。

公式说明：rt = memory[base + offset]

举例：LL rt, off(b)   表示：处理器会记住 LL 指令的这次操作（会在 CPU 的寄存器中设置一个不可见的 bit 位），同时 LL 指令读取的地址 off(b) 也会保存在处理器的寄存器中。

 

### 11.8 LLD

汇编格式：LLD rt, offset(base)

功能描述：加载标志处地址双字地址

举例：   表示：

 

### 11.9 SWL

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps614.tmp.jpg) 

汇编格式：SWL rt, offset(base)

功能描述：将地址为rt的通用寄存器的最高4 - n个字节存储到地址storeaddr处。

其中：存储地址storeaddr = signed_extended(offset) + GPR[base]，storeaddr最低两位的值为n，storeaddr最低两位设为0后的值称为storeaddr_align。

即：n = storeaddr[1:0]  storeaddr_align = storeaddr – n

举例：如果计算出来的存储地址是5，swl指令要向地址5存储数据，那么storeaddr就等于5，n等于1，storeaddr_align等于4。

swl $1, 5($0)  表示：将地址rt的通用寄存器的最高3个字节存储到从地址5开始处。相应的是地址为5、6、7三个位置。

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps615.tmp.jpg) 

 

### 11.10 SWR

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps616.tmp.jpg) 

汇编格式：SWR rt, offset(base)

功能描述：将地址为rt的通用寄存器的最低n+1个字节存储到地址storeaddr_align处。

举例：如果计算出来的存储地址是9，swr指令要向地址9存储数据，那么storeaddr就等于9，n等于1，storeaddr_align等于8。

swr $1, 9($0)    表示：将地址rt的通用寄存器的最低2个字节存储到从地址8开始处，相应的是地址为8、9两个位置。

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps617.tmp.jpg) 

 

### 11.11 SD

汇编格式：SD rt, offset(base)

功能描述：存双字

举例：sd ra, -352(ra)   表示：memory[ra -352] = ra

即：把寄存器ra里的64位数写出到内存地址为（ra-352）的位置。如果地址无效，程序会收到异常信号。

 

### 11.12 SDL

汇编格式：SDL rt, offset(base)

功能描述：存双字左部

 

### 11.13 SDR

汇编格式：SDR rt, offset(base)

功能描述：存双字右部

 

### 11.14 SC

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps618.tmp.jpg) 

汇编格式：SC rt, offset(base)

功能描述：向内存中写入一个字，以完成前面的RMW(Read-Modify-Write)操作。

即：首先会检查从上次执行的LL开始以来的读写操作能否原子性的完成，如果能，rt值被成功存储同时rt被置1。否则rt被置0。

公式说明：if atomic_update  memory[base + offset] = rt, rt = 1; else rt = 0.

举例：SC rt, off(b)   表示：首先检查上次 LL 指令执行后的 RMW 操作是否是原子操作（即不存在其它对这个地址的操作），如果是原子操作，则 rt 的值将会被更新至内存中，同时 rt 的值也会变为1，表示操作成功；反之，如果 RMW 的操作不是原子操作（即存在其它对这个地址的访问冲突），则 rt 的值不会被更新至内存中，且 rt = 0，表示操作失败。

 

### 11.15 SCD

汇编格式：SCD rt, offset(base)

功能描述：带条件的存双字

 

## 12、其他分支跳转指令

### 12.1 B

汇编格式：B lable

功能描述：相对PC的跳转，寻址范围小于256K

 

### 12.2 BAL

汇编格式：BAL lable

功能描述：相对PC的子程序调用，寻址空间小于256K

注：BAL属于分支调用。另：跳转调用指令，一般用JAL。

 

## 13、其他协处理器0指令

### 13.1 CACHE

![img](https://gitee.com/halley5/documents/raw/master/img/img/wps619.tmp.jpg) 

汇编格式：CACHE op，offset(base)

功能描述：执行op制定的cache操作，16位的offset经符号位扩展，添加到base寄存器以形成有效地址。

说明：在MIPS中，当发现一个无法译码的地址的时候，就触发一个TLB重装异常，然后由程序作剩下的事情。

mips的内存管理方式：虚拟地址VA经过MMU，MMU查询TLB进行转换为PA，整个转换的过程不需要访问内存，所以mips的VA到PA的转换很快。 缺点是TLB的大小有限，如果TLB查询不到，就会引发TLB 异常，而频繁的异常处理会浪费大量的时间。

### TLB的概念：

参考链接：http://renyan.spaces.eepw.com.cn/articles/article/item/123985

### 13.2 TLBR

功能描述：根据index来读tlb表项。

 

### 13.3 TLBWI

功能描述：根据index来写tlb表项。

 

### 13.4 TLBWR

功能描述：对radom寄存器选中的tlb表项进行写操作。

 

### 13.5 TLBP

功能描述：在tlb中查找VPN, ASID 都跟EntryHi寄存器中的相应域匹配的入口，并把相应表项的索引值存入index寄存器。如果匹配失败，那么将index的P位（最高为）置一。