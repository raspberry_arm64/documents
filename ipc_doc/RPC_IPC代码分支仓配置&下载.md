# RPC_IPC代码分支仓配置&下载

# 一：HoperunHarmony中建立OpenHarmony分支仓库



HoperunHarmony中分支是如何建立的请参考
https://gitee.com/halley5/documents/blob/master/operation_manual/%E4%BB%A3%E7%A0%81%E5%88%86%E6%94%AF%E5%88%9B%E5%BB%BA%E5%92%8C%E6%8B%89%E5%8F%96.md
当前只介绍如何在分支上建立仓库,以OpenHarmony组织中的communication_ipc_lite仓说明。
1>把OpenHarmony组织中的communication_ipc_lite仓库fork到HoperunHarmony组织中
2>在HoperunHarmony组织中manifest的master分支上创建添加rpc_ipc.xml文件

# 二：rpc_ipc配置&代码下载

## 2.1 rpc_ipc.xml配置说明

```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    <remote fetch="https://gitee.com/openharmony-sig/" name="sig" review="https://gitee.com/openharmony-sig/"/>
    <remote fetch="https://gitee.com/halley5/" name="rpc_ipc" review="https://gitee.com/halley5"/>
    <include name="default.xml" />
    <remove-project name="communication_ipc_lite"/>
    <project name="communication_ipc_lite" path="foundation/communication/ipc_lite" revision="master" remote="rpc_ipc"/>
</manifest>
```

说明：

```
   <remote fetch="https://gitee.com/openharmony-sig/" name="sig" review="https://gitee.com/openharmony-sig/"/>
    <remote fetch="https://gitee.com/halley5/" name="rpc_ipc" review="https://gitee.com/halley5"/>
```

这里有两个fetch分支，一个分支仓库路径为https://gitee.com/openharmony-sig/别名sig，一个为https://gitee.com/openharmony-sig/别名rpc_ipc我们下载代码就是从这两个路径下载

```
<include name="default.xml" />
```

指明了要下载的全量仓库，这里其实就是https://gitee.com/openharmony-sig/组织的全量仓库

```
<remove-project name="communication_ipc_lite"/>
```

则是说明下载时从全量仓库中去除communication_ipc_lite仓库，这个仓库从哪里下载啦，由下面指定

```
 <project name="communication_ipc_lite" path="foundation/communication/ipc_lite" revision="master" remote="rpc_ipc"/>
```

从remote="rpc_ipc"即https://gitee.com/halley5/下载name="communication_ipc_lite"仓库到全量仓库代码路径path="foundation/communication/ipc_lite路径下

## 2.2 rpc_ipc代码下载

```
mkdir -p ~/OpenHarmony/rpc_ipc
cd ~/OpenHarmony/rpc_ipc
repo init -u https://gitee.com/halley5/manifest.git -b master -m rpc_ipc.xml --no-repo-verify
repo sync -c -j16
repo forall -c 'git lfs pull'
repo start master --all
```



## 2.3 rpc_ipc修改和提交

以communication_ipc_lite仓为例

```
cd ~~/OpenHarmony/rpc_ipc/foundation/communication/ipc_lite
git remote add origin https://gitee.com/halley5/communication_ipc_lite.git
// todo
git add .
git commint .
git push -u origin master
```

