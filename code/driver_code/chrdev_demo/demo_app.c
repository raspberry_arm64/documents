#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>

struct demo_msg {
    char name[32];
    int value;
    char data[128];
};

#define IOCTL_IO   _IO('d', 0x10)
#define IOCTL_IOR  _IOR('d', 0x11, int)
#define IOCTL_IOW  _IOW('d', 0x12, struct demo_msg)
#define IOCTL_IOWR _IOR('d', 0x13, struct demo_msg)

int main(int argc, char *argv[])
{
    int fd;
    int value = 0;
    ssize_t size;
    int ret;
    struct demo_msg msg;
    fd = open("/dev/demo_device", O_RDWR);
    if (fd < 0) {
        perror("open demo device");
        return -1;
    }

    ret = ioctl(fd, IOCTL_IO);
    ret = ioctl(fd, IOCTL_IOR, &msg);
    if (ret) {
        printf("[line]: %d, IOCTL_IOR error, ret = %d\n", __LINE__, ret);
        return -1;
    }
    printf("name = %s, value = %d, data = %s\n", msg.name, msg.value, msg.data);

    strncpy(msg.name, "userapp", strlen("userapp") + 1);
    msg.value = 0;
    strncpy(msg.name, "clean data", strlen("clean data") + 1);
    ret = ioctl(fd, IOCTL_IOW, &msg);
    if (ret) {
        printf("[line]: %d, IOCTL_IOW error, ret = %d\n", __LINE__, ret);
        return -1;
    }

    ret = ioctl(fd, IOCTL_IOWR, &msg);
    if (ret) {
        printf("[line]: %d, IOCTL_IOWR error, ret = %d\n", __LINE__, ret);
        return -1;
    }
    printf("name = %s, value = %d, data = %s\n", msg.name, msg.value, msg.data);

    close(fd);
    return 0;
}
