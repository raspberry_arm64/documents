#ifndef __CHRDEV_DEMO_H__
#define __CHRDEV_DEMO_H__

#define CHRNAME     "chrdev_demo"
#define CLASSNAME   "demo_class"
#define DEVICENAME  "demo_device"
#define BASE_ADDR   0x11000000
#define BASE_SIZE   0x10

typedef long (*ioctl_fun)(struct file *filep, unsigned long arg);

struct demo_desc {
    unsigned int dev_major;
    struct class *pcls;
    struct device *pdev;
    void *reg_virt_base;
};

struct ioctl_cmd {
    int cmd;
    ioctl_fun func;
};

struct demo_msg {
    char name[32];
    int value;
    char data[128];
};

#define IOCTL_IO   _IO('d', 0x10)
#define IOCTL_IOR  _IOR('d', 0x11, int)
#define IOCTL_IOW  _IOW('d', 0x12, struct demo_msg)
#define IOCTL_IOWR _IOR('d', 0x13, struct demo_msg)

#endif