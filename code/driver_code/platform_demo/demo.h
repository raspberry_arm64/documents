#ifndef __DEMO_DRIVER_H__
#define __DEMO_DRIVER_H__

#include <linux/ioctl.h>
#include <linux/miscdevice.h>

#define LOG_TAG "DEMO_LOG"

#define DEMO_MODULE_NAME "dev-demo"

#define DEMO_IOCTL_IO _IO('d', 0x01)
#define DEMO_IOCTL_IOR _IOR('d', 0x02, int)
#define DEMO_IOCTL_IOW _IOW('d', 0x03, struct demo_msg)
#define DEMO_IOCTL_IOWR _IOWR('d', 0x04, struct demo_msg)
#define DEMO_IOCTL_OPS(_cmd, _func) { .cmd = (_cmd), .func = (_func) }

#define demo_info(fmt, ...) pr_info("[" LOG_TAG "][I] %s:" fmt "\n", __FUNCTION__, ##__VA_ARGS__)
#define demo_err(fmt, ...) pr_err("[" LOG_TAG "][E] %s:" fmt "\n", __FUNCTION__, ##__VA_ARGS__)

struct demo_device {
    int major;
    int minor;
    struct miscdevice device;
    struct platform_device *dev;
    char *name;
};

struct demo_msg {
    int fd;
    unsigned int size;
    unsigned int mapped;
    unsigned int iova;
    char name[32];
    unsigned int value;
    char data[128];
};

typedef long (*demo_ioctl_func_t)(struct file *fd, unsigned long args);
struct demo_ioctl_ops {
    unsigned int cmd;
    demo_ioctl_func_t func;
};



#endif
