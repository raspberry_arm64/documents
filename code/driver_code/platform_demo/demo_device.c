#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/kernel.h>
#include "demo.h"

static void demo_release(struct device *dev)
{
    return 0;
}
 
const struct device demo_dev = {
    .release = demo_release,
};

static struct platform_device demo_device = {
    .name = DEMO_MODULE_NAME,
    .dev = demo_dev,
};

static int __init demo_init(void)
{
    return platform_device_register(&demo_device);
}

static void __exit demo_exit(void)
{
    platform_device_unregister(&demo_device);
}

module_init(demo_init);
module_exit(demo_exit);

MODULE_DESCRIPTION("platform demo device");
MODULE_LICENSE("GPL");
