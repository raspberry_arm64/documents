# code代码介绍

## 一、文档列表

| 文档名                                                   | 代码简单描述           | 提供者 |
| -------------------------------------------------------- | ---------------------- | ------ |
| [Linux字符设备代码Template](./driver_code/chrdev_demo)   | 字符设备驱动的框架流程 | 闻飞   |
| [Linux平台驱动代码Template](./driver_code/platform_demo) | 平台驱动的框架流程     | 闻飞   |
| [Harmony HDF驱动demo](./harmony_template/driver_demo)    | 鸿蒙HDF驱动框架的Demo  | 闻飞   |
|                                                          |                        |        |
|                                                          |                        |        |
|                                                          |                        |        |
|                                                          |                        |        |
|                                                          |                        |        |
|                                                          |                        |        |

## 二、文档功能摘要

### 2.1 [Linux字符设备代码Template](./driver_code/chrdev_demo)摘要

​		简单介绍Linux下编写字符设备驱动代码的步骤或者说框架，适合初学者了解

### 2.2 [Linux平台驱动代码Template](./driver_code/platform_demo)摘要

​		介绍Linux下平台驱动的框架，借助框架进行配置即可实现一个简单的平台驱动demo

### 2.3 [Harmony HDF驱动demo](./harmony_template/driver_demo)摘要

​		鸿蒙HDF驱动框架下的demo设备驱动框架代码



