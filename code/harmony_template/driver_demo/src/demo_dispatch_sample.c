/* Copyright 2020 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "demo_core.h"
#include "demo_dispatch_sample.h"
#include "hdf_log.h"
#include "hdf_sbuf.h"
#include "osal_mem.h"

#define HDF_LOG_TAG demo_dispatch_sample

static int32_t SampleDispatchWrite(struct DemoDevice *device, struct HdfSBuf *txBuf)
{
    return HDF_SUCCESS;
}

int32_t SampleDispatch(struct HdfDeviceIoClient *client, int cmdId, struct HdfSBuf *data, struct HdfSBuf *reply)
{
    int32_t result = HDF_FAILURE;
    if (client == NULL || client->device == NULL) {
        HDF_LOGE("%s: client or client->device is NULL", __func__);
        return result;
    }
    struct DemoHost *demoHost = (struct DemoHost *)client->device->service;
    if (demoHost == NULL) {
        HDF_LOGE("%s: demoHost is NULL", __func__);
        return result;
    }
    struct DemoDevice *demoDevice = (struct DemoDevice *)demoHost->priv;
    if (demoDevice == NULL) {
        HDF_LOGE("%s: demoDevice is NULL", __func__);
        return result;
    }
    switch (cmdId) {
        case DEMO_WRITE: {
            result = SampleDispatchWrite(demoDevice, data);
            break;
        }
        default:
            break;
    }
    return result;
}
