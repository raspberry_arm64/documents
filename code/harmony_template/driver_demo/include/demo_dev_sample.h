/* Copyright 2020 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DEMO_DEV_SAMPLE_H
#define DEMO_DEV_SAMPLE_H

#include "sys/ioctl.h"
#include "demo_core.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

#define DEMO_IOC_MAGIC   'd'

struct DemoResource {
    uint32_t num;        /* UART port num */
};

enum DemoDeviceState {
    DEMO_DEVICE_UNINITIALIZED = 0x0u,
    DEMO_DEVICE_INITIALIZED = 0x1u,
};

struct DemoDevice {
    struct IDeviceIoService ioService;
    struct DemoResource resource;
    enum DemoDeviceState state;     /* UART State */
};


void AddUartDevice(struct DemoHost *host);
void RemoveUartDevice(struct DemoHost *host);

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif /* UART_DEV_SAMPLE_H */
